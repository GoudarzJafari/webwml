#use wml::debian::translation-check translation="5fc831c6a77ce47c0c4e44da8a3b594b8280e3dc"
<define-tag pagetitle>Debian 12 <q>bookworm</q> werd uitgebracht</define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news

<p>Na 1 jaar, 9 maanden en 28 dagen ontwikkeling, stelt het Debian-project met trots zijn nieuwe stabiele versie 12 (codenaam <q>bookworm</q>) voor.</p>

<p><q>bookworm</q> zal de komende 5 jaar worden ondersteund dankzij het gezamenlijke werk van het <a href="https://security-team.debian.org/">Debian Beveiligingsteam</a> en het <a href="https://wiki.debian.org/LTS">Debian Langetermijnondersteuningsteam</a>.</p>

<p>In navolging van de <a href="$(HOME)/vote/2022/vote_003">Algemene resolutie van 2022 over niet-vrije firmware</a> hebben we een nieuw archiefgebied geïntroduceerd dat het mogelijk maakt om niet-vrije firmware te scheiden van de andere niet-vrije pakketten:</p>
<ul>
<li>non-free-firmware</li>
</ul>

<p>
De meeste niet-vrije firmwarepakketten zijn verplaatst van <b>non-free</b> naar <b>non-free-firmware</b>. Deze opsplitsing maakt het mogelijk om verschillende officiële installatie-images te bouwen.
</p>

<p>Debian 12 <q>bookworm</q> wordt geleverd met verschillende grafische werkomgevingen, zoals:
</p>
<ul>
<li>Gnome 43,</li>
<li>KDE Plasma 5.27,</li>
<li>LXDE 11,</li>
<li>LXQt 1.2.0,</li>
<li>MATE 1.26,</li>
<li>Xfce 4.18</li>
</ul>

<p>Deze release bevat meer dan <b>11.089</b> nieuwe pakketten op een totaal aantal van <b>64.419</b> pakketten, terwijl meer dan <b>6.296</b> pakketten als <q>verouderd</q> verwijderd werden. <b>43.254</b> pakketten werden in deze release bijgewerkt.

Het totale schijfgebruik voor <q>bookworm</q> bedraagt <b>365.016.420 kB (365 GB)</b> en bestaat uit <b>1.341.564.204</b> regels code.</p>

<p><q>bookworm</q> heeft meer vertaalde man-pagina's dan ooit dankzij onze vertalers die <b>man</b>-pagina's beschikbaar hebben gemaakt in meerdere talen zoals: Tsjechisch, Deens, Grieks, Fins, Indonesisch, Macedonisch, Noors (Bokmål), Russisch, Servisch, Zweeds, Oekraïens en Vietnamees.
Alle <b>systemd</b> man-pagina's zijn nu volledig beschikbaar in het Duits.</p>

<p>De doelgroepspecifieke collectie Debian Med introduceert een nieuw pakket, <b>shiny-server</b>, dat wetenschappelijke webtoepassingen vereenvoudigt met behulp van <b>R</b>. We zijn doorgegaan met onze inspanningen om continue integratieondersteuning te bieden voor de pakketten van het Debian Med-team. Installeer de metapakketten van versie 3.8.x voor Debian bookworm.</p>

<p>De doelgroepspecifieke collectie Debian Astro blijft een totaaloplossing bieden voor professionele astronomen, liefhebbers en hobbyisten met updates voor bijna alle versies van de softwarepakketten in de collectie. <b>astap</b> en <b>planetary-system-stacker</b> helpen bij beeldstapeling en astrometrische resolutie. <b> openvlbi</b>, de openbron correlator zit nu in de collectie.</p>

<p>Ondersteuning voor Secure Boot op ARM64 werd opnieuw geïntroduceerd: gebruikers van UEFI-compatibele ARM64-hardware kunnen opstarten met een geactiveerde Secure Boot-modus om de beveiligingsfunctie ten volle te benutten.</p>

<p>Debian 12 <q>bookworm</q> bevat talrijke bijgewerkte softwarepakketten (meer dan 67% van alle pakketten uit de vorige release), zoals:
</p>

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS Server 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (standaard email-server) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU Compiler Collection 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>De GNU C Library 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Linux kernel 6.1 serie</li>
<li>LLVM/Clang gereedschapsset 13.0.1, 14.0 (standaard), en 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>

<p>
Met deze brede selectie van pakketten en de traditionele brede architectuurondersteuning blijft Debian opnieuw trouw aan zijn doel om <q>Het Universele Besturingssysteem</q> te zijn. Het is geschikt voor veel verschillende gebruikssituaties: van desktopsystemen tot netbooks, van ontwikkelservers tot clustersystemen, en voor database-, web- en opslagservers. Tegelijkertijd zorgen extra inspanningen voor kwaliteitsgarantie, zoals automatische installatie- en upgradetests voor alle pakketten in het Debian-archief, ervoor dat <q>bookworm</q> voldoet aan de hoge verwachtingen die gebruikers hebben van een stabiele Debian-release.
</p>

<p>
In totaal worden negen architecturen officieel ondersteund in <q>bookworm</q>:
</p>
<ul>
<li>32-bits pc (i386) en 64-bits pc (amd64),</li>
<li>64-bits ARM (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (EABI hard-float ABI, armhf),</li>
<li>little-endian MIPS (mipsel),</li>
<li>64-bits little-endian MIPS (mips64el),</li>
<li>64-bits little-endian PowerPC (ppc64el),</li>
<li>IBM System z (s390x)</li>
</ul>

<p>
De architectuur 32-bits pc (i386) ondersteunt niet langer de i586-processor; de nieuwe minimale processorvereiste is i686. <i>Als uw machine niet compatibel is met deze vereiste, wordt u aangeraden om bij bullseye te blijven gedurende de rest van zijn ondersteuningscyclus.</i>
</p>

<p>Het Debian Cloud-team publiceert <q>bookworm</q> voor verschillende cloudcomputingdiensten:
</p>
<ul>
<li>Amazon EC2 (amd64 en arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (algemeen) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amd64, arm64, ppc64el)</li>
</ul>
Het genericcloud-image zou in elke gevirtualiseerde omgeving moeten kunnen werken, en er is ook een nocloud-image dat handig is voor het testen van het bouwproces.
</p>

<p>GRUB-pakketten zullen standaard <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#grub-os-prober">niet langer os-prober uitvoeren om andere besturingssystemen te vinden.</a></p>

<p>Tussen de releases in heeft het Technisch Comité beslist dat Debian <q>bookworm</q> <a href="$(HOME)/UsrMerge">alleen de indeling van het basisbestandssysteem met een samengevoegde usr</a> moet ondersteunen, waarbij de ondersteuning voor de indeling met een niet-samengevoegde usr weggelaten wordt. Voor systemen die geïnstalleerd zijn als buster of bullseye zullen er geen wijzigingen zijn aan het bestandssysteem; systemen die de oudere indeling gebruiken zullen echter geconverteerd worden tijdens de upgrade.</p>


<h3>Wilt u het eens uitproberen?</h3>
<p>
Als u Debian 12 <q>bookworm</q> gewoon wilt uitproberen zonder het te installeren, kunt u een van de beschikbare <a href="$(HOME)/CD/live/">live-images</a> gebruiken. Hiermee laadt u en voert u het volledige besturingssysteem uit in een alleen-lezen modus in het geheugen van uw computer.
</p>

<p>
Deze live-images worden ter beschikking gesteld voor de architecturen <code>amd64</code> en <code>i386</code> en zijn beschikbaar voor dvd, USB-stick en netboot-opstellingen. De gebruiker kan kiezen uit verschillende grafische werkomgevingen om uit te proberen: GNOME, KDE Plasma, LXDE, LXQt, MATE en Xfce. Debian Live <q>bookworm</q> heeft een standaard live-image, waardoor het ook mogelijk is om een basaal Debian-systeem uit te proberen zonder enige grafische gebruikersinterface.
</p>

<p>
Als u het besturingssysteem leuk vindt, heeft u de mogelijkheid om het vanaf
het live-image op de harde schijf van uw computer te installeren. Het
live-image bevat zowel het onafhankelijke Calamares-installatieprogramma als
het standaard Debian-installatieprogramma.
Meer informatie is te vinden in de
<a href="$(HOME)/releases/bookworm/releasenotes">notities bij de release</a>
en de secties van de website van Debian over
<a href="$(HOME)/CD/live/">live installatie-images</a>.
</p>

<p>
Om Debian 12 <q>bookworm</q> rechtstreeks op het opslagapparaat van uw computer te installeren, kunt u kiezen uit verschillende soorten installatiemedia die u kunt <a href="https://www.debian.org/download">downloaden</a>, zoals: Blu-rayschijf, dvd, cd, USB-stick, of via een netwerkverbinding.

Zie de <a href="$(HOME)/releases/bookworm/installmanual">Installatiehandleiding</a> voor meer details.
</p>

# Translators: some text taken from:

<p>
Debian kan nu in 78 talen worden geïnstalleerd, waarvan de meeste beschikbaar zijn in zowel een op tekst gebaseerde als in een grafische gebruikersinterface.
</p>

<p>
De installatie-images kunnen nu worden gedownload via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (de aanbevolen methode),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, of
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; zie
<a href="$(HOME)/CD/">Debian op cd's</a> voor verdere informatie. <q>bookworm</q> zal weldra ook beschikbaar zijn op fysieke dvd-, cd-, en
blu-rayschijven bij talloze <a href="$(HOME)/CD/vendors">leveranciers</a>.
</p>


<h3>Debian opwaarderen</h3>
<p>
Opwaarderingen naar Debian 12 <q>bookworm</q> vanaf de vorige release, Debian 11
<q>bullseye</q>, worden voor de meeste configuraties automatisch afgehandeld door het pakketbeheerprogramma APT.
</p>

<p>Voordat u een opwaardering van uw systeem uitvoert, is het sterk aanbevolen om een volledige back-up te maken, of op zijn minst een back-up van alle gegevens of configuratie-informatie die u zeker niet wilt kwijt raken. De opwaarderingshulpmiddelen en het opwaarderingsproces zijn vrij betrouwbaar, maar een hardwarestoring midden in een opwaardering kan resulteren in een ernstig beschadigd systeem.

De belangrijkste zaken waarvan u een back-up zult willen maken, zijn de inhoud van /etc, /var/lib/dpkg, /var/lib/apt/extended_states en de uitvoer van:

<code>$ dpkg --get-selections '*' # (de aanhalingstekens zijn belangrijk)</code>

<p>We verwelkomen alle informatie van gebruikers met betrekking tot de opwaardering van <q>bullseye</q> naar <q>bookworm</q>. Deel informatie door een bug in te dienen met uw resultaten in het <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.en.html#upgrade-reports">bugvolgsysteem van Debian</a> met behulp van het pakket <b>upgrade-reports</b>.
</p>

<p>
Er werd veel ontwikkeld aan het installatiesysteem van Debian, wat heeft geleid tot verbeterde hardwareondersteuning en andere functies, zoals oplossingen voor de grafische ondersteuning op UTM, reparaties aan de lettertypelader van GRUB, het verwijderen van het lange wachten aan het einde van het installatieproces en reparaties aan de detectie van BIOS-opstartbare systemen. Deze versie van het installatiesysteem van Debian kan waar nodig niet-vrije firmware gebruiken.</p>



<p>
Het pakket <b>ntp</b> werd vervangen door het pakket <b>ntpsec</b>, waarbij de standaard systeemklokdienst nu <b>systemd-timesyncd</b> is>; er
is ook ondersteuning voor <b>chrony</b> en <b>openntpd</b>.
</p>


<p>Omdat <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split"> <b>niet-vrije</b>
firmware verplaatst werd naar zijn eigen component in het archief</a>, is het aanbevolen om <b>non-free-firmware</b> toe te voegen aan uw bronnenlijst van APT als u niet-vrije firmware geïnstalleerd heeft.</p>

<p>Het is raadzaam om vermeldingen van bullseye-backports te verwijderen uit de bronnenlijstbestanden voor APT vóór de opwaardering; overweeg om na de opwaardering <b>bookworm-backports</b> toe te voegen.

<p>
De naam van de veiligheidssuite voor <q>bookworm</q> is <b>bookworm-security</b>; gebruikers moeten hun bronnenlijstbestanden voor APT dienovereenkomstig aanpassen bij het opwaarderen.

Als u in uw APT-configuratie ook gebruik maakt van pinning of van <code>APT::Default-Release</code>, is het waarschijnlijk dat er aanpassingen nodig zijn om de opwaardering van pakketten naar de nieuwe stabiele release mogelijk te maken. Overweeg om <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#disable-apt-pinning">APT-pinning uit te schakelen</a>.
</p>

<p>De opwaardering van OpenLDAP 2.5 bevat een aantal <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#openldap-2.5">incompatibele wijzigingen die mogelijk handmatige tussenkomst vereisen</a>. Afhankelijk van de configuratie kan de dienst <b>slapd</b> na de opwaardering gestopt blijven totdat nieuwe configuratie-updates voltooid zijn.</p>


<p>Het nieuwe pakket <b>systemd-resolved</b> zal bij opwaarderingen niet automatisch geïnstalleerd worden omdat het <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#systemd-resolved">afgesplitst werd in een apart pakket</a>. Indien u de systeemdienst systemd-resolved gebruikt, installeer het nieuwe pakket dan handmatig na de opwaardering, en merk op dat DNS-omzetting mogelijk niet meer werkt totdat het is geïnstalleerd, omdat de dienst niet aanwezig zal zijn op het systeem.</p>



<p>Er zijn wat <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#changes-to-system-logging">veranderingen aan de vastlegging van systeemgegevens in logboeken</a>; op de meeste systemen is het pakket <b>rsyslog</b> niet langer vereist en het wordt standaard niet geïnstalleerd. Gebruikers kunnen overstappen op <b>journalctl</b> of de nieuwe <q>zeer nauwkeurige tijdstempels</q> gebruiken die nu door <b>rsyslog</b> worden gebruikt.
</p>


<p><a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#trouble">Mogelijke problemen tijdens de opwaardering</a> zijn lussen van Conflicts en Pre-Depends die kunnen worden opgelost door sommige pakketten te verwijderen en te elimineren of door de herinstallatie van andere pakketten te forceren. Andere problemen zijn fouten van het type <q>Kan onmiddellijke configuratie niet uitvoeren ...</q> waarvoor men <b>zowel</b> <q>bullseye</q> (dat zojuist werd verwijderd) als < q>bookworm</q> (dat zojuist werd toegevoegd) in het bronnenlijstbestand  voor APT zal moeten behouden, en bestandsconflicten waarvoor het nodig kan zijn om pakketten geforceerd te verwijderen. Zoals gezegd is het maken van een back-up van het systeem de sleutel tot een probleemloze opwaardereing, mochten er onverwachte fouten optreden.</p>


<p>Er zijn enkele pakketten waarbij Debian niet kan beloven minimale backports te leveren voor beveiligingsproblemen. Zie de <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#limited-security-support">Beperkingen in beveiligingsondersteuning</a>.</p>


<p>
Zoals altijd kunnen Debian-systemen probleemloos worden opgewaardeerd terwijl ze blijven functioneren, zonder enige gedwongen uitvaltijd, maar het wordt sterk aanbevolen om de <a href="$(HOME)/releases/bookworm/releasenotes">notities bij de release</a> te lezen alsook de <a href="$(HOME)/releases/bookworm/installmanual">installatiehandleiding</a> voor mogelijke problemen en voor gedetailleerde instructies over installeren en opwaarderen. De notities bij de release zullen in de weken na de release verder worden verbeterd en vertaald naar andere talen.
</p>


<h2>Over Debian</h2>

<p>
Debian is een vrij besturingssysteem dat ontwikkeld wordt door duizenden
vrijwilligers van over de hele wereld die samenwerken via het internet. De
belangrijkste sterke punten van het Debian-project zijn het feit dat het
gedragen wordt door vrijwilligers, de toewijding aan het Debian Sociale
Contract en aan Vrije Software, en het streven om het best mogelijke
besturingssysteem aan te bieden. Deze nieuwe release is weer een belangrijke
stap in die richting.
</p>


<h2>Contactinformatie</h2>

<p>
Bezoek voor meer informatie de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>  of stuur een e-mail naar
&lt;press@debian.org&gt;.
</p>

