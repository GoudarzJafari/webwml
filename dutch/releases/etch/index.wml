#use wml::debian::template title="Debian &ldquo;etch&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="28d2aaba8d42e9ca9808154a8580598385f19912"


<p>Debian GNU/Linux <current_release_etch> werd uitgebracht op
<a href="$(HOME)/News/<current_release_newsurl_etch/>"><current_release_date_etch></a>.
Debian 4.0 werd oorspronkelijk uitgebracht op <:=spokendate('2007-04-08'):>.
De release bevatte verschillende belangrijke wijzigingen, beschreven in ons
<a href="$(HOME)/News/2007/20070408">persbericht</a> en in de
<a href="releasenotes">Notities bij de release</a>.</p>


<p><strong>Debian GNU/Linux 4.0 werd vervangen door
<a href="../lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds eind februari 2010.
</strong></p>


<p>Raadpleeg de installatie-informatie-pagina en de
Installatiehandleiding over het verkrijgen en installeren
van Debian GNU/Linux. Zie de instructies in de
<a href="releasenotes">Notities bij de release</a> om van een oudere Debian release
op te waarderen.</p>

<p>De volgende computerarchitecturen werden in deze release ondersteund:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/amd64/">64-bits pc (amd64)</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd andere problemen rapporteren.</p>
