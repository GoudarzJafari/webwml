#use wml::debian::cdimage title="Debian USB/cd/dvd-images downloaden via HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="019351f156c8f8ffc0f7922929f7062fb4b732db"

# Last Translation Update by: $Author$
# Last Translation Update at: $Date$


<p>De volgende Debian-images zijn beschikbaar om te
downloaden:</p>

<ul>

  <li><a href="#stable">Officiële USB/cd/dvd-images voor de <q>stable</q>
  (stabiele) release</a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Officiële USB/cd/dvd-images voor de distributie <q>testing</q> (<em>wekelijks opnieuw aangemaakt</em>)</a></li>

</ul>

<p>Zie ook:</p>
<ul>

  <li>Een volledige <a href="#mirrors">lijst met
  <tt>debian-cd/</tt>-spiegelservers</a></li>

  <li>Voor <q>netwerkinstallatie</q>-images,
  zie de pagina
  <a href="../netinst/">netwerkinstallatie</a>.</li>

  <li>Voor images van de release <q>testing</q>,
  zie de pagina over
  het <a href="$(DEVEL)/debian-installer/">Debian-Installatiesysteem</a>.</li>

</ul>

<hr />

<h2><a name="stable">Officiële USB/cd/dvd-images voor de <q>stable</q> (stabiele)
    release</a></h2>

<p>Voor de installatie van Debian op een machine zonder (snelle)
Internetverbinding kunt u gebruik maken van cd/USB-images (elk 700&nbsp;MB) of
dvd/USB-images (elk 4.7&nbsp;GB). Download het eerste cd/USB- of dvd/USB-imagebestand,
schrijf het weg met behulp van een USB/cd/dvd-schrijver en herstart daarna uw systeem
vanaf dat medium.</p>

<p>De <strong>eerste</strong> USB/cd/dvd-schijf bevat alle bestanden die nodig
zijn voor de installatie van een standaard Debian systeem.<br /></p>

<div class="line">
<div class="item col50">
<p><strong>cd/USB</strong></p>

<p>Via onderstaande links vindt u imagebestanden tot een grootte
van 700&nbsp;MB geschikt voor het beschrijven van normale cd-r(w)-media:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>dvd/USB</strong></p>

<p>Via onderstaande links vindt u imagebestanden tot een grootte
van 4.7&nbsp;GB geschikt voor het beschrijven van normale dvd-r/dvd+r en
vergelijkbare media:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Bekijk ook de beschikbare documentatie voor u een installatie begint.
<strong>Als u slechts één document wilt lezen</strong> voor u met de
installatie begint, lees dan onze
<a href="$(HOME)/releases/stable/amd64/apa">Installatie Howto</a>, een kort
overzicht van het installatieproces. Andere nuttige documentatie:
</p>

<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installatiehandleiding</a>,
    bevat gedetailleerde installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentatie over het
    Debian Installatiesysteem</a>, waaronder antwoorden op
    veel gestelde vragen (FAQ)</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Errata bij het
    Debian Installatiesysteem</a>, een overzicht van bekende problemen
    in het installatiesysteem</li>
</ul>

<hr />

<h2><a name="mirrors">Geregistreerde spiegelservers voor het
    <q>debian-cd</q>-archief</a></h2>

<p>Merk op dat <strong>sommige spiegelservers niet up-to-date zijn</strong>
&mdash; de huidige release van de "stabiele" USB/cd/dvd-images is <strong><current-cd-release></strong>.</p>

<p><strong>Gebruik bij twijfel de <a href="https://cdimage.debian.org/debian-cd">
primaire cd-imageserver</a> in Zweden.</strong>.</p>

<p>Bent u geïnteresseerd in het aanbieden van Debian cd-images op
uw spiegelserver? Zo ja, bekijk dan <a href="../mirroring/">de instructies voor
het opzetten van een cd-imagespiegelserver</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
