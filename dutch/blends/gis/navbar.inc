#use wml::debian::blends::gis

{#alternate_navbar#:
   <div id="second-nav">
      <p><a href="$(HOME)/blends/gis/">De&nbsp;Debian&nbsp;GIS-uitgave</a></p>
    <ul>
      <li><a href="$(HOME)/blends/gis/about">Over&nbsp;de&nbsp;uitgave</a></li>
      <li><a href="$(HOME)/blends/gis/contact">Contact&nbsp;opnemen</a></li>
      <li><a href="$(HOME)/blends/gis/get/">De&nbsp;uitgave&nbsp;verkrijgen</a>
         <ul>
         <li><a href="$(HOME)/blends/gis/get/metapackages">Het&nbsp;gebruik&nbsp;van&nbsp;de&nbsp;metapakketten</a></li>
	 </ul>
      </li>
      <li><a href="$(HOME)/blends/gis/deriv">Afgeleide&nbsp;distributies</a></li>
      <li><a href="https://wiki.debian.org/DebianGis">Ontwikkeling</a>
        <ul>
        <li><a href="<gis-policy-html/>">Beleid&nbsp;van&nbsp;het&nbsp;GIS-Team</a></li>
        </ul>
      </li>
    </ul>
   </div>
:#alternate_navbar#}
