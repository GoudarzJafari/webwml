#use wml::debian::blend title="De metapakketten gebruiken"
#use "../navbar.inc"
#use wml::debian::translation-check translation="7699c3698cd2627c6da0718bd3329188b02ad8c6"

<p>Metapakketten worden door deze doelgroepspecifieke uitgave gebruikt als een handige manier om verwante softwarepakketten samen te nemen. Bij elk metapakket dat wordt geïnstalleerd, zal het pakketbeheersysteem de pakketten installeren die betrekking hebben op de betrokken taak.</p>

<p>De volgende metapakketten worden momenteel onderhouden door deze doelgroepspecifieke uitgave:</p>

<table>
	<tr><th>Taaknaam</th><th>Metapakket</th><th>Beschrijving</th><th>Catalogus</th></tr>
	<tr>
		<td>gis-data</td>
		<td><a href="https://packages.debian.org/unstable/gis-data"><code>gis-data</code></a></td>
		<td>Deze taak bevat pakketten met gegevens die door verschillende GIS-toepassingen kunnen worden gebruikt.</td>
		<td><a href="https://blends.debian.org/gis/tasks/data">Link</a></td>
	</tr>
	<tr>
		<td>gis-devel</td>
		<td><a href="https://packages.debian.org/unstable/gis-devel"><code>gis-devel</code></a></td>
		<td>Deze taak stelt uw systeem in voor GIS-ontwikkeling.</td>
		<td><a href="https://blends.debian.org/gis/tasks/devel">Link</a></td>
	</tr>
	<tr>
		<td>gis-gps</td>
		<td><a href="https://packages.debian.org/unstable/gis-gps"><code>gis-gps</code></a></td>
		<td>Reeks Debian-pakketten die te maken hebben met GPS-apparaten en -gegevens.</td>
		<td><a href="https://blends.debian.org/gis/tasks/gps">Link</a></td>
	</tr>
	<tr>
		<td>gis-osm</td>
		<td><a href="https://packages.debian.org/unstable/gis-osm"><code>gis-osm</code></a></td>
		<td>Reeks Debian-pakketten die te maken hebben met OpenStreetMap-gegevens.</td>
		<td><a href="https://blends.debian.org/gis/tasks/osm">Link</a></td>
	</tr>
	<tr>
		<td>gis-remotesensing</td>
		<td><a href="https://packages.debian.org/unstable/gis-remotesensing"><code>gis-remotesensing</code></a></td>
		<td>Debian-pakketten die te maken hebben met verwerking (interferometrie, polarimetrie, gegevensvisualisatie, enz.) van remote sensing of teledetectie (bijvoorbeeld Synthetic Aperture-radar -- SAR) en aardobservatie.</td>
		<td><a href="https://blends.debian.org/gis/tasks/remotesensing">Link</a></td>
	</tr>
	<tr>
		<td>gis-statistics</td>
		<td><a href="https://packages.debian.org/unstable/gis-statistics"><code>gis-statistics</code></a></td>
		<td>Reeks Debian-pakketten die nuttig zijn voor de statistische verwerking van geografische gegevens.</td>
		<td><a href="https://blends.debian.org/gis/tasks/statistics">Link</a></td>
	</tr>
	<tr>
		<td>gis-web</td>
		<td><a href="https://packages.debian.org/unstable/gis-web"><code>gis-web</code></a></td>
		<td>Debian-pakketten die te maken hebben met geografische informatie die voor het web moet worden aangeboden op zogenaamde kaarttegelservers. Deze zijn erg handig bij het opzetten van een OpenStreetMap-tegelserver, maar ze zijn niet beperkt tot enkel OpenStreetMap-gegevens.</td>
		<td><a href="https://blends.debian.org/gis/tasks/web">Link</a></td>
	</tr>
	<tr>
		<td>gis-workstation</td>
		<td><a href="https://packages.debian.org/unstable/gis-workstation"><code>gis-workstation</code></a></td>
		<td>Deze taak stelt uw systeem in als een GIS-werkstation om geografische informatie te verwerken en kaarten te maken.</td>
		<td><a href="https://blends.debian.org/gis/tasks/workstation">Link</a></td>
	</tr>
</table>

<p>Om een van de taakmetapakketten te installeren, gebruikt u uw favoriete pakketbeheerprogramma zoals u dat met elk ander Debian-pakket zou doen. Voor
<code>apt-get</code>:</p>

<pre>apt-get install gis-&lt;task&gt;</pre>

<p>Als u de volledige doelgroepspecifieke uitgave wilt installeren:</p>

<pre>apt-get install gis-data gis-devel gis-gps gis-osm gis-remotesensing gis-statistics gis-web gis-workstation</pre>

