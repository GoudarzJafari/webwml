#use wml::debian::translation-check translation="5fc831c6a77ce47c0c4e44da8a3b594b8280e3dc"
<define-tag pagetitle>데비안 12 <q>bookworm</q> 릴리스</define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news

<p>저희 데비안 프로젝트는 1년 9개월 28일의 개발 끝에, 새로운 안정 버전 12를
(코드네임 <q>bookworm</q>) 자랑스럽게 발표합니다.</p>

<p><a href="https://security-team.debian.org/">데비안 보안 팀</a> 및
<a href="https://wiki.debian.org/LTS">데비안 장기 지원</a> 팀의 합동 작업 덕분에,
<q>bookworm</q>은 다음 5년 동안 지원될 예정입니다.
</p>

<p><a href="$(HOME)/vote/2022/vote_003">2022년 비자유 펌웨어에 대한 투표</a>에
뒤이은 조치로, 비자유 펌웨어를 기타 비자유 패키지에서 분리한 새로운 아카이브 영역을
도입합니다.</p>
<ul>
<li>non-free-firmware</li>
</ul>

<p>
대부분의 비자유 펌웨어 패키지는 <b>non-free</b>에서 <b>non-free-firmware</b>로
이동했습니다. 이렇게 구분했기 때문에 여러가지 종류의 공식 설치 이미지를 만들 수 있습니다.
</p>

<p>데비안 12 <q>bookworm</q>에는 다음과 같은 여러 데스크톱 환경이 있습니다:
</p>
<ul>
<li>그놈 43,</li>
<li>KDE Plasma 5.27,</li>
<li>LXDE 11,</li>
<li>LXQt 1.2.0,</li>
<li>MATE 1.26,</li>
<li>Xfce 4.18</li>
</ul>

<p>이번 릴리스에는 <b>11,089</b>개 이상 새로운 패키지가 추가되어 전체
<b>64,419</b>개 패키지가 들어 있습니다. 한편 <b>6,296</b>개 패키지가 더 이상
사용하지 않아 제거되었습니다. <b>43,254</b>개의 패키지가 이번 릴리스에서
업데이트되었습니다.

<q>bookworm</q>의 전체 디스크 사용량은 <b>365,016,420 kB (365 GB)</b>이고, 총
<b>1,341,564,204</b>줄의 코드로 이루어져 있습니다.</p>

<p><q>bookworm</q>에는 지금까지 어떤 릴리스보다 더 많은 맨(man)페이지가
번역되었습니다. 맨페이지를 여러 언어로 사용할 수 있게
만든 번역자들 덕분입니다: 그리스어, 노르웨이어(보크몰), 덴마크어, 러시아어,
마케도니아어, 베트남어, 세르이바어, 스웨덴어, 우크라이나어, 인도네시아어,
체코어, 핀란드어. 이제 모든 <b>systemd</b> 맨페이지를 독일어로 이용할 수
있습니다.</p>

<p>Debian Med 블렌드에 새로운 패키지 <b>shiny-server</b>가 도입되었습니다.
이를 사용해 <b>R</b>을 사용한 과학 웹 앱 사용을 간단히 할 수 있습니다. 데비안
프로젝트는 Debian Med 팀 패키지에 대해 지속적인 통합(CI) 기능을 계속
유지하도록 노력해 왔습니다. 데비안 bookworm에서 버전 3.8.x 메타패키지를
설치할 수 있습니다.</p>

<p>Debian Astro Blend는 직업 천문학자, 애호가, 취미가를 위한 원스톱 솔루션을
계속 제공합니다. 블렌드에 들어 있는 거의 모든 소프트웨어에 버전 업데이트가
있습니다. <b>astap</b> 및 <b>planetary-system-stacker</b> 패키지를 사용해
이미지 스택 및 천문학 해상도를 사용할 수 있습니다. 이제 오픈소스
상관기(correlator)인 <b>openvlbi</b>가 포함되었습니다.</p>

<p>ARM64에 대한 보안 부팅 지원이 다시 도입되었습니다. UEFI 지원 ARM64
하드웨어는 보안 부팅 모드를 켜면 해당 보안 기능을 완전히 활용할 수 있습니다.</p>

<p>데비안 12 <q>bookworm</q>은 다음과 같이 다양한 소프트웨어 패키지가 (이전
릴리스 전체 패키지의 67% 이상) 업데이트되었습니다:
</p>

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS 서버 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (기본 전자메일 서버) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU 컴파일러 모음 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>GNU C 라이브러리 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Linux kernel 6.1 series</li>
<li>LLVM/Clang 툴체인 13.0.1, 14.0 (기본), 및 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>

<p>
이렇게 폭넓게 선택할 수 있는 패키지가 있고, 전통적으로 다양한 아키텍쳐를
지원해 왔으므로, 데비안은 <b>범용 운영 체제</b>가 되려는 목표를 다시 한번 잘
유지하고 있습니다. 데비안은 여러가지로 사용할 수 있습니다: 데스크톱에서
넷북까지; 개발 서버에서 클러스터 시스템까지; 그리고 데이터베이스, 웹, 스토리지
서버까지. 동시에 데비안 아카이브의 모든 패키지에 대해 자동적인 설치 및
업그레이드 테스트를 하는 등 추가적인 품질 관리가 있으므로, 데비안
<q>bookworm</q>은 안정 버전 데비안 릴리스 사용자가 가지고 있는 높은 기대를
충족합니다.
</p>

<p>
<q>bookworm</q>에서 총 9개 아키텍쳐를 공식 지원합니다:
</p>
<ul>
<li>32비트 PC (i386) 및 64비트 PC (amd64),</li>
<li>64비트 ARM (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (EABI hard-float ABI, armhf),</li>
<li>리틀 엔디안 MIPS (mipsel),</li>
<li>64비트 리틀 엔디안 MIPS (mips64el),</li>
<li>64비트 리틀 엔디안 PowerPC (ppc64el),</li>
<li>IBM System z (s390x)</li> 
</ul>

<p>
32비트 PC는 (i386) 이제 i586 프로세서를 지원하지 않습니다. 새로운 최소 요구
프로세서는 i686입니다. <i>컴퓨터가 이 요구 사항에 호환되지 않으면, 지원이 끝날
때까지 bullseye에 계속 머물러 있기를 추천합니다.</i>
</p>

<p>데비안 클라우드 팀에서 여러가지 클라우드 컴퓨팅 서비스에 대한 <q>bookworm</q>
이미지를 발표합니다:
</p>
<ul>
<li>Amazon EC2 (amd64 및 arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (generic) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amd64, arm64, ppc64el)</li>
</ul>
genericcloud 이미지는 모든 가상화 환경에서 쓸 수 있고, nocloud 이미지는
빌드 프로세스를 테스트할 때 유용합니다.
</p>

<p>GRUB 패키지는 이제 기본값으로 <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#grub-os-prober">다른
운영 체제를 찾는데 os-prober를 실행하지 않습니다.</a></p>

<p>이전 릴리스 이후, 기술 위원회에서는 데비안 <q>bookworm</q>에서 <a
href="https://wiki.debian.org/UsrMerge">usr 통합 루트 파일시스템 레이아웃만 지원</a>한다고
결정했습니다. 즉 usr 통합되지 않은 레이아웃 지원을 중단합니다. buster
또는 bullseye에서 설치된 시스템에서는 파일시스템에 변화가 없을 것입니다.
하지만 그보다 오래 전 레이아웃의 경우에는 업그레이드하면서 변환됩니다.</p>

<h3>한번 시험해 보고 싶으신가요?</h3>
<p>
데비안 12 <q>bookworm</q>을 설치하지 않고 써 보려면,
<a href="$(HOME)/CD/live/">라이브 이미지</a> 중에 하나를 사용해 볼 수 있습니다.
라이브 이미지는 전체 운영 체제를 컴퓨터 메모리를 사용해 읽기 전용 상태로 읽어들여
실행합니다.
</p>

<p>
라이브 이미지는 <code>amd64</code> 및 <code>i386</code> 아키텍쳐용이 있고,
DVD, USB 메모리, netboot 셋업 이미지를 사용할 수 있습니다. 여러가지 데스크톱
환경 중에 하나를 골라서 사용할 수 있습니다: 그놈, KDE 플라스마, LXDE, LXQt, MATE, Xfce.
데비안 라이브 <q>bookworm</q>의 standard 라이브 이미지를 사용하면, 그래픽 사용자 인터페이스 없이
베이스 데비안 시스템을 써 볼 수도 있습니다.
</p>

<p>
라이브 이미지에서 컴퓨터 하드 디스크로 운영 체제를 설치하는 방법도 있습니다.
라이브 이미지에는 표준 데비안 설치 프로그램 외에 독립적인 Calamares 설치 프로그램이 들어 있습니다.
자세한 정보는 데비안 웹사이트의
<a href="$(HOME)/releases/bookworm/releasenotes">릴리스 노트</a> 및
<a href="$(HOME)/CD/live/">라이브 설치 이미지</a> 부분에 있습니다.
</p>

<p>
데비안 12 <q>bookworm</q>을 직접 컴퓨터의 저장 장치에 설치하려면, 여러가지 설치 미디어 종류 중에 하나를 골라
<a href="https://www.debian.org/download">다운로드</a>합니다. 블루레이 디스크, DVD, CD, USB 메모리,
네트워크 연결 사용과 같은 미디어 종류가 있습니다.

자세한 정보는 <a href="$(HOME)/releases/bookworm/installmanual">설치 안내서</a>를 참고하십시오.
</p>

# Translators: some text taken from: 

<p>
데비안은 이제 78개 언어로 설치할 수 있습니다. 대부분 텍스트 기반 및 그래픽 사용자
인터페이스 모두에서 사용할 수 있습니다.
</p>

<p>
설치 이미지는 다음 방법으로 다운로드할 수 있습니다:
<a href="$(HOME)/CD/torrent-cd/">비트토렌트</a> (추천하는 방법),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, 또는
<a href="$(HOME)/CD/http-ftp/">HTTP</a>;
자세한 정보는 <a href="$(HOME)/CD/">데비안 CD</a>를 참고하십시오. <q>bookworm</q>은
곧 여러 <a href="$(HOME)/CD/vendors">벤더</a>에서 물리적인 DVD, CD-ROM,
블루레이 디스크로도 구할 수 있을 것입니다.
</p>

<h3>데비안 업그레이드하기</h3>
<p>
이전 릴리스인 데비안 11 <q>bullseye</q>에서, 데비안 12 <q>bookworm</q>으로 업그레이드는
대부분의 환경에서 자동으로 APT 패키지 관리 도구에서 처리합니다.
</p>

<p>시스템을 업그레이드하기 전에 전체 백업을 추천합니다. 아니면 최소한 잃어버리면
안 되는 모든 데이터와 설정 정보 백업을 추천합니다. 업그레이드 도구와
업그레이드 과정은 상당히 믿을만 하지만, 업그레이드 중에 하드웨어 실패가 있으면
심각하게 시스템이 손상될 수 있습니다.

백업할 주요 사항은 /etc, /var/lib/dpkg, /var/lib/apt/extended_states의 내용과
다음 출력입니다:

<code>$ dpkg --get-selections '*' # (따옴표가 꼭 필요합니다)</code>

<p><q>bullseye</q>에서 <q>bookworm</q>으로 업그레이드할 때 관계된 모든 정보를 환영합니다.
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.en.html#upgrade-reports">데비안
버그 추적 시스템</a>에 설치 결과를 <b>upgrade-reports</b> 패키지의 버그로 추가해서
해당 정보를 공유하십시오.
</p>

<p>
데비안 설치 프로그램에 아주 많은 개발 노력이 있었습니다. 그 결과 하드웨어
지원과 기타 여러가지 기능이 발전했습니다. UTM에서 그래픽 지원, GRUB 글꼴
읽어들이기 수정, 설치가 끝난 뒤에 기다리는 시간 줄이기, BIOS 부팅 가능 시스템
검색 수정 등이 있습니다. 이 버전의 데비안 설치 프로그램은 필요에 따라
non-free-firmware를 사용할 수도 있습니다.
</p>


<p>
이제 <b>systemd-timesyncd</b>가 기본 시스템 시계 서비스가 되면서, <b>ntp</b>
패키지는 <b>ntpsec</b> 패키지로 대체되었습니다. <b>chrony</b> 및 <b>openntpd</b>도
지원합니다.
</p>

<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split"> <b>non-free</b> 
펌웨어가 아카이브에서 전용 컴포넌트로 이동</a>함에 따라, 비자유 펌웨어가 설치되어 있으면
APT source 목록에 <b>non-free-firmware</b> 추가를 추천합니다.</p>

<p>업그레이드 전에 APT source 목록 파일에서 bullseye-backports 항목 제거를 권합니다.
업그레이드 뒤에 <b>bookworm-backports</b>를 추가해 보십시오.

<p>
<q>bookworm</q>에서, 보안 패치 모음의 이름은 <b>bookworm-security</b>입니다; 업그레이드하면
APT sources.list 파일을 맞춰야 합니다.

APT 설정에 피닝(pinning) 또는 <code>APT::Default-Release</code> 설정이 들어 있다면,
새로운 안정 릴리스에 맞게 패키지를 업그레이드하려면 설정을 조정해야 할 것입니다.
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#disable-apt-pinning">APT 피닝 기능 끄기</a>를
고려해 보십시오.
</p>

<p>OpenLDAP 2.5 업그레이드에는 일부 <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#openldap-2.5">수동 개입이 필요할 수도 있는
비호환 변경 사항</a>이 있습니다. <b>slapd</b> 설정에 따라 업그레이드 뒤에
새로운 설정을 마치지 않으면, 서비스가 멈춘 상태로 남아 있을 수도 있습니다.</p>

<p>새로운 <b>systemd-resolved</b> 패키지는
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#systemd-resolved">별도의 패키지로 분리되었기 때문에</a>
업그레이드할 때 자동으로 설치하지 않습니다. systemd-resolved 시스템 서비스를 사용하는 경우,
업그레이드 뒤에 수동으로 새 패키지를 설치하십시오.
설치 전에는 DNS 서비스가 시스템에 없기 때문에 DNS 찾기가 동작하지 않을 수도 있습니다.</p>

<p>
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#changes-to-system-logging">시스템 로그에 변화</a>가 있습니다;
이제 대부분의 시스템에서 <b>rsyslog</b> 패키지는 필요하지 않고, 그러므로 기본으로 설치하지 않습니다.
사용자들은 <b>journalctl</b>로 바꾸거나 현재 버전의 <b>rsyslog</b>에서 사용하는
새로운 <q>고정밀 타임스탬프<q>를 사용할 수 있습니다.
</p>

<p><a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#trouble">업그레이드 중 발생할 수 있는 문제</a>
중에서 충돌 또는 미리 의존성의 반복은 일부 패키지를 제거하거나 기타 패키지를 강제로 다시 설치하는 것으로 해결할 수 있습니다.
기타 문제는 <q>Could not perform immediate configuration ...</q> 오류가 나면
(막 제거한) <q>bullseye</q> 및 (막 추가한) <b>bookworm</b>을 <b>모두</b>
APT source 목록 파일에 유지해야 할 수도 있습니다. 그리고 파일 충돌이 일어나면 강제로 패키지를 제거해야 할 수도 잇습니다.
앞에서 말한 것처럼, 뜻하지 않은 오류가 발생했을 때 원활하게 업그레이드할 수
있으려면 시스템 백업이 핵심입니다.</p> 


<p>보안 문제에 대해 최소한의 백포팅을 유지할 수 없었던 패키지가 있습니다.
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#limited-security-support">보안 지원 limitation</a>
페이지를 참고하십시오.</p>


<p>
항상 그렇듯, 데비안 시스템은 큰 수고를 들이지 않고 강제적인 다운 타임 없이 기존
시스템을 업그레이드할 수 있습니다. 하지만 발생할 수 있는 문제 및 설치와
업그레이드에 대한 자세한 정보는
<a href="$(HOME)/releases/bookworm/releasenotes">릴리스 노트</a> 및
<a href="$(HOME)/releases/bookworm/installmanual">설치 안내서</a>를 읽어
보기를 강력히 권장합니다. 릴리스 노트는 릴리스 뒤 몇 주 동안 추가로 개선되거나
번역될 수 있습니다.
</p>

<h2>데비안 정보</h2>

<p>
데비안은 수천명의 자원 봉사자가 인터넷을 통해 협동해 개발하는 자유 소프트웨어 운영
체제입니다. 데비안 프로젝트의 강점은 자원 봉사자 기반, 데비안 사회 계약 및
자유 소프트웨어에 대한 헌신과 최고의 운영 체제를 제공하겠다는 약속입니다.
이번의 새로운 릴리스는 그러한 방향으로 가능 중요한 한 걸음이 될 것입니다.
</p>


<h2>연락처 정보</h2>

<p>
더 많은 정보가 필요하면, 데비안 홈 페이지를 (<a href="$(HOME)/">https://www.debian.org/</a>)
방문해 보시거나, &lt;press@debian.org&gt; 주소로 메일을 보내십시오.
</p>
