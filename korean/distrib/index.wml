#use wml::debian::template title="데비안 구하기"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="47e0a86f4e71194a409b72e7ed1faaa3dc3160d5"

<p>이 페이지에서는 데비안 안정 버전을 설치하는 여러가지 방법을 설명합니다.


<ul>
<li> 설치 이미지의 <a href="../CD/http-ftp/#mirrors">미러 다운로드</a>
<li> <a href="../releases/stable/installmanual">설치 설명서</a>, 자세한 설치 안내가 들어 있습니다
<li> <a href="../releases/stable/releasenotes">릴리스 노트</a>
+<li> <a href="../devel/debian-installer/">데비안 테스팅 ISO 이미지</a>
+<li> <a href="../CD/verify">데비안 이미지 무결성 확인하기</a>
</ul>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">설치 이미지 다운로드</a></h2>
    <ul>
      <li><a href="netinst"><strong>작은 설치 이미지</strong></a>:
            빨리 다운로드할 수 있고 이동식 디스크에 기록되어야 합니다.
            작은 설치 이미지를 이용하려면 인터넷에 연결된 컴퓨터가 필요합니다.
	<ul class="quicklist downlist">
	  <li><a title="64비트 인텔 및 AMD PC를 위한 설치관리자 다운로드"
                 href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64비트 PC용 네트워크 설치 ISO</a></li>
          <li><a title="일반 32비트 인텔 및 AMD PC를 위한 설치관리자 다운로드"
                 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32비트 PC용 네트워크 설치 ISO</a></li>
          <li><a title="64비트 Intel 및 AMD PC를 위한 CD 토렌트 다운로드"
                 href="<stable-images-url/>/amd64/bt-cd/">64비트 PC용 토렌트 (CD)</a></li>
	  <li><a title="일반 32비트 Intel과 AMD PC를 위한 CD 토렌트 다운로드"
		 href="<stable-images-url/>/i386/bt-cd/">32비트 PC용 토렌트 (CD)</a></li>
	</ul>
      </li>
      <li>더 큰 <a href="../CD/"><strong>완전한 설치 이미지</strong></a>:
        더 많은 패키지가 들어있고, 인터넷 연결 없이 더 쉽게 설치할 수 있습니다.
	<ul class="quicklist downlist">
          <li><a title="64비트 Intel 및 AMD PC를 위한 데비안 설치 관리자"
                href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">64비트
              PC DVD-1 iso</a></li>
          <li><a title="32비트 Intel 및 AMD PC를 위한 데비안 설치 관리자"
                href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">32비트
              PC DVD-1 iso</a></li>
	  <li><a title="64비트 Intel 및 AMD PC를 위한 DVD 토렌트 다운로드"
                 href="<stable-images-url/>/amd64/bt-dvd/">64비트 PC용 토렌트 (DVD)</a></li>
	  <li><a title="일반 32비트 Intel 및 AMD PC를 위한 DVD 토렌트 다운로드"
		 href="<stable-images-url/>/i386/bt-dvd/">32비트 PC용 토렌트 (DVD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">설치 전에 데비안 라이브 써 보기</a></h2>
    <p>
      CD, DVD 또는 USB 메모리에서 라이브 시스템을 부팅하면, 컴퓨터에 파일을 전혀 설치하지
      않아도 데비안을 써 볼 수 있습니다. 또 라이브 시스템에 포함된
      <a href="https://calamares.io">칼라마레스 설치 관리자</a>를 써 볼 수 있습니다.
      64비트 PC에서만 사용할 수 있습니다.
      <a href="../CD/live#choose_live">이 방법에 대해서 정보를</a> 더 읽어 보십시오.
    </p>
    <ul class="quicklist downlist">
      <li><a title="64비트 인텔 및 AMD PC용 그놈 라이브 ISO 다운로드"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="64비트 인텔 및 AMD PC용 XFCE 라이브 ISO 다운로드"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="64비트 인텔 및 AMD PC용 KDE 라이브 ISO 다운로드"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="64비트 인텔 및 AMD PC용 기타 라이브 ISO 다운로드"
            href="<live-images-url/>/amd64/iso-hybrid/">Other live ISO</a></li>
      <li><a title="64비트 인텔 및 AMD PC용 기타 라이브 토렌트 다운로드"
          href="<live-images-url/>/amd64/bt-hybrid/">live torrents</a></li>
    </ul>
  </div>
</div>

<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">데비안 설치 미디어 공급 업체에서
      CD, DVD, USB 메모리 구매</a></h2>

   <p>
     많은 공급 업체들이 배포판을 5달러 이하의 가격에 배송비를 추가해 팝니다.
     (웹 페이지에서 국제 배송 여부 확인).
   </p>

   <p>CD의 기본적인 장점은 아래와 같습니다:</p>

   <ul>
     <li>인터넷에 연결하지 않고 컴퓨터에 설치할 수 있습니다.</li>
     <li>모든 패키지를 직접 다운로드하지 않고도 데비안을 설치할 수 있습니다.</li>
   </ul>

    <h2><a href="pre-installed">데비안이 설치된 컴퓨터 구매</a></h2>
   <p>컴퓨터 구매는 여러 장점이 있습니다:</p>
   <ul>
    <li>데비안 설치 안 해도 됩니다.</li>
    <li>하드웨어에 맞게 미리 구성되어 설치됩니다.</li>
    <li>공급업체가 기술지원을 할 수도 있습니다.</li>
   </ul>
</div>

<div class="item col50 lastcol">
  <h2><a href="https://cloud.debian.org/images/cloud/">데비안 클라우드 이미지를 쓰세요</a></h2>
    <p>공식 <a href="https://cloud.debian.org/images/cloud/"><strong>클라우드 이미지</strong></a>, 클라우드 팀이 빌드하고, 다음에 사용 가능합니다:</p>
    <ul>
      <li>오픈스택 공급자, qcow2 또는 raw 형식.
      <ul class="quicklist downlist">
        <li>64비트 AMD/인텔 (<a title="64비트 AMD/인텔 오픈스택 이미지 qcow2" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="64비트 AMD/인텔 오픈스택 이미지 raw" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
        <li>64비트 ARM (<a title="64비트 ARM 오픈스택 이미지 qcow2" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="64비트 ARM 오픈스택 이미지 raw" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
        <li>64비트 리틀 엔디안 PowerPC (<a title="64비트 리틀 엔디안 PowerPC 오픈스택 이미지 qcow2" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="64비트 리틀 엔디안 PowerPC 오픈스택 이미지 raw" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>로컬 QEMU 가상 기계, qcow2 또는 raw 형식.
      <ul class="quicklist downlist">
        <li>64비트 AMD/인텔 (<a title="64비트 AMD/인텔 QEMU 이미지 qcow2" href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a title="64비트 AMD/인텔 QEMU 이미지 raw" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
        <li>64비트 ARM (<a title="64비트 ARM QEMU 이미지 qcow2" href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a title="64비트 ARM QEMU 이미지 raw" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
        <li>64비트 리틀 엔디안 PowerPC (<a title="64비트 리틀 엔디안 PowerPC QEMU 이미지 qcow2" href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a title="64비트 리틀 엔디안 PowerPC QEMU 이미지 raw" href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>아마존 EC2, 컴퓨터 이미지 또는 AWS 마켓 플레이스를 통해.
	   <ul class="quicklist downlist">
	    <li><a title="Amazon 머신 이미지" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon 머신 이미지</a></li>
	    <li><a title="AWS 마켓 플레이스" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS 마켓 플레이스</a></li>
	   </ul>
      </li>
      <li>마이크로소프트 Azure, Azure 마켓 플레이스를 통해.
	   <ul class="quicklist downlist">
	    <li><a title="Azure 마켓 플레이스의 데비안 12" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">데비안 12 ("Bookworm")</a></li>
	    <li><a title="Azure 마켓 플레이스의 데비안 11" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">데비안 11 ("Bullseye")</a></li>
	   </ul>
      </li>

  </div>
</div>
