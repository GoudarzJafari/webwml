#use wml::debian::translation-check translation="1c49456c19b64e777ff4c1303f2d1e5402da4775" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 12 aktualisiert: 12.4 veröffentlicht</define-tag>
<define-tag release_date>2023-12-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>


<p>Wir weisen darauf hin, dass dieser Text nachbearbeitet wurde, weil Debian 12.3
durch Debian 12.4 ersetzt worden ist. Die Änderungen haben sich ergeben, weil
in letzter Minute eine Warnung vor dem Fehler Nummer <a href=https://bugs.debian.org/1057843>#1057843</a>
in linux-image-6.1.0-14 (6.1.64-1) herausgegeben worden war.</p>

<p>Debian 12.4 wird mit dem linux-image-6.1.0-15 (6.1.66-1) und einigen anderen
Fehlerkorrekturen veröffentlicht.</p>

<p>
Das Debian-Projekt freut sich, die zweite Aktualisierung seiner Stable-Distribution
Debian <release> (Codename <q><codename></q>) ankündigen zu können. Diese
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken der Stable-Veröffentlichung
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen
veröffentlicht worden, auf die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt.
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>


<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Stable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction adequate "Symbolgrößenvergleich (symbol-size-mismatch test) auf Architekturen, auf denen Array-Symbole keine spezifische Länge haben, überspringen; Missbilligungs-Warnungen wegen smartmatch unter Perl 5.38 abgestellt; Warnungen des Versionsvergleichs wegen des Experimentalstatus von smartwatch abgestellt">
<correction amanda "Lokale Privilegieneskalation behoben [CVE-2023-30577]">
<correction arctica-greeter "Bei der Begrüßung das Logo vom Rand wegbewegen">
<correction awstats "Nachfragen beim Upgrade wegen Konfigurations-Entrümpelung von logrotate vermeiden">
<correction axis "Nicht unterstützte Protokolle in der Client-Klasse ServiceFactory ausfiltern [CVE-2023-40743]">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung 12.4">
<correction ca-certificates-java "Kreisabhängigkeit aufgelöst">
<correction calibre "Absturz in Get Books beim Regenerieren der UIC-Dateien behoben">
<correction crun "Container mit systemd als Init-System bei Verwendung neuerer Kernel-Versionen überarbeitet">
<correction cups "Berücksichtigen, dass bei manchen Druckern die Farbdruck-Option fürs ColorModel nicht RGB, sondern CMYK ist">
<correction dav4tbsync "Neue Version der Originalautoren, welche die Kompatibilität mit neueren Thunderbird-Versionen wiederherstellt">
<correction debian-edu-artwork "Bilder mit Emerald-Thema für Debian Edu 12 vereitgestellt">
<correction debian-edu-config "Neue Version der Originalautoren; Setzen und Ändern von LDAP-Passwörtern überarbeitet">
<correction debian-edu-doc "Enthaltene Dokumentation und Übersetzungen aktualisiert">
<correction debian-edu-fai "Neue Version der Originalautoren">
<correction debian-edu-router "Erzeugung der dnsmasq-Konfiguration für Netzwerke über VLAN korrigiert; UIF-Filterregeln für SSH nur dann erzeugen, wenn eine »Uplink«-Schnittstelle definiert worden ist; Übersetzungen überarbeitet">
<correction debian-installer "Kernel-ABI auf 6.1.0-15 angehoben; Neukompilierung gegen proposed-updates">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debootstrap "Änderungen an der Unterstützung für merged-/usr aus Trixie zurückportiert: merged-/usr durch post-merging erstellen, standardmäßig merged-/usr für Suites nach Bookworm in allen Profilen verwenden">
<correction devscripts "Debchange: Auf aktuelle Debian-Distributionen aktualisiert">
<correction dhcpcd5 "Beschädigt/Ersetzt dhcpcd5 auf Konflikt mit dhcpcd5 abgeändert">
<correction di-netboot-assistant "Unterstützung für Bookworm-Live-ISO-Abbild korrigiert">
<correction distro-info "Tests für distro-info-data 0.58+deb12u1 aktualisiert, welches das Lebensdauer-Enddatum von Debian 7 aktualisiert hat">
<correction distro-info-data "Ubuntu 24.04 LTS Noble Numbat hinzugefügt; mehrere Lebensdauer-Enddaten korrigiert">
<correction eas4tbsync "Neue Version der Originalautoren, welche die Kompatibilität mit neueren Thunderbird-Versionen wiederherstellt">
<correction exfatprogs "Speicherzugriffe außerhalb der Grenzen behoben [CVE-2023-45897]">
<correction exim4 "Sicherheitsprobleme im Zusammenhang mit dem Proxy-Protokoll [CVE-2023-42117] und DNSDB-Abfragen [CVE-2023-42119] behoben; Härtung für SPF-Abfragen hinzugefügt; UTF16-Ersetzungen aus ${utf8clean:...} verbieten; Absturz mit <q>tls_dhparam = none</q> behoben; $recipients-Erweiterung bei Verwendung in ${run...} geradegezogen; Ablaufdatum automatisch generierter SSL-Zertifikate überarbeitet; Absturz durch einige Kombinationen von Zeichenketten mit Nuller-Länge und ${tr...} behoben">
<correction fonts-noto-color-emoji "Unterstützung für Unicode 15.1 hinzugefügt">
<correction gimp "Konflikt- und Ersetzt-Abhängigkeit von gimp-dds hinzugefügt, um alte Versionen dieses Plugins, welches gimp seit 2.10.10 selbst mitliefert, zu entfernen">
<correction gnome-characters "Unterstützung für Unicode 15.1 hinzugefügt">
<correction gnome-session "Textdateien im gnome-text-editor öffnen, falls gedit nicht installiert ist">
<correction gnome-shell "Neue stabile Veröffentlichung der Originalautoren; ermöglicht, Benachrichtigungen nicht nur mit der Entfernen-, sondern auch mit der Backspace-Taste zu verwerfen; doppelte Anzeige der Geräte nach Wiederverbindung mit PulseAudio behoben; mögliche Use-after-free-Abstürze nach Neustart von PulseAudio/Pipewire behoben; Schieberegler in den Schnelleinstellungen (Lautstärke usw.) den Bedienungshilfen nicht als ihr eigenes Elternobjekt melden; gescrollte Viewports am Pixelraster anlegen, um sichtbaren Jitter beim Scrollen zu vermeiden">
<correction gnutls28 "Timing-Seitenkanal geschlossen [CVE-2023-5981]">
<correction gosa "Neue stabile Veröffentlichung der Originalautoren">
<correction gosa-plugins-sudo "Uninitialisierte Variable behoben">
<correction hash-slinger "Erzeugung von TLSA-Einträgen überarbeitet">
<correction intel-graphics-compiler "Kompatibilität mit intel-vc-intrinsics-Version in Stable verbessert">
<correction iotop-c "Logik in der <q>only</q>-Option korrigiert; Aktivitätsschleife nach Drücken von ESC behoben; ASCII-Graph-Darstellung überarbeitet">
<correction jdupes "Abfragen aktualisiert, um Auswahlen, die zu unerwarteten Datenverlusten führen können, vermeiden zu helfen">
<correction lastpass-cli "Neue stabile Veröffentlichung der Originalautoren; Zertifikat-Hashes aktualisiert; Unterstützung fürs Lesen verschlüsselter URLs hinzugefügt">
<correction libapache2-mod-python "Sichergestellt, dass die binNMU-Versionen PEP-440-konform sind">
<correction libde265 "Segmentierungsverletzung [CVE-2023-27102], Pufferüberläufe [CVE-2023-27103 CVE-2023-47471], übermäßiges Pufferauslesen [CVE-2023-43887] behobebn">
<correction libervia-backend "Startfehlschlag bei nicht schon vorhandener Konfiguration behoben; Auführungspfad in dbus-Dienstdateimake auf einen absoluten umgestellt; Abhängigkeiten von python3-txdbus/python3-dbus übrerarbeitet">
<correction libmateweather "Orte: San Miguel de Tucuman (Argentinien) hinzugefügt; Vorhersagezonen für Chicago aktualisiert; Datenserver-URL aktualisiert; einige Ortsnamen korrigiert">
<correction libsolv "Unterstützung für zstd-Kompirimierung hinzugefügt">
<correction linux "Aktualisierung auf Veröffentlichung 6.1.66 der Originlautoren; ABI auf 15 angehoben; [rt] Aktualisiert auf 6.1.59-rt16; X86_PLATFORM_DRIVERS_HP aktiviert; nvmet: NQNs, die im connect-Befehl übergeben werden, nul-terminieren [CVE-2023-6121]">
<correction linux-signed-amd64 "Aktualisierung auf Veröffentlichung 6.1.66 der Originlautoren; ABI auf 15 angehoben; [rt] Aktualisiert auf 6.1.59-rt16; X86_PLATFORM_DRIVERS_HP aktiviert; nvmet: NQNs, die im connect-Befehl übergeben werden, nul-terminieren [CVE-2023-6121]">
<correction linux-signed-arm64 "Aktualisierung auf Veröffentlichung 6.1.66 der Originlautoren; ABI auf 15 angehoben; [rt] Aktualisiert auf 6.1.59-rt16; X86_PLATFORM_DRIVERS_HP aktiviert; nvmet: NQNs, die im connect-Befehl übergeben werden, nul-terminieren [CVE-2023-6121]">
<correction linux-signed-i386 "Aktualisierung auf Veröffentlichung 6.1.66 der Originlautoren; ABI auf 15 angehoben; [rt] Aktualisiert auf 6.1.59-rt16; X86_PLATFORM_DRIVERS_HP aktiviert; nvmet: NQNs, die im connect-Befehl übergeben werden, nul-terminieren [CVE-2023-6121]">
<correction llvm-toolchain-16 "Neues zurückportiertes Paket, um das Kompilieren neuerer Chromium-Versionen zu unterstützen">
<correction lxc "Erzeugung flüchtiger Kopien überarbeitet">
<correction mda-lv2 "Ort der LV2-Plugin-Installation korrigiert">
<correction midge "Unfreie Beispieldateien entfernt">
<correction minizip "Ganzzahl- und Heap-Überläufe behoben [CVE-2023-45853]">
<correction mrtg "Umplatzierung der Konfigurationsdatei verarbeiten; Übersetzungen aktualisiert">
<correction mutter "Neue stabile Veröffentlichung der Originalautoren; Unterstützung für Verschieben der libdecor-Fenster mit ihrer Titelleiste auf berührungsempfindlichen Bildschirmen überarbeitet; Flackern und Render-Artefakte bei Verwendung von Software-Rendering behoben; GNOME-Shell-App-Gitter-Geschwindigkeit durch Vermeiden der Neuzeichnung von Bildschirmen, auf denen es nicht dargestellt wird, verbessert">
<correction nagios-plugins-contrib "Ermittlung der Kernel-Version auf der Platte überarbeitet">
<correction network-manager-openconnect "User Agent für Openconnect-VPN für NetworkManager hinzugefügt">
<correction node-undici "Cookie- und Host-Kopfzeilen bei ursprungsübergreifender Weiterleitung (cross-origin redirect) verwerfen [CVE-2023-45143]">
<correction nvidia-graphics-drivers "Neue Veröffentlichung der Originalautoren; Nullzeiger-Dereferenzierung behoben [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla "Neue Veröffentlichung der Originalautoren; Nullzeiger-Dereferenzierung behoben [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "Neue Veröffentlichung der Originalautoren; Nullzeiger-Dereferenzierung behoben [CVE-2023-31022]">
<correction nvidia-open-gpu-kernel-modules "Neue Veröffentlichung der Originalautoren; Nullzeiger-Dereferenzierung behoben [CVE-2023-31022]">
<correction opendkim "Entfernung eingehender Authentication-Results-Kopfzeilen überarbeitet [CVE-2022-48521]">
<correction openrefine "Codeausführung aus der Ferne unterbunden [CVE-2023-41887 CVE-2023-41886]">
<correction opensc "Lesezugriff außerhalb der Grenzen [CVE-2023-4535], potenzielle PIN-Umgehung [CVE-2023-40660], Probleme bei Speicherverwaltung [CVE-2023-40661] behoben">
<correction oscrypto "Fix OpenSSL version parsing; fix autopkgtest">
<correction pcs "<q>Resource move</q> (Ressourcenverschiebung) behoben">
<correction perl "Pufferüberlauf behoben [CVE-2023-47038]">
<correction php-phpseclib3 "Dienstblockade behoben [CVE-2023-49316]">
<correction postgresql-15 "Neue stabile Veröffentlichung der Originalautoren; SQL-Injektion unterbunden [CVE-2023-39417]; MERGE überarbeitet, um Sicherheitsrichtlinien auf Zeilen ordnungsgemäß durchzusetzen [CVE-2023-39418]">
<correction proftpd-dfsg "Größe der Puffer für SSH-Schlüsselaustausch angepasst">
<correction python-cogent "Bei Ausführung auf einem Einzel-CPU-System nur die Tests überspringen, die mehrere CPUs erfordern">
<correction python3-onelogin-saml2 "Abgelaufene Test-Nutzlasten korrigiert">
<correction pyzoltan "Kompilierung auf Ein-Kern-Systemen unterstützen">
<correction qbittorrent "UPnP für die Weboberfläche in qbittorrent-nox standardmäßig deaktiviert">
<correction qemu "Aktualisierung auf Veröffentlichung 7.2.7 der Originalautoren; hw/scsi/scsi-disk: Keine Blockgrößen unterhalb 512 zulassen [CVE-2023-42467]">
<correction qpdf "Datenverlust bei einigen zitierten Oktettzeichenketten behoben">
<correction redis "Härtungsschalter ProcSubset=pid aus der systemd-Unit entfernt, weil er Abstürze verursacht hat">
<correction rust-sd "Sichergestellt, dass die Binärpaketversionen relativ zu älteren Veröffentlichungen (die aus einem anderen Quellpaket erstellt wurden) richtig sortiert werden">
<correction sitesummary "Zum Ausführen von sitesummary-client einen systemd-Timer verwenden, falls verfügbar">
<correction speech-dispatcher-contrib "voxin auf armhf und arm64 aktiviert">
<correction spyder "Automatische Konfiguration der Oberflächensprache überarbeitet">
<correction symfony "Sitzungsfixierung abgestellt [CVE-2023-46733]; fehlende Maskierung hinzugefügt [CVE-2023-46734]">
<correction systemd "Neue stabile Veröffentlichung der Originalautoren">
<correction tbsync "Neue Version der Originalautoren, welche die Kompatibilität mit neueren Thunderbird-Versionen wiederherstellt">
<correction toil "Nur einen Kern für Tests anfragen">
<correction tzdata "Liste der Schaltsekunden aktualisiert">
<correction unadf "Pufferüberlauf abgestellt [CVE-2016-1243]; Codeausführung unterbunden [CVE-2016-1244]">
<correction vips "Nullzeiger-Dereferenzierung abgestellt [CVE-2023-40032]">
<correction weborf "Dienstblockade behoben">
<correction wormhole-william "Unzuverlässige Tests deaktiviert, um Kompilierungsprobleme zu beseitigen">
<correction xen "Neue stabile Version der Originalautoren; mehrere Sicherheitsprobleme behoben [CVE-2022-40982 CVE-2023-20569 CVE-2023-20588 CVE-2023-20593 CVE-2023-34320 CVE-2023-34321 CVE-2023-34322 CVE-2023-34323 CVE-2023-34325 CVE-2023-34326 CVE-2023-34327 CVE-2023-34328 CVE-2023-46835 CVE-2023-46836]">
<correction yuzu ":native von den glslang-tools-Kompilierabhängigkeiten entfernt, um Kompilierfehlschläge zu beheben">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5521 tomcat10>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5525 samba>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5529 slurm-wlm-contrib>
<dsa 2023 5529 slurm-wlm>
<dsa 2023 5531 roundcube>
<dsa 2023 5532 openssl>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5541 request-tracker5>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 jtreg6>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5552 ffmpeg>
<dsa 2023 5553 postgresql-15>
<dsa 2023 5555 openvpn>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5559 wireshark>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5562 tor>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5568 fastdds>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
</table>

<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernt, die sich unserer Kontrolle entziehen:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction gimp-dds "Nicht länger nötig, da in GIMP integriert">
</table>


<h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält,
die mit dieser Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern
Freier Software, die ihre Zeit und Mühen einbringen, um das
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das
Stable-Veröffentlichungs-Team (auf Englisch)
unter &lt;debian-release@lists.debian.org&gt;.</p>




