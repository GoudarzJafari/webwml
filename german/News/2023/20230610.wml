#use wml::debian::translation-check translation="5fc831c6a77ce47c0c4e44da8a3b594b8280e3dc" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 12 <q>Bookworm</q> veröffentlicht</define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news

<p>Nach einem Jahr, neun Monaten und 28 Tagen Entwicklungszeit darf das
Debian-Projekt nun mit Stolz seine neue stabile Version 12 (Codename <q>Bookworm</q>) ankündigen.</p>

<p><q>Bookworm</q> enthält dank des gemeinsamen Engagements des <a href="https://security-team.debian.org/">Debian-Sicherheitsteams</a>
und des <a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>-Teams Unterstützung für die nächsten fünf Jahre.</p>

<p>Als Ergebnis des <a href="$(HOME)/vote/2022/vote_003">Allgemeinen Beschlusses über unfreie Firmware von 2022</a> (Englisch)
haben wir einen neuen Archivbereich eingerichtet, der es ermöglicht, unfreie Firmware von anderen unfreien Paketen zu trennen:</p>
<ul>
<li>non-free-firmware</li>
</ul>

<p>
Die meisten unfreien Firmware-Pakete wurden von <b>non-free</b> nach
<b>non-free-firmware</b> verschoben. Diese Trennung ermöglicht es, unterschiedliche
offizielle Installationsabbilder bereitzustellen.
</p>

<p>Debian 12 <q>Bookworm</q> wird mit mehreren Arbeitsumgebungen ausgeliefert, unter anderem:
</p>
<ul>
<li>Gnome 43</li>
<li>KDE Plasma 5.27</li>
<li>LXDE 11</li>
<li>LXQt 1.2.0</li>
<li>MATE 1.26</li>
<li>Xfce 4.18</li>
</ul>

<p>
Zu dieser Veröffentlichung sind <b>11.089</b> Pakete neu hinzugekommen, sie enthält also
insgesamt <b>64.419</b> Pakete. <b>6.296</b> <q>veraltete</q> Pakete sind nicht mehr dabei und
<b>43.254</b> Pakete sind für diese Veröffentlichung aktualisiert worden.

Insgesamt belegt <q>Bookworm</q> <b>365.016.420 kB (365 GB)</b> Speicherplatz und besteht aus
<b>1.341.564.204</b> Zeilen Code.</p>

<p>Dank unserer Übersetzerinnen und Übersetzer enthält <q>Bookworm</q> mehr übersetzte
Handbuchseiten als jemals zuvor. Sie haben die <b>Handbuchseiten</b> in eine Reihe von
Sprachen wie Dänisch, Finnisch, Griechisch, Indonesisch, Mazedonisch,
Norwegisch (Bokmål), Russisch, Serbisch, Schwedisch, Tschechisch, Ukrainisch und Vietnamesisch übertragen.
Die <b>systemd</b>-Handbuchseiten sind jetzt komplett in Deutsch verfügbar.</p>

<p>Debian Med Blend führt ein neues Paket ein: <b>shiny-server</b>. Der shiny-server
vereinfacht wissenschaftliche Webapplikationen mit <b>R</b>. Wir haben unsere
Bemühungen fortgesetzt, Continuous-Integration-Unterstützung für Debian-Med-Team-Pakete
zu ermöglichen. Installieren Sie die Metapakete in der Version 3.8.x für Bookworm.</p>

<p>Der Debian Astro Blend bleibt auch weiterhin die Komplettlösung für
professionelle Astronomen, Enthusiasten und Hobbyisten und versorgt sie mit fast durchweg
aktualisierten Softwareversionen. <b>astap</b> und <b>planetary-system-stacker</b>
helfen bei der Bilderstapelung und astrometrischen Auflösung. Auch der Open-Source-Korrelator
<b>openvlbi</b> ist jetzt enthalten.</p>

<p>Auf ARM64 ist die Secure-Boot-Unterstützung zurückgekehrt; Anwender von UEFI-fähiger
ARM64-Hardware können jetzt im Secure-Boot-Modus starten und das volle Potenzial dieser
Sicherheitsfunktionalität nutzen.</p>

<p>Debian 12 <q>Bookworm</q> enthält zahlreiche aktualisierte Softwarepakete
(über 67% aller Pakete aus der Vorversion), unter anderem:
</p>

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS Server 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (Standard-E-Mail-Server) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU Compiler Collection 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>The GNU C Library 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Linux-Kernel 6.1-Reihe</li>
<li>LLVM/Clang-Toolchain 13.0.1, 14.0 (Standard) und 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>

<p>
Durch diese umfangreiche Paketauswahl und der traditionell breiten Unterstützung
für Architekturen bleibt Debian seinem Ziel des <q>Universellen Betriebssystems</q> treu.
Es eignet sich für viele verschiedene Einsatzzwecke: Desktop-Systeme und Netbooks,
Entwicklungsserver und Cluster-Systeme sowie für Datenbank-, Web und Speicher-Server.
Gleichzeitig sorgen weitere Qualitätssicherungsmaßnahmen wie automatische Installations-
und Upgrade-Tests für alle Pakete im Debian-Archiv dafür, dass <q>Bookworm</q> die
hohen Erwartungen seiner Anwenderschaft an die Stabilität einer Debian-Veröffentlichung
erfüllt.
</p>

<p>
Insgesamt werden für <q>Bookworm</q> neun Architekturen offiziell unterstützt:
</p>
<ul>
<li>32-bit PC (i386) und 64-bit PC (amd64)</li>
<li>64-bit ARM (arm64)</li>
<li>ARM EABI (armel)</li>
<li>ARMv7 (EABI hard-float ABI, armhf)</li>
<li>little-endian MIPS (mipsel)</li>
<li>64-bit little-endian MIPS (mips64el)</li>
<li>64-bit little-endian PowerPC (ppc64el)</li>
<li>IBM System z (s390x)</li>
</ul>

<p>
32-bit PC (i386) deckt keine i586-Prozessoren mehr ab; die neue Minimalanforderung
ist i686. <i>Falls Ihre Maschine diese Anforderung nicht erfüllt, empfiehlt
es sich, bis zum Ende seines Unterstützungs-Zyklus bei Bullseye zu bleiben.</i>
</p>

<p>Das Debian-Cloud-Team veröffentlicht <q>Bookworm</q> für verschiedene Cloud-Computing-Dienste:
</p>
<ul>
<li>Amazon EC2 (amd64 und arm64)</li>
<li>Microsoft Azure (amd64)</li>
<li>OpenStack (generic) (amd64, arm64, ppc64el)</li>
<li>GenericCloud (arm64, amd64)</li>
<li>NoCloud (amd64, arm64, ppc64el)</li>
</ul>
<p>
Das genericcloud-Abbild sollte in jeder virtualisierten Umgebung laufen, außerdem gibt es ein
nocloud-Abbild zum Testen des Kompilierungsprozesses.
</p>

<p>GRUB-Pakete werden den os-prober für andere Betriebssysteme <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.de.html#grub-os-prober">
standardmäßig nicht mehr ausführen.</a></p>

<p>Zwischen den Versionen hat das Technische Komitee beschlossen, dass Debian <q>Bookworm</q> nun
ausschließlich das <a href="https://wiki.debian.org/UsrMerge">UsrMerge-Dateisystem-Layout</a> (Englisch) verwenden und die
Unterstützung für das Layout mit nicht zusammengeführtem /usr einstellen soll. Bei Systemen, die als Buster
oder Bullseye aufgesetzt worden sind, ändert sich nichts, allerdings werden ältere Systeme während des Upgrades
umgestellt.</p>

<h3>Kostprobe gefällig?</h3>
<p>
Falls Sie Debian 12 <q>Bookworm</q> einfach ausprobieren wollen, ohne es gleich zu installieren,
können Sie eins der <a href="$(HOME)/CD/live/">Live-Abbilder</a> verwenden, welche das komplette
Betriebssystem nur-lesbar in den Arbeitsspeicher Ihres Computers laden und dort ausführen.
</p>

<p>
Diese Live-Abbilder sind für die Architekturen <code>amd64</code> sowie
<code>i386</code> zu haben und es gibt sie für DVDs, USB-Sticks und Netzwerk-Boot-Umgebungen.
Anwender können aus mehreren verschiedenen Arbeitsumgebungen auswählen: GNOME, KDE Plasma, LXDE, LXQt, MATE und Xfce.
Debian Live <q>Bookworm</q> hat ein Standard-Live-Abbild, kann also auch als grundlegendes
Debian-System ganz ohne grafische Benutzeroberfläche getestet werden.
</p>


<p>
Wenn Ihnen das Betriebssystem gefällt, haben Sie die Möglichkeit, direkt vom Live-Abbild aus die Installation
auf Ihrem Computer anzustoßen. Es enthält dazu den unabhängigen Calamares-Installer sowie den
standardmäßigen Debian-Installer.
Weitere Informationen finden Sie in den
<a href="$(HOME)/releases/bookworm/releasenotes">Veröffentlichungshinweisen</a> und dem
<a href="$(HOME)/CD/live/">Live-Installations-Images</a>-Abschnitt auf der Debian-Website.
</p>

<p>
Falls Sie Debian 12 <q>Bookworm</q> sofort auf Ihrem Computer installieren wollen, haben Sie die Wahl aus vielen
verschiedenen Installationsmedien zum <a href="https://www.debian.org/download">Herunterladen</a>, unter anderem:
Blu-ray Disc, DVD, CD, USB-Stick oder über eine Netzwerkverbindung.

Die <a href="$(HOME)/releases/bookworm/installmanual">Installationsanleitung</a> versorgt Sie mit weiteren Informationen.
</p>

<p>
Debian kann jetzt in 78 Sprachen installiert werden; die meisten davon
sind sowohl in textbasierten als auch in grafischen Benutzeroberflächen verfügbar.
</p>

<p>
Die Installationsabbilder können ab sofort über
<a href="$(HOME)/CD/torrent-cd/">BitTorrent</a> (die empfohlene Methode),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> oder
<a href="$(HOME)/CD/http-ftp/">HTTP</a> heruntergeladen werden; siehe
<a href="$(HOME)/CD/">Debian auf CDs</a> für Details. <q>Bookworm</q> wird
außerdem bald bei zahlreichen <a href="$(HOME)/CD/vendors">Händlern</a>
auf physischen DVDs, CD-ROMs und Blu-ray Discs verfügbar sein.
</p>


<h3>Debian-Upgrade</h3>
<p>
Für die meisten Konfigurationen kümmert sich das APT-Paketverwaltungssystem automatisch
um das Upgrade von Debian 11 <q>Bullseye</q> auf Debian 12 <q>Bookworm</q>.
</p>

<p>Vor dem Upgrade wird nachdrücklich empfohlen, dass Sie Ihre gesamten oder
zumindest die unersetzlichen Daten und Konfigurationsinformationen wegsichern. Die
Upgrade-Werkzeuge und der -Prozess sind sehr zuverlässig, aber trotzdem kann ein
Hardware-Ausfall mitten im Upgrade das System in einem unbrauchbaren Zustand hinterlassen.

Das Wichtigste, das Sie sichern sollten, sind die Inhalte von /etc,
/var/lib/dpkg, /var/lib/apt/extended_states und die Ausgabe von:

<code>$ dpkg --get-selections '*' # (die Anführungszeichen nicht weglassen)</code>

<p>Wir freuen uns über alle Hinweise von Anwendern zum Upgrade von <q>Bullseye</q> auf <q>Bookworm</q>.
Bitte reichen Sie dazu Ihr Ergebnis als Fehlerbericht in der
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.de.html#upgrade-reports">Debian-Fehlerdatenbank</a> ein und
geben Sie als Paket <b>upgrade-reports</b> an.
</p>

<p>
Es wurde viel Entwicklungsarbeit im Debian-Installer investiert. Nicht nur die
Hardware-Unterstützung ist besser geworden, es gibt auch Korrekturen für die
Grafikunterstützung auf UTM, Verbesserungen beim GRUB-Schriftarten-Ladeprogramm, sodass die lange
Wartezeit am Ende der Installation nun entfällt, sowie Korrekturen bei der Erkennung
von BIOS-bootbaren Systemen. Diese Version des Debian-Installers kann unfreie Firmware
aktivieren, wenn sie nötig ist.
</p>


<p>
Das <b>ntp</b>-Paket wurde durch das <b>ntpsec</b>-Paket ersetzt und der standardmäßige
Systemzeitdienst wird jetzt von <b>systemd-timesyncd</b> bereitgestellt; abgesehen davon
gibt es auch Unterstützung für <b>chrony</b> und <b>openntpd</b>.
</p>

<p>Nachdem <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.de.html#non-free-split"> <b>unfreie</b>
Firmware ihren eigenen Bereich im Archiv erhalten hat</a>, empfiehlt es sich, dass Sie <b>non-free-firmware</b> Ihrer APT-Quellenliste
hinzufügen, wenn bei Ihnen solche Firmware installiert ist.</p>

<p>Außerdem sollten bullseye-backports-Einträge vor dem Upgrade aus der APT-Quellenliste
entfernt werden; nach dem Upgrade können Sie ggf. <b>bookworm-backports</b> eintragen.

<p>
Die Security-Suite für<q>Bookworm</q> heißt <b>bookworm-security</b>; wer sie verwendet, sollte die APT-Quellliste beim Upgrade entsprechend anpassen.

Falls Ihre APT-Konfiguration auch Pinning oder <code>APT::Default-Release</code> enthält, sind vermutlich Anpassungen notwendig, um das Upgrade auf die
neue stabile Veröffentlichung zu erlauben. Bitte erwägen Sie, <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.de.html#disable-apt-pinning">APT Pinning abzuschalten</a>.
</p>

<p>Das OpenLDAP-2.5-Upgrade enthält einige <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.de.html#openldap-2.5">inkompatible Änderungen, die Eingriffe
von Hand erfordern</a>. Abhängig von der Konfiguration kann der <b>slapd</b>-Dienst im angehaltenen Zustand verbleiben, bis die Aktualisierung auf die neue Konfiguration
abgeschlossen ist.</p>

<p>Das neue Paket <b>systemd-resolved</b> wird bei Upgrades nicht automatisch installiert, weil es in
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.de.html#systemd-resolved">ein separates Paket ausgelagert wurde</a>. Wer den
systemd-resolvd-Systemdienst verwendet, möchte bitte das neue Paket nach dem Upgrade per Hand installieren und sich bewusst sein, dass
DNS-Auflösungen möglicherweise so lange nicht funktionieren werden wie der neue Dienst nicht auf dem System vorhanden ist.</p>

<p>Es gibt einige an <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.de.html#changes-to-system-logging">Änderungen an der Systemprotokollierung</a>, denn
das Paket <b>rsyslog</b> ist auf den meisten Systemen nicht mehr erforderlich und wird deswegen nicht standardmäßig installiert. Anwender können daher auf <b>journalctl</b> umsteigen oder die
neuen <q>Hochpräzisions-Zeitstempel</q> verwenden, die <b>rsyslog</b> nun schreibt.
</p>

<p>Zu den <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.de.html#trouble">möglichen Problemen während des Upgrades</a> gehören Konflikte
oder Pre-Depends-Schleifen, die sich dadurch lösen lassen, dass einige Pakete entfernt und gelöscht oder die Neuinstallation anderer Pakete erzwungen wird.
Außerdem können <q>Could not perform immediate configuration ...</q>-Fehler (Unmittelbare Konfiguration konnte nicht durchgeführt werden) auftreten, zu deren
Lösung <b>sowohl</b> <q>Bullseye</q> (das gerade entfernt wurde) <b>als auch</b> <q>Bookworm</q> (das gerade hinzugefügt wurde) in der APT-Quellendatei
eingetragen sein müssen. Dateikonflikte können es notwendig machen, Pakete zwangsweise zu entfernen.
Wie schon erwähnt ist eine Datensicherung der Schlüssel zum Geradeziehen von Upgrades, falls irgendwelche widerspenstigen Fehler auftreten.</p>

<p>Es gibt einige Pakete, bei denen Debian nicht versprechen kann, dass zur Behebung von Sicherheitslücken minimale Rückportierungen in die Pakete mit einfließen. Bitte lesen Sie dazu die <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.de.html#limited-security-support">Einschränkungen bei der Sicherheitsunterstützung</a>.</p>

<p>
Wie immer können Debian-Systeme schmerzfrei, ohne Austausch und ohne erzwungene
Ausfallzeit aktualisiert werden, aber es wird nachdrücklich empfohlen, in den
<a href="$(HOME)/releases/bookworm/releasenotes">Veröffentlichungshinweisen</a> sowie
der <a href="$(HOME)/releases/bookworm/installmanual">Installationsanleitung</a> mögliche
Probleme und detaillierte Anleitungen zum Installieren und Upgraden nachzuschlagen. Die
Veröffentlichungshinweise werden in den Wochen nach der Veröffentlichung weiter verfeinert
und in zusätzliche Sprachen übersetzt.
</p>


<h2>Über Debian</h2>

<p>
Debian ist ein freies Betriebssystem, das von tausenden Freiwilligen
aus aller Welt, die sich über das Internet zusammenschließen, entwickelt wird.
Die grundlegenden Stärken des Debian-Projekts sind seine freiwillige Basis,
seine Verpflichtung gegenüber dem Debian-Sozialvertrag und der Freien Software
sowie sein Bestreben, das beste Betriebssystem bereitzustellen, das möglich ist.
Diese neue Veröffentlichung ist ein weiterer wichtiger Schritt in diese Richtung.</p>

<h2>Kontaktinformationen</h2>

<p>
Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a> oder senden eine E-Mail auf Englisch
an &lt;press@debian.org&gt;.
</p>

