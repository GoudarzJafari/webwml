#use wml::debian::template title="Wer verwendet Debian?" BARETITLE=true
#use wml::debian::toc
#use wml::debian::users_list
#use wml::debian::translation-check translation="d4327894f56ad37ddeab0cfc32e3b096993e9766"
# Initial translation by Tobias Toedter <t.toedter@gmx.net> 2005-07-31

<p>
  Hier finden Sie Beschreibungen von einigen Organisationen,
  die Debian anwenden und eine kurze Erläuterung eingeschickt haben,
  wie sie Debian verwenden und warum sie es ausgewählt haben. Die
  Einträge sind alphabetisch sortiert. Wenn Sie in diese Liste
  aufgenommen werden möchten,
  <a href="#submissions">folgen Sie bitte diesen Anweisungen</a>.
</p>

<toc-display />

<toc-add-entry name="edu">Bildungseinrichtungen</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'edu', '.*') :>

<toc-add-entry name="com">Firmen</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'com', '.*') :>

<toc-add-entry name="org">Gemeinnützige Organisationen</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'org', '.*') :>

<toc-add-entry name="gov">Regierungseinrichtungen</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'gov', '.*') :>

<hr />

<h2><a name="submissions" id="submissions">Aufnahmeanträge</a></h2>

<p>
  Um in diese Liste aufgenommen zu werden, schicken Sie bitte die
  folgenden Informationen per E-Mail an
  <a href="mailto:debian-www@lists.debian.org?subject=Who's%20using%20Debian%3F">debian-www@lists.debian.org</a>.
Bitte ändern Sie den Betreff <q>Who's using Debian</q> nicht, da wir sonst über den Antrag nicht informiert werden.
  Die Aufnahmeanträge sollten auf Englisch geschrieben werden.
  Falls Sie nicht Englisch sprechen, senden Sie Ihren Aufnahmeantrag
  bitte an die
  <a href="https://lists.debian.org/i18n.html">passende Übersetzerliste</a>.
  Beachten Sie, dass Ihre Einsendung an eine öffentliche Mailingliste
  geht.
</p>

<p>
  Es sollten nur Informationen über Organisationen eingereicht werden, die von Ihnen repräsentiert werden.
  Es kann sein, dass Sie um Erlaubnis fragen müssen, um die Informationen hier einreichen zu dürfen.
  Falls Sie im Consulting tätig sind und einen Teil Ihres Einkommens über <strong>bezahlten</strong> Support
  für Debian erhalten, konsultieren Sie bitte unsere Seite für <a href="$(HOME)/consultants/">Berater</a>.
</p>

<p>
  Informationen über private Personen sind nicht gestattet. Wir schätzen Ihren Support, verfügen aber nicht 
  über genügend Personal, um Informationen bezüglich privater Personen zu verwalten.
</p>

<p>
  Sie sollten ausschließlich Informationen einreichen, die Sie auch öffentlich machen wollen.
</p>


<ol>
  <li>
    <p>
      Name der Organisation (in der Form <em>Abteilung</em>,
      <em>Organisation</em>, <em>Stadt</em> (optional), <em>Region/Provinz</em> (optional), <em>Staat</em>).
      Ein Beispiel ist: AI Lab, Massachusetts Institute of Technology, USA
    </p>
    <p>
      Es sind noch nicht alle Einträge in dieser Form, aber wir wollen
      diejenigen, die dieser Form nicht entsprechen, umwandeln.
    </p>
  </li>
  <li>Art der Organisation (educational (Bildungseinrichtung),
      non-profit (gemeinnützige Organisationen), commercial (Firmen),
      government (Regierungseinrichtung))</li>
  <li><em>(wahlweise)</em> Verweis auf die Homepage</li>
  <li>Ein oder zwei kurze Absätze darüber, wie Ihre Organisation Debian
      verwendet. Versuchen Sie, Details über die Anzahl der
      Arbeitsplatzrechner/Server hinzuzufügen, die Software, die auf
      den Rechnern läuft (Sie brauchen die Version nicht angeben)
      sowie die Gründe, warum Sie Debian den
      Mitbewerbern vorgezogen haben.
  </li>
</ol>

<p>
  Falls Sie eine Organisation repräsentieren, die ein Debian Partner werden möchte,
  konsultieren Sie bitte unser <a href="$(HOME)/partners/">Partner-Programm</a>. Dort 
  erhalten Sie mehr Informationen darüber, wie Sie Debian längerfristig unterstützen können.
  Bezüglich Spenden für Debian konsultieren Sie bitte unsere <a href="$(HOME)/donations/">\
  Spenden-Seite</a>.
</p>
