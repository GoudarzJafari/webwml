#use wml::debian::template title="Einführung in Debian" MAINPAGE="true" FOOTERMAP="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"
# Translator: Martin Schulze <joey@infodrom.north.de>
# Updated: Holger Wansing <linux@wansing-online.de>, 2016.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian ist eine Gemeinschaft</h2>
      <p>Tausende von Freiwilligen auf der ganzen Welt arbeiten zusammen am Ziel Freie Software und zum Nutzen der Anwender. Mach' mit beim Debian-Projekt!</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Menschen:</a>
          Wer wir sind, was wir tun
        </li>
        <li>
          <a href="philosophy">Unsere Philosophie:</a>
          Warum und wie wir es tun
        </li>
        <li>
          <a href="../devel/join/">Mitmachen:</a>
          Wie du ein Teil des Debian-Projekts wirst
        </li>
        <li>
          <a href="help">Etwas zu Debian beitragen:</a>
          Wie du Debian helfen kannst
        </li>
        <li>
          <a href="../social_contract">Gesellschaftsvertrag:</a>
          Unsere Moralvorstellungen
        </li>
        <li>
          <a href="diversity">Jeder ist willkommen:</a>
          Debians Stellungnahme zur Vielfalt
        </li>
        <li>
          <a href="../code_of_conduct">Für Bewerber:</a>
          Debians Verhaltenskodex
        </li>
        <li>
          <a href="../partners/">Partner:</a>
          Firmen und Organisationen, die das Debian-Projekt fortlaufend unterstützen
        </li>
        <li>
          <a href="../donations">Spenden:</a>
          Wie du das Debian-Projekt als Sponsor unterstützen kannst
        </li>
        <li>
          <a href="../legal/">Rechtliche Themen:</a>
          Lizenzen, Markenzeichen, Datenschutzrichtlinien, Patentrichtlinien usw.
        </li>
        <li>
          <a href="../contact">Kontakt:</a>
          Mit uns in Verbindung treten
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian ist ein Betriebssystem</h2>
      <p>Debian ist ein freies Betriebssystem, entwickelt und betreut durch das Debian-Projekt.
         Eine freie Linux-Distribution mit Tausenden Anwendungen, bestrebt, den Bedürfnissen unserer Anwender gerecht zu werden.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Download:</a>
          Wo du Debian bekommen kannst
        </li>
        <li>
          <a href="why_debian">Warum Debian:</a>
          Gründe, warum du Debian nutzen solltest
        </li>
        <li>
          <a href="../support">Unterstützung:</a>
          Wo du Hilfe bekommen kannst
        </li>
        <li>
          <a href="../security">Sicherheit:</a>
          Letztes Update: <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Software:</a>
          Stöbere in der reichhaltigen Liste unserer Anwendungen
        </li>
        <li>
          <a href="../doc">Dokumentation:</a>
          Installationsanleitung, FAQ, HOWTOs, Wiki, und vieles mehr
        </li>
        <li>
          <a href="../bugs">Fehlerdatenbank (Bug Tracking System; BTS):</a>
          Wie du einen Fehlerbericht einreichst, Dokumentation zum BTS
        </li>
        <li>
          <a href="https://lists.debian.org/">Mailinglisten:</a>
          Viele Debian-Listen für Benutzer, Entwickler usw.
        </li>
        <li>
          <a href="../blends">Pure Blends:</a>
          Metapakete für spezielle Anwendungsfälle
        </li>
        <li>
          <a href="../devel">Entwicklerecke:</a>
          Informationen, die primär für Debian-Entwickler von Interesse sind
        </li>
        <li>
          <a href="../ports">Portierungen/Architekturen:</a>
          Verschiedene CPU-Architekturen, die wir unterstützen
        </li>
        <li>
          <a href="search">Suchen:</a>
          Informationen zur Verwendung von Debians Suchmaschine
        </li>
        <li>
          <a href="cn">Sprachen:</a>
          Spracheinstellungen für die Debian-Website
        </li>
      </ul>
    </div>
  </div>

</div>

