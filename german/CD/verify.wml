#use wml::debian::cdimage title="Die Authentizität von Debian-Images überprüfen" BARETITLE=true
#use wml::debian::translation-check translation="e96ee42901d20a8878ded2c462204cd1b7a7210f"
# Translated: Holger Wansing <linux@wansing-online.de>, 2011, 2016.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2023.

<p>
Offizielle Veröffentlichungen von Debian-Installations- und Live-Images
enthalten signierte
Prüfsummen-Dateien; Sie finden diese in den Verzeichnissen
<code>iso-cd</code>, <code>jigdo-dvd</code>, <code>iso-hybrid</code>
usw. Die Prüfsummen ermöglichen Ihnen, zu überprüfen, ob die
Images, die Sie heruntergeladen haben, korrekt sind. Zu allererst
kann die Prüfsumme benutzt werden, um sicherzustellen, dass die Images
während des Downloads nicht kompromittiert oder beschädigt wurden.
Zweitens erlauben es die Signaturen der Prüfsummen-Dateien, zu bestätigen,
dass die Dateien diejenigen sind, die von Debian
offiziell herausgegeben wurden und dass nicht daran herumgepfuscht
worden ist.
</p>

<p>
Um den Inhalt einer Image-Datei zu überprüfen, stellen Sie sicher, dass
Sie das passende Prüfsummen-Programm verwenden.
Für jede Veröffentlichung existieren kryptografisch starke
Prüfsummen-Algorithmen (SHA256 und SHA512); Sie sollten für die Arbeit
damit die Werkzeuge <code>sha256sum</code> bzw. <code>sha512sum</code>
nutzen.
</p>

<p>
Um sich zu versichern, dass die Prüfsummen-Dateien selbst korrekt sind,
nutzen Sie GnuPG, um sie gegenüber den beigefügten Signaturdateien zu
verifizieren (z.B. <code>SHA512SUMS.sign</code>).
Die Schlüssel, die für diese Signaturen verwendet werden, sind im <a
href="https://keyring.debian.org">Debian GPG-Schüsselring</a>
enthalten und der beste Weg zur Überprüfung der Schlüssel ist, diesen
Schlüsselring über das Web of Trust zu validieren.
Um das Leben für Leute einfacher zu machen, die keinen funktionsfähigen
Zugriff auf offizielle Debian-Maschinen haben, hier Details zu
den Schlüsseln, die zur Signatur der Veröffentlichungen in den letzten
Jahren genutzt worden sind, sowie Links, um die öffentlichen Schlüssel
direkt herunterzuladen:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
