#use wml::debian::template title="Debian-Spiegel" MAINPAGE="true"
#use wml::debian::translation-check translation="0d90a782010be0f74eb482a26ebb41d18aebee61"
# $Id$

# Translation update: Holger Wansing <linux@wansing-online.de>, 2016, 2017, 2020, 2023.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian-Spiegel werden von Freiwilligen betreut.
   Wenn Sie in einer Position sind, Plattenplatz und Netzwerkanbindung spenden zu können,
   ziehen Sie bitte in Erwägung, einen Spiegel-Server zu erstellen, um so dazu beizutragen,
   Debian besser verfügbar zu machen. Für weitere Informationen besuchen Sie bitte
   <a href="ftpmirror">diese Seite</a>.<p>
</aside>

<p>Debian wird auf Hunderte Server weltweit verteilt (aka gespiegelt), so dass alle denselben
   Inhalt bereitstellen. So können wir einen besseren Zugriff auf unser Paketarchiv gewährleisten.
</p>

<p>Sie finden folgende Inhalte auf unseren Spiegel-Servern:</p>

<dl>
<dt><strong>Debian-Pakete</strong> (<code>debian/</code>)</dt>
  <dd>Der Debian-Paket-Pool: er umfasst die überwiegende Mehrheit der
      <code>.deb</code>-Pakete, die Installationsmedien und den Quellcode.
      <br>
      Hier eine Liste der <a href="list">Debian-Spiegel</a>, die das
      <code>debian/</code>-Archiv enthalten.
  </dd>
<dt><strong>CD-Images</strong> (<code>debian-cd/</code>)</dt>
  <dd>Das Depot der CD-Images: Jigdo-Dateien und ISO-Image-Dateien.
      <br>
      Hier eine Liste der <a href="$(HOME)/CD/http-ftp/#mirrors">Debian-Spiegel</a>,
      die das <code>debian-cd/</code>-Archiv beinhalten.
  </dd>
<dt><strong>Alte Veröffentlichungen</strong> (<code>debian-archive/</code>)</dt>
  <dd>Das Archiv alter veröffentlichter Versionen von Debian.
      <br>
      Lesen Sie die Seite über <a href="$(HOME)/distrib/archive">Distributions-Archive</a>
      bezüglich weiterer Informationen.
  </dd>
</dl>

<p>Übersicht über den
<a href="https://mirror-master.debian.org/status/mirror-status.html">
Status von Debian-Spiegel-Servern
</a></p>
