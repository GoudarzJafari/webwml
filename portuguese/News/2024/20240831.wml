#use wml::debian::translation-check translation="f93b89c74962ae015feaa58f013cef8f7ec08dd9"
<define-tag pagetitle>Atualização Debian 12: 12.7 lançado</define-tag>
<define-tag release_date>2024-08-31</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a sétima atualização de sua
versão estável (stable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, além de pequenos ajustes para problemas mais sérios. Avisos de
segurança já foram publicados em separado e são referenciados quando
necessário.</p>

<p>Por favor, note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de jogar fora as antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir de
security.debian.org não terão que atualizar muitos pacotes, e a maioria de tais
atualizações estão incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Secure Boot e outros sistemas operacionais</h2>

<p>Pessoas que inicializam outros sistemas operacionais no mesmo hardware, e
que possuem Secure Boot habilitado, devem estar cientes que o shim 15.8
(incluso no Debian <revision>) revoga assinaturas espalhadas em versões
anteriores do shim no firmware UEFI.
Isso pode causar a falha de inicialização de outros sistemas operacionais que
utilizem uma versão shim anterior à 15.8.</p>

<p>Pessoas afetadas podem desabilitar temporariamente o Secure Boot antes de
atualizar outros sistemas operacionais.</p>


<h2>Correções gerais de bugs</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction amd64-microcode "New upstream release; security fixes [CVE-2023-31315]; SEV firmware fixes [CVE-2023-20584 CVE-2023-31356]">
<correction ansible "New upstream stable release; fix key leakage issue [CVE-2023-4237]">
<correction ansible-core "New upstream stable release; fix information disclosure issue [CVE-2024-0690]; fix template injection issue [CVE-2023-5764]; fix path traversal issue [CVE-2023-5115]">
<correction apache2 "New upstream stable release; fix content disclosure issue [CVE-2024-40725]">
<correction base-files "Update for the point release">
<correction cacti "Fix remote code execution issues [CVE-2024-25641 CVE-2024-31459], cross site scripting issues [CVE-2024-29894 CVE-2024-31443 CVE-2024-31444], SQL injection issues [CVE-2024-31445 CVE-2024-31458 CVE-2024-31460], <q>type juggling</q> issue [CVE-2024-34340]; fix autopkgtest failure">
<correction calamares-settings-debian "Fix Xfce launcher permission issue">
<correction calibre "Fix remote code execution issue [CVE-2024-6782, cross site scripting issue [CVE-2024-7008], SQL injection issue [CVE-2024-7009]">
<correction choose-mirror "Update list of available mirrors">
<correction cockpit "Fix denial of service issue [CVE-2024-6126]">
<correction cups "Fix issues with domain socket handling [CVE-2024-35235]">
<correction curl "Fix ASN.1 date parser overread issue [CVE-2024-7264]">
<correction cyrus-imapd "Fix regression introduced in CVE-2024-34055 fix">
<correction dcm2niix "Fix potential code execution issue [CVE-2024-27629]">
<correction debian-installer "Increase Linux kernel ABI to 6.1.0-25; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction dmitry "Security fixes [CVE-2024-31837 CVE-2020-14931 CVE-2017-7938]">
<correction dropbear "Fix <q>noremotetcp</q> behaviour of keepalive packets in combination with the <q>no-port-forwarding</q> authorized_keys(5) restriction">
<correction gettext.js "Fix server side request forgery issue [CVE-2024-43370]">
<correction glibc "Fix freeing uninitialized memory in libc_freeres_fn(); fix several performance issues and possible crashses">
<correction glogic "Require Gtk 3.0 and PangoCairo 1.0">
<correction graphviz "Fix broken scale">
<correction gtk+2.0 "Avoid looking for modules in the current working directory [CVE-2024-6655]">
<correction gtk+3.0 "Avoid looking for modules in the current working directory [CVE-2024-6655]">
<correction imagemagick "Fix segmentation fault issue; fix incomplete fix for CVE-2023-34151">
<correction initramfs-tools "hook_functions: Fix copy_file with source including a directory symlink; hook-functions: copy_file: Canonicalise target filename; install hid-multitouch module for Surface Pro 4 Keyboard; add hyper-keyboard module, needed to enter LUKS password in Hyper-V; auto_add_modules: Add onboard_usb_hub, onboard_usb_dev">
<correction intel-microcode "New upstream release; security fixes [CVE-2023-42667 CVE-2023-49141 CVE-2024-24853 CVE-2024-24980 CVE-2024-25939]">
<correction ipmitool "Add missing enterprise-numbers.txt file">
<correction libapache2-mod-auth-openidc "Avoid crash when the Forwarded header is not present but OIDCXForwardedHeaders is configured for it">
<correction libnvme "Fix buffer overflow during scanning devices that do not support sub-4k reads">
<correction libvirt "birsh: Make domif-setlink work more than once; qemu: domain: Fix logic when tainting domain; fix denial of service issues [CVE-2023-3750 CVE-2024-1441 CVE-2024-2494 CVE-2024-2496]">
<correction linux "New upstream release; bump ABI to 25">
<correction linux-signed-amd64 "New upstream release; bump ABI to 25">
<correction linux-signed-arm64 "New upstream release; bump ABI to 25">
<correction linux-signed-i386 "New upstream release; bump ABI to 25">
<correction newlib "Fix buffer overflow issue [CVE-2021-3420]">
<correction numpy "Conflict with python-numpy">
<correction openssl "New upstream stable release; fix denial of service issues [CVE-2024-2511 CVE-2024-4603]; fix use after free issue [CVE-2024-4741]">
<correction poe.app "Make comment cells editable; fix drawing when an NSActionCell in the preferences is acted on to change state">
<correction putty "Fix weak ECDSA nonce generation allowing secret key recovery [CVE-2024-31497]">
<correction qemu "New upstream stable release; fix denial of service issue [CVE-2024-4467]">
<correction riemann-c-client "Prevent malformed payload in GnuTLS send/receive operations">
<correction rustc-web "New upstream stable release, to support building new chromium and firefox-esr versions">
<correction shim "New upstream release">
<correction shim-helpers-amd64-signed "Rebuild against shim 15.8.1">
<correction shim-helpers-arm64-signed "Rebuild against shim 15.8.1">
<correction shim-helpers-i386-signed "Rebuild against shim 15.8.1">
<correction shim-signed "New upstream stable release">
<correction systemd "New upstream stable release; update hwdb">
<correction usb.ids "Update included data list">
<correction xmedcon "Fix buffer overflow issue [CVE-2024-29421]">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
A equipe de segurança já lançou um aviso para cada uma dessas atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2024 5617 chromium>
<dsa 2024 5629 chromium>
<dsa 2024 5634 chromium>
<dsa 2024 5636 chromium>
<dsa 2024 5639 chromium>
<dsa 2024 5648 chromium>
<dsa 2024 5654 chromium>
<dsa 2024 5656 chromium>
<dsa 2024 5668 chromium>
<dsa 2024 5675 chromium>
<dsa 2024 5676 chromium>
<dsa 2024 5683 chromium>
<dsa 2024 5687 chromium>
<dsa 2024 5689 chromium>
<dsa 2024 5694 chromium>
<dsa 2024 5696 chromium>
<dsa 2024 5697 chromium>
<dsa 2024 5701 chromium>
<dsa 2024 5710 chromium>
<dsa 2024 5716 chromium>
<dsa 2024 5719 emacs>
<dsa 2024 5720 chromium>
<dsa 2024 5719 emacs>
<dsa 2024 5722 libvpx>
<dsa 2024 5723 plasma-workspace>
<dsa 2024 5724 openssh>
<dsa 2024 5725 znc>
<dsa 2024 5726 krb5>
<dsa 2024 5727 firefox-esr>
<dsa 2024 5728 exim4>
<dsa 2024 5729 apache2>
<dsa 2024 5731 linux-signed-amd64>
<dsa 2024 5731 linux-signed-arm64>
<dsa 2024 5731 linux-signed-i386>
<dsa 2024 5731 linux>
<dsa 2024 5732 chromium>
<dsa 2024 5734 bind9>
<dsa 2024 5735 chromium>
<dsa 2024 5737 libreoffice>
<dsa 2024 5738 openjdk-17>
<dsa 2024 5739 wpa>
<dsa 2024 5740 firefox-esr>
<dsa 2024 5741 chromium>
<dsa 2024 5743 roundcube>
<dsa 2024 5745 postgresql-15>
<dsa 2024 5748 ffmpeg>
<dsa 2024 5749 bubblewrap>
<dsa 2024 5749 flatpak>
<dsa 2024 5750 python-asyncssh>
<dsa 2024 5751 squid>
<dsa 2024 5752 dovecot>
<dsa 2024 5753 aom>
<dsa 2024 5754 cinder>
<dsa 2024 5755 glance>
<dsa 2024 5756 nova>
<dsa 2024 5757 chromium>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos por circunstâncias fora de nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction bcachefs-tools "Buggy; obsolete">

</table>


<h2>Instalador do Debian</h2>

<p>O instalador foi atualizado para incluir as correções incorporadas
na versão estável (stable) pela versão pontual.</p>


<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informações da versão estável (stable) (notas de lançamento, errata, etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional completamente livre Debian.</p>


<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da versão estável (stable) em
&lt;debian-release@lists.debian.org&gt;.</p>
