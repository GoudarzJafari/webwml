#use wml::debian::template title="Informações de lançamento do Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="edbe7403c8846f9f66e83e00e7152cfbf0bacace"

<p>O Debian <current_release_bullseye> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 foi lançado inicialmente em <:=spokendate('2021-08-14'):>."
/>
O lançamento incluiu várias grandes mudanças descritas em
nosso <a href="$(HOME)/News/2021/20210814">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 11 foi substituído pelo
<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
</strong></p>

<p>
O ciclo de vida do Debian 11 abrange cinco anos: os três anos iniciais de
suporte total ao Debian, até <:=spokendate('2024-08-14'):>, e dois anos de
Suporte de Longo Prazo (LTS - Long Term Support), até
<:=spokendate('2026-08-31'):>. O conjunto de arquiteturas suportadas é
reduzido para i386, amd64, armhf e arm64 durante a vigência do LTS do Bullseye.
Para obter mais informações, por favor consulte a página
<a href="$(HOME)/security/">informações de segurança</a> e a
<a href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
</p>

<p>Para obter e instalar o Debian, consulte
a página de informação de instalação e o guia de instalação. Para atualizar a
partir de uma versão mais antiga do Debian, consulte as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

<p>Arquiteturas de computadores suportadas no lançamento inicial do bullseye:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips64el/">64-bit MIPS (little endian)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
<li><a href="../../ports/s390x/">IBM System z</a>

</ul>

<p>Apesar de nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>
