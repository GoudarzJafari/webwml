#use wml::debian::template title="Informações de lançamento do Debian &ldquo;squeeze&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="476ef1924df8fdb75ad78952645435d512788254"

<p>O Debian <current_release_squeeze> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_squeeze/>"><current_release_date_squeeze></a>.
<ifneq "6.0.0" "<current_release>"
  "Debian 6.0.0 was initially released on <:=spokendate('2011-02-06'):>."
/>
O lançamento incluiu várias grandes mudanças, descritas em
nosso <a href="$(HOME)/News/2011/20110205a">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 6 foi substituído pelo
<a href="../wheezy/">Debian 7 (<q>wheezy</q>)</a>.
As atualizações de segurança foram descontinuadas em
<:=spokendate('2014-05-31'):>.
</strong></p>

<p><strong>O squeeze se beneficiou do suporte de longo prazo (LTS - Long Term
Support) até o final de fevereiro de 2016. O LTS era limitado ao i386 e amd64.
Para mais informações, consulte a
<a href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
</strong></p>

<p>Para obter e instalar o Debian, veja a página de informações de instalação e
o guia de instalação. Para atualizar a partir de uma versão mais antiga do
Debian, veja as instruções nas <a href="releasenotes">notas de lançamento</a>.</p>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>

<ul>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/sparc/">SPARC</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/kfreebsd-amd64/">kFreeBSD 64-bit PC (amd64)</a>
<li><a href="../../ports/kfreebsd-i386/">kFreeBSD 32-bit PC (i386)</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
</ul>

<p>Apesar dos nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>
