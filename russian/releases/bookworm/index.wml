#use wml::debian::template title="Информация о выпуске Debian &ldquo;bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="77c9da4860132027cb6546223ff0e9b84071a5b7" maintainer="Lev Lamberov"

<p>Debian <current_release_bookworm> был
выпущен <a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 изначально был выпущен <:=spokendate('2023-06-10'):>."
/>
Выпуск включает множество важных
изменений, описанных в
нашем <a href="$(HOME)/News/2023/20230610">анонсе</a> и
в <a href="releasenotes">информации о выпуске</a>.</p>

# <p><strong>Debian 12 был заменён на
# <a href="../trixie/">Debian 13 (<q>trixie</q>)</a>.
# </strong></p>

### This paragraph is orientative, please review before publishing!
# <p><strong>Тем не менее bullseye получает долгосрочную поддержку (LTS) вплоть до
# 30-го июня 2028 года. LTS ограничиается архитектурами i386, amd64, armel, armhf и arm64.
# Все остальные архитектуры более не поддерживаются в bookworm.
# Дополнительную информацию см. в <a
# href="https://wiki.debian.org/LTS">разделе LTS вики Debian</a>.
# </strong></p>

<p>
Жизненный цикл Debian 12 составляет пять лет: сначала три года
полной поддержки Debian до <:=spokendate('2026-06-10'):>, затем два года
долгосрочной поддержки (LTS) до <:=spokendate('2028-06-30'):>. Количество
поддерживаемых архитектур во время долгосрочной поддержки уменьшается. За дополнительной
информацией обращайтесь к странице <a href="$(HOME)/security/">информации о безопасности</a> и
<a  href="https://wiki.debian.org/LTS">разделу LTS вики Debian</a>.
</p>

<p>О том, как получить и установить Debian, см. страницу с
<a href="debian-installer/">информацией по установке</a> и
<a href="installmanual">руководство по установке</a>. Инструкции
по обновлению со старого выпуска см. в
<a href="releasenotes">информации о выпуске</a>.</p>

### Activate the following when LTS period starts.
#<p>Архитектуры, поддерживаемые в ходе жизненного цикла LTS:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Изначально в выпуске bookworm поддерживались следующие архиектуры:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Независимо от нашего желания в выпуске могут быть некоторые проблемы, несмотря на то, что он объявлен
<em>стабильным</em>. Мы составили
<a href="errata">список основных известных проблем</a>, и вы всегда можете
<a href="../reportingbugs">сообщить нам о других ошибках</a>.</p>

<p>Наконец, мы составили список <a href="credits">людей, которые внесли свой вклад</a>
в создание этого выпуска.</p>
