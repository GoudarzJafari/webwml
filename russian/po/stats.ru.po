# translation of others.ru.po to Russian
# Lev Lamberov <l.lamberov@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml stats\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2024-04-29 11:12+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.4.2\n"

#: ../../stattrans.pl:282 ../../stattrans.pl:495
msgid "Wrong translation version"
msgstr "Неверная версия перевода"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Этот перевод слишком устарел"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Оригинал новее, чем перевод"

#: ../../stattrans.pl:290 ../../stattrans.pl:495
msgid "The original no longer exists"
msgstr "Оригинал более не существует"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "обращения"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "число обращений не доступно"

#: ../../stattrans.pl:598 ../../stattrans.pl:737
msgid "Created with <transstatslink>"
msgstr "Создано с помощью <transstatslink>"

#: ../../stattrans.pl:603
msgid "Translation summary for"
msgstr "Резюме перевода для"

#: ../../stattrans.pl:606
msgid "Translated"
msgstr "Переведено"

#: ../../stattrans.pl:606 ../../stattrans.pl:685 ../../stattrans.pl:759
#: ../../stattrans.pl:805 ../../stattrans.pl:848
msgid "Up to date"
msgstr "Актуально"

#: ../../stattrans.pl:606 ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Outdated"
msgstr "Устарело"

#: ../../stattrans.pl:606 ../../stattrans.pl:761 ../../stattrans.pl:807
#: ../../stattrans.pl:850
msgid "Not translated"
msgstr "Не переведено"

#: ../../stattrans.pl:607 ../../stattrans.pl:608 ../../stattrans.pl:609
#: ../../stattrans.pl:610
msgid "files"
msgstr "файлы"

#: ../../stattrans.pl:613 ../../stattrans.pl:614 ../../stattrans.pl:615
#: ../../stattrans.pl:616
msgid "bytes"
msgstr "байты"

#: ../../stattrans.pl:623
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Замечание: список страниц отсортирован по популярности. Наведите указатель "
"мыши на название страницы, чтобы увидеть количество обращений."

#: ../../stattrans.pl:629
msgid "Outdated translations"
msgstr "Устаревшие переводы"

#: ../../stattrans.pl:631 ../../stattrans.pl:684
msgid "File"
msgstr "Файл"

#: ../../stattrans.pl:633
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:635
msgid "Comment"
msgstr "Комментарий"

#: ../../stattrans.pl:636
msgid "Git command line"
msgstr "Командная строка git"

#: ../../stattrans.pl:638
msgid "Log"
msgstr "Журнал"

#: ../../stattrans.pl:639
msgid "Translation"
msgstr "Перевод"

#: ../../stattrans.pl:640
msgid "Maintainer"
msgstr "Сопровождающий"

#: ../../stattrans.pl:642
msgid "Status"
msgstr "Состояние"

#: ../../stattrans.pl:643
msgid "Translator"
msgstr "Переводчик"

#: ../../stattrans.pl:644
msgid "Date"
msgstr "Дата"

#: ../../stattrans.pl:651
msgid "General pages not translated"
msgstr "Не переведённые страницы общего характера"

#: ../../stattrans.pl:652
msgid "Untranslated general pages"
msgstr "Не переведённые страницы общего характера"

#: ../../stattrans.pl:657
msgid "News items not translated (low priority)"
msgstr "Не переведённые новости (низкий приоритет)"

#: ../../stattrans.pl:658
msgid "Untranslated news items"
msgstr "Не переведённые новости"

#: ../../stattrans.pl:663
msgid "Consultant/user pages not translated (low priority)"
msgstr ""
"Не переведённые страницы о консультантах/пользователях (низкий приоритет)"

#: ../../stattrans.pl:664
msgid "Untranslated consultant/user pages"
msgstr "Не переведённые страницы консультантов/пользователей"

#: ../../stattrans.pl:669
msgid "International pages not translated (very low priority)"
msgstr "Не переведённые международные страницы (очень низкий приоритет)"

#: ../../stattrans.pl:670
msgid "Untranslated international pages"
msgstr "Не переведённые международные страницы"

#: ../../stattrans.pl:675
msgid "Translated pages (up-to-date)"
msgstr "Переведённые страницы (актуальные)"

#: ../../stattrans.pl:682 ../../stattrans.pl:832
msgid "Translated templates (PO files)"
msgstr "Переведённые шаблоны (файлы PO)"

#: ../../stattrans.pl:683 ../../stattrans.pl:835
msgid "PO Translation Statistics"
msgstr "Статистика перевода PO"

#: ../../stattrans.pl:686 ../../stattrans.pl:849
msgid "Fuzzy"
msgstr "Неточные"

#: ../../stattrans.pl:687
msgid "Untranslated"
msgstr "Не переведено"

#: ../../stattrans.pl:688
msgid "Total"
msgstr "Всего"

#: ../../stattrans.pl:705
msgid "Total:"
msgstr "Всего:"

#: ../../stattrans.pl:739
msgid "Translated web pages"
msgstr "Переведённые веб-страницы"

#: ../../stattrans.pl:742
msgid "Translation Statistics by Page Count"
msgstr "Статистика перевода по счетчику страниц"

#: ../../stattrans.pl:757 ../../stattrans.pl:803 ../../stattrans.pl:847
msgid "Language"
msgstr "Язык"

#: ../../stattrans.pl:758 ../../stattrans.pl:804
msgid "Translations"
msgstr "Переводы"

#: ../../stattrans.pl:785
msgid "Translated web pages (by size)"
msgstr "Переведённые веб-страницы (по размеру)"

#: ../../stattrans.pl:788
msgid "Translation Statistics by Page Size"
msgstr "Статистика перевода по размеру страниц"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Статистика перевода сайта Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Страниц для перевода: %d."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Байт для перевода: %d."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Строк для перевода: %d."

#~ msgid "Click to fetch diffstat data"
#~ msgstr "Нажмите, чтобы получить данные diffstat"

#~ msgid "Colored diff"
#~ msgstr "Цветной diff"

#~ msgid "Commit diff"
#~ msgstr "diff коммита"

#~ msgid "Created with"
#~ msgstr "Создано с помощью"

#~ msgid "Diffstat"
#~ msgstr "Diffstat"

#~ msgid "Hit data from %s, gathered %s."
#~ msgstr "Данные о посещениях с %s, собраны %s."

#~ msgid "Unified diff"
#~ msgstr "Унифицированный diff"
