#use wml::debian::template title="Загрузка Debian" BARETITLE=true
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="96a44b00371cb1e75df59d859a900aab253ee412" maintainer="Lev Lamberov"

<p>На этой странице представлены варианты загрузки и установки стабильного выпуска Debian.</p>
<ul>
<li> <a href="../CD/http-ftp/#mirrors">Зеркала для загрузки</a> установочных образов
<li> <a href="../releases/stable/installmanual">Руководство по установке</a> с подробными инструкциями
<li> <a href="../releases/stable/releasenotes">Информация о выпуске</a>
<li> <a href="../devel/debian-installer/">ISO-образы тестируемого выпуска Debian</a>
<li> <a href="../CD/verify">Проверка аутентичности образов Debian</a>
</ul>

<div class="line">
  <div class="item col50">
    <h2>Загрузка установочного образа</h2>
    <ul>
      <li><a href="netinst"><strong>Небольшой установочный образ</strong></a>:
          Эти маленькие образы могут быть быстро загружены, их следует записать на сменный
          диск. Чтобы использовать эти образы, необходимо подключение к Интернет
          на машине, на которой производится установка Debian.
        <ul class="quicklist downlist">
          <li><a title="Загрузить установщик для обычного 64-битного ПК Intel и AMD"
                 href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">iso-образы
              netinst для 64-битных ПК</a></li>
          <li><a title="Загрузить установщик для обычного 32-битного ПК Intel и AMD"
                 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">iso-образы
              netinst для 32-битных ПК</a></li>
          <li><a title="Загрузить торренты CD для 64-битного ПК Intel и AMD"
                 href="<stable-images-url/>/amd64/bt-cd/">торренты для 64-битного ПК (CD)</a></li>
          <li><a title="Загрузить торренты CD для обычного 32-битного ПК Intel и AMD"
                 href="<stable-images-url/>/i386/bt-cd/">торренты для 32-битного ПК (CD)</a></li>
        </ul>
       </li>
       <li><a href="../CD/"><strong>Полные установочные образы</strong></a>
        содержат большее количество пакетов, что упрощает процесс установки на
        машины без соединения с Интернет.
        <ul class="quicklist downlist">
          <li><a title="Загрузить установщик для 64-битного ПК Intel и AMD"
                href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">iso-образ первого DVD для 64-битного
             ПК</a></li>
         <li><a title="Загрузить установщик для обычного 32-битного ПК Intel и AMD"
                href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">iso-образ первого DVD для 32-битного
             ПК</a></li>
          <li><a title="Загрузить торренты DVD для 64-битного ПК Intel и AMD"
                 href="<stable-images-url/>/amd64/bt-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso.torrent">торренты для 64-битного ПК (DVD)</a></li>
          <li><a title="Загрузить торренты DVD для обычного 32-битного ПК Intel и AMD"
                 href="<stable-images-url/>/i386/bt-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso.torrent">торренты для 32-битного ПК (DVD)</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2>Попробуйте <q>живой</q> образ Debian до установки</h2>
    <p>
      Вы можете попробовать Debian, загрузим <q>живую</q> систему с компакт-диска, DVD или USB
      без установки файлов на ваш компьютер. Также вы можете запустить
      <a href="https://calamares.io">установщик Calamares</a>. Он доступен только для 64-битных ПК.
      Ознакомьтесь с дополнительной <a href="../CD/live#choose_live">информацией об этом методе установки</a>.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Загрузить <q>живой</q> образ с Gnome для 64-битных ПК Intel и AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso"><q>Живой</q> образ с Gnome</a></li>
      <li><a title="Загрузить <q>живой</q> образ с Xfce для 64-битных ПК Intel и AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso"><q>Живой</q> образ с Xfce</a></li>
      <li><a title="Загрузить <q>живой</q> образ с KDE для 64-битных ПК Intel и AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso"><q>Живой</q> образ с KDE</a></li>
      <li><a title="Загрузить другие <q>живые</q> образы для 64-битных ПК Intel и AMD"
            href="<live-images-url/>/amd64/iso-hybrid/">Другие <q>живые</q> образы</a></li>
      <li><a title="Загрузить через торрент <q>живой</q> образ для 64-битных ПК Intel и AMD"
          href="<live-images-url/>/amd64/bt-hybrid/">Торренты с <q>живыми</q> образами</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Покупка компакт-дисков, DVD или USB у одного из поставщиков
      установочных носителей Debian</a></h2>

   <p>
      Многие поставщики продают дистрибутив менее, чем за 5 долларов плюс цена доставки
      (но они не всегда осуществляют международную доставку).
   </p>

   <p>У этих дисков есть несколько преимуществ:</p>

   <ul>
     <li>Вы можете установить систему на машину без подключения к Интернет.</li>
         <li>Вы можете установить Debian без необходимости скачивать при каждой установке
         нужные вам пакеты.</li>
   </ul>

   <h2><a href="pre-installed">Купить компьютер с предустановленным
      Debian</a></h2>
   <p>Свои преимущества есть и у этого способа:</p>
   <ul>
    <li>Вам не нужно устанавливать Debian.</li>
    <li>Установленная система заранее сконфигурирована под оборудование.</li>
    <li>Продавец может предоставлять техническую поддержку.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2>Используйте облачные образы Debian</h2>
    <p>Официальный <a href="https://cloud.debian.org/images/cloud/"><strong>облачный образ</strong></a>, собранный облачной командой, может использоваться:</p>
    <ul>
      <li>у вашего поставщика OpenStack в qcow2 или raw форматах.
        <ul class="quicklist downlist">
          <li>64-битный образ для AMD/Intel (<a title="Образ OpenStack для 64-битных архитектур Intel и AMD в формате qcow2" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="Образ OpenStack для 64-битных архитектур Intel и AMD в формате raw" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
          <li>64-битный образ для ARM (<a title="Образ OpenStack для 64-битной архитектуры ARM в формате qcow2" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="Образ OpenStack для 64-битной архитектуры ARM в формате raw" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
          <li>64-битный образ для Little Endian PowerPC (<a title="Образ OpenStack для 64-битной архитектуры Little Endian PowerPC в формате qcow2" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="Образ OpenStack для 64-битной архитектуры Little Endian PowerPC в формате raw" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
        </ul>
      </li>
      <li>локальная виртуальная машина QEMU, в формате qcow2 или raw.
      <ul class="quicklist downlist">
	   <li>64-битный образ для AMD/Intel (<a title="Образ QEMU для 64-битных архитектур AMD/Intel в формате qcow2" href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a title="Образ QEMU для 64-битных архитектур AMD/Intel в формате raw" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
       <li>64-битный образ для ARM (<a title="Образ QEMU для 64-битной архитектуры ARM в формате qcow2" href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a title="Образ QEMU для 64-битной архитектуры ARM в формате raw" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
	   <li>64-битный образ для PowerPC с порядком байтов от младшего к старшему (<a title="Образ QEMU для 64-битной архитектуры PowerPC с порядком байтов от младшего к старшему в формате qcow2" href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a title="Образ QEMU для 64-битной архитектуры PowerPC с порядком байтов от младшего к старшему в формате raw" href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>в Amazon EC2 в виде образа машины или через AWS Marketplace.
        <ul class="quicklist downlist">
          <li><a title="Amazon Machine Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">ОБразы машин Amazon</a></li>
          <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
        </ul>
      </li>
      <li>в Microsoft Azure через Azure Marketplace.
        <ul class="quicklist downlist">
          <li><a title="Debian 12 в Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
          <li><a title="Debian 11 в Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
