#use wml::debian::blend title="Debian Hamradio Pure Blend" NOHEADER="true" BLENDHOME="true"
#use wml::debian::recent_list
#use wml::debian::blends::hamradio
#use wml::debian::translation-check translation="9bc4ecf1abce08efe4132833b3d0ba344ca3ff10"

<div id="splash">
	<h1 id="hamradio">Debian Hamradio Ren Blandning</h1>
</div>

<p><b>Debian Hamradio Ren Blandning</b> är ett projekt från <a
href="https://wiki.debian.org/DebianHams/">Debian Hamradio Underhållargruppen</a>
som samarbetar med underhåll av amatörradiorelaterade paket för Debian.
Varje <a href="https://blends.debian.org/">Ren Blandning</a> är en delmängd av
Debian som är konfigurerad att stödja en speciell målgrupp out-of-the-box.
Denna blandning har målet att stödja radioamatörers behov.</p>

<p><a href="./about">Läs mer&hellip;</a></p>

<div id="hometoc">
<ul id="hometoc-cola">
  <li><a href="./about">Om blandningen</a></li>
  <li><a href="./News/">Nyhetsarkiv</a></li>
  <li><a href="./contact">Kontakta oss</a></li>
</ul>
<ul id="hometoc-colb">
  <li><a href="./get/">Skaffa blandningen</a>
  <ul>
    <li><a href="./get/metapackages">Använda metapaketen</a></li>
  </ul></li>
</ul>
<ul id="hometoc-colc">
  <li><a href="./docs">Dokumentation</a>
  <ul>
    <li><a href="https://wiki.debian.org/DebianHams/Handbook">Handbok för Debian Hamradio</a></li>
  </ul></li>
  <li><a href="./support">Stöd</a></li>
</ul>
<ul id="hometoc-cold">
  <li><a href="./dev">Utveckling</a>
    <ul>
      <li><a href="https://wiki.debian.org/DebianHams">Hamradio Underhållargruppen</a></li>
      <li><a href="https://www.debian.org/doc/user-manuals#hamradio-maintguide">Hamradio Underhållarguide</a></li>
    </ul>
  </li>
</ul>
<ul id="hometoc-cole">
  <li><a href="https://twitter.com/DebianHamradio"><img src="Pics/twitter.gif" alt="Twitter" width="80" height="15" /></a></li>
</ul>
</div>

<h2>Komma igång</h2>

<ul>
<li>Om du redan har Debian installerat, kolla på <a
href="./get/metapackages">listan på metapaket</a> för att upptäcka
amatörradiomjukvara som du kan installera.</li>
<li>För att få hjälp med att använda amatörradiomjukvara i Debian kan du
använda en av våra <a href="./support">stödkanaler</a>.</li>
</ul>

<h2>Nyheter</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6',
'$(ENGLISHDIR)/blends/hamradio', '', '\d+\w*' ) :>
</p>

