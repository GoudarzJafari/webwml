#use wml::debian::template title="Alpha-anpassningen -- Länkar" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/alpha/menu.inc"
#use wml::debian::translation-check translation="9bc02718accd3c01750374e028c798608a099281"

<h1>Alpha-länkar</h1>


<div class="note">Den ursprungliga <q>AlphaLinux.org</q>-webbplatsen existerar
inte längre, men en kopia har laddats upp till GitHub. Länkarna nedan har
uppdaterats till deras motsvarande sidor på GitHub där det är möjligt.
För ytterligare information om sidarkivet på GitHub, vänligen se
beskrivningen på
<a href="http://www.alphalinux.org/wiki/index.php/Former_alphalinux.org_content">Tidigare
innehåll på alphalinux.org</a>.</div>

<ul>

<li><a href="http://www.alphalinux.org/">AlphaLinux.org</a><br />
Denna sida innehåller allt du vill känna till eller kan behöva angående att köra
Linux på Alpha-maskiner. Den inkluderar många länkar och en excellent
nyhetssektion. Webbsidan är även hemmet för de officiella 
<a href="https://github.com/alphalinux/web/blob/master/docs/">FAQ och HOWTO</a> relaterade till Linux på Alpha-maskiner.</li>

<li><a href="https://github.com/alphalinux/web/blob/master/docs/alpha-howto.html">\
Introduktion till Alpha-system</a><br />
Ett rätt gammalt, men fortfarande informativt dokument för nya eller
erfarna användare.
Det beskriver skillnaderna mellan Alpha och andra arkitekturer utöver att det
beskriver de olika systemen. <b>Om du har ett gammalt system och du verkligen
vill veta vilket system du kör, oberoende av vad som står på lådan, se
<a href="https://github.com/alphalinux/web/blob/master/docs/alpha-howto.html#L225">detta underavsnitt</a>.</b></li>

<li><a href="https://github.com/alphalinux/web/blob/master/faq/FAQ.html">Linux/Alpha FAQ</a><br />
En god källa för information. Linux/Alpha-FAQen sattes ursprungligen samman
av Red Hat-användare, men har utökats för att inkludera mer allmän information
och är fortfarande en god referens.</li>

<li><a href="https://github.com/alphalinux/web/blob/master/faq/SRM-HOWTO/index.html">SRM Firmware HOWTO</a><br />
Den officiella HOWTOn för system med SRM-firmware. Om du behöver använda
<a href="https://sourceforge.net/projects/aboot/">aboot</a>
för att starta din Alpha, så är detta sidan för dig.
#This HOWTO is included in
#the latest version of the
#<a href="https://tracker.debian.org/pkg/aboot">Debian packet</a>
#as well.
</li>

<li><a href="https://github.com/alphalinux/web/blob/master/faq/MILO-HOWTO/t1.html">MILO HOWTO</a><br />
Officiella HOWTOn för MILO. Vänligen notera att
<a href="$(HOME)/devel/debian-installer/">debian-installer</a> saknar MILO-stöd.
SRM rekommenderas starkt, men om du inte kan växla och är intresserad av
MILO-stöd i utgåvor av Debian efter Woody, läs
<a href="https://lists.debian.org/debian-alpha/2004/debian-alpha-200402/msg00003.html">detta meddelande till debian-alpha</a> och
prenumerera på sändlistan
<a href="https://lists.debian.org/debian-boot/">debian-boot</a> för att lägga till MILO-stöd. 
De senast kända platserna är
<a href="http://www.suse.de/~stepan/">Stepan Rainauers sida</a>, <a href="ftp://genie.ucd.ie/pub/alpha/milo/">Nikita Schmidt</a> 
och arbetet som gjorts av <a href="http://dev.gentoo.org/~taviso/milo/">Gentoo</a>.</li>

<li><a href="https://github.com/alphalinux/web/blob/master/faq/alphabios-howto.html">AlphaBIOS Firmware HOWTO</a><br />
Officiella HOWTOn för system som använder AlphaBIOS-firmware.</li>

<li><a href="https://digital.com/about/dec/">Digital's Gamla Dokumentationbibliotek</a> <!-- with a <a href="ftp://ftp.unix-ag.org/user/nils/Alpha-Docs/">Mirror by Nils Faerber</a> --> </li>

<!-- <li><a href="http://www.alphanews.net/">alphanews.net</a><br />
Some alpha related news are posted here, for several OS which run or used
to run on alphas.</li> -->

<li><a href="http://www.helgefjell.de/browser.php">Webbläsare som kör på Linux Alpha</a><br />
Om du har problem med din webbläsare på ett 64-bitars system (det bör inte vara fallet längre)
eller om du bara vill testa en ny webbläsare, så kan du hitta en lista på webbläsare
som är kända att fungera (och även de som inte fungerar).</li>

<li><a href="http://alphacore.info/wiki/">AlphaCore WikiPage</a><br />
Denna Wiki, som för närvarande huvudsakligen fokuserar på AlphaCore 
(Fedora Core på Alpha) har även som mål att samla allmän Alpha-relaterad
information.</li>
</ul>

<p>
Tack till Nils Faerber för att han tillåter mig att inkludera delar av hans 
länksamling här.
</p>

<h1><a name="lists">Sändlistor</a></h1>

<ul>

<li>Sändlistan debian-alpha<br />
Skicka ett ebrev med ämnesraden 'subscribe' till
<email "debian-alpha-request@lists.debian.org" /> för att prenumerera.
<a href="https://lists.debian.org/debian-alpha/">Arkiv finns tillgängliga</a>.
</li>

<li>Red Hat's Linux/Alpha-sändlista

<p>
Denna lista riktar sig till Red Hat's Linux/Alpha-användare, men har även
värdefulla tips för allmäna Linux-Alpha-problem. För att prenumerera
gå till <a href="https://www.redhat.com/mailman/listinfo/axp-list">Listinformationssidan</a>.
<a href="https://www.redhat.com/archives/axp-list/">Arkiv</a> från denna sändlista finns också tillgängliga.
Ett alternativt (sökbart) arkiv kan hittas på
<url "http://www.lib.uaa.alaska.edu/axp-list/" />.
</p></li>

</ul>
