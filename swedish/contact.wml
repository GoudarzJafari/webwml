#use wml::debian::template title="Hur du kan kontakta oss" NOCOMMENTS="yes" MAINPAGE="true"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">Allmän information</a>
  <li><a href="#installuse">Installera och använda Debian</a>
  <li><a href="#press">PR/presskontakt</a>
  <li><a href="#events">Evenemang/konferenser</a>
  <li><a href="#helping">Hjälpa Debian</a>
  <li><a href="#packageproblems">Rapportera problem i Debianpaket</a>
  <li><a href="#development">Debianutveckling</a>
  <li><a href="#infrastructure">Problem med Debianinfrastruktur</a>
  <li><a href="#harassment">Trakasserier</a>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Vi ber er vänligen att skicka
initiala förfrågningar på <strong>engelska</strong>, eftersom detta är det språk
som de flesta av oss talar. Om detta inte är möjligt, vänligen fråga om hjälp
på någon av våra <a href="https://lists.debian.org/users.html#debian-user">användarsändlistor</a>,
som finns tillgängliga på många olika språk, även en
<a href="https://lists.debian.org/users.html#debian-user-swedish">svensk</a>.</p>
</aside>

<p>
Debian är en stor organisation och det finns många sätt att kontakta projektmedlemmar
och andra Debiananvändare.
Denna sida sammanfattar de oftast efterfrågade kontaktsätten, men gör inget
anspråk på att vara fullständig.
Se övriga webbsidor för andra kontaktmetoder.
</p>

<p>
Vänligen notera att de flesta av e-postadresserna nedan representerar öppna
sändlistor med publika arkiv. Läs <a href="$(HOME)/MailingLists/disclaimer">ansvarsfriskrivningen</a>
innan du skickar några meddelanden.
</p>


<h2 id="generalinfo">Allmän information</h2>

<p>
Huvuddelen av informationen om Debian är samlad på vår webbplats
<a href="$(HOME)">https://www.debian.org/</a>, så läs och
<a href="$(SEARCH)">sök</a> igenom den innan du kontaktar oss.</p>

<p>
Du kan skicka frågor om Debianprojektet till sändlistan <email debian-project@lists.debian.org>.
Vänligen ställ inte allmänna frågor om Linux till denna sändlista; läs vidare
för mer information om andra sändlistor.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">Läs vår FAQ</a></button></p>


<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Installera och använda Debian</h2>

<p>
Om du är säker på att dokumentationen på installationsmedia och på vår
webbplats inte har lösningen till ditt problem, så finns det en väldigt
aktiv användarsändlista där Debiananvändare och utvecklare kan besvara dina
frågor. Här kan du ställa alla frågor om följande ämnen:
</p>

<ul>
  <li>Installation</li>
  <li>Konfiguration</li>
  <li>Hårdvara med stöd</li>
  <li>Maskinadministration</li>
  <li>Använda Debian</li>
 </ul>

<p>
Vänligen <a href="https://lists.debian.org/debian-user/">prenumerera</a> på
sändlistan och skicka dina frågor till <email debian-user@lists.debian.org>.
</p>

<p>
Utöver detta så finns det användarsändlistor för dom som talar olika språk.
Se prenumerationsinformationen för <a href="https://lists.debian.org/users.html#debian-user">internationella sändlistor</a>.
</p>



<p>
Du kan bläddra igenom våra <a href="https://lists.debian.org/">sändlistearkiv</a>
eller <a href="https://lists.debian.org/search.html">söka</a> igenom arkiven
utan att prenumerera.
</p>

<p>
Utöver det så är det möjligt att söka igenom sändlistor som nyhetsgrupper.
</p>

<p>
Om du tror att du har hittat ett fel i vårat installationssystem, vänligen
skriv till sändlistan <email debian-boot@lists.debian.org>. Du kan också
skicka en <a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">felrapport</a>
mot pseudopaketet <a href="https://bugs.debian.org/debian-installer">debian-installer</a>.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">PR/Presskontakt</h2>

<p>
<a href="https://wiki.debian.org/Teams/Publicity">Debians publicetetsgrupp</a>
modererar nyheter och tillkännagivanden i alla Debians officiella resurser,
exempelvis några sändlistor, bloggen, webbplatsen för mikronyheter, och
officiella kanaler för sociala medier.
</p>

<p>
Om du skriver om Debian och behöver hjälp eller information, vänligen kontakta
<a href="mailto:press@debian.org">publicitetsgruppen</a>. Detta är även rätta
adressen om du planerar att skicka nyhter för vår egen nyhetssida.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Evenemang och Konferenser</h2>

<p>
Vänligen skicka inbjudningar för <a href="$(HOME)/events/">konferenser</a>,
utställningar, eller andra evenemang till <email events@debian.org>.
</p>

<p>
Förfrågningar och flyers, posters och deltagande i Europa skall skickas till
sändlistan <email debian-events-eu@lists.debian.org>.
</p>


<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Hjälpa Debian</h2>


<p>
Det finns en mängd möjligheter att stödja Debianprojektet och bidra till
distributionen. Om du vill erbjuda hjälp, vänligen besök vår
sida för <a href="devel/join/">Hur man går med i Debian</a> först.
</p>

<p>
Om du vill stå värd för en Debianspegel, se sidorna om att
<a href="mirror/">spegla Debian</a>.
Nya speglar sänds in via
<a href="mirror/submit">detta formulär</a>.
Problem med existerande speglar kan rapporteras till
<email mirrors@debian.org>.
</p>


<p>
Om du vill sälja Debian-cds/dvds, se
<a href="CD/vendors/info">information för cd/dvd-distributörer</a>.
För att komma med i listan över cd-distributörer, vänligen
använd <a href="CD/vendors/adding-form">detta formulär</a>.
</p>


<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
 <h2 id="packageproblems">Rapportera problem i Debianpaket</h2>

<p>
Om du vill rapportera ett fel i ett Debianpaket har vi ett
felrapporteringssystem där du enkelt kan sända in din rapport.
Läs
<a href="Bugs/Reporting">instruktionerna om att sända in felrapporter</a>.
</p>

<p>
Om du bara vill kommunicera med den ansvarige för ett Debianpaket kan du
använda de speciella e-postalias som skapats för alla paket.
Alla brev som sänds till &lt;<var>paketnamn</var>&gt;@packages.debian.org
kommer vidaresändas till den ansvarige utvecklaren för det paketet.
</p>


<p>Om du på ett diskret sätt vill göra utvecklarna uppmärksamma på
ett säkerhetsproblem i Debian, sänd e-post till
<email security@debian.org>.
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
 <h2 id="development">Debianutveckling</h2>

<p>Om du har frågor om utvecklingen av Debian, så har vi
flera <a href="https://lists.debian.org/devel.html">sändlistor</a>
för detta. Här kan du komma i kontakt med våra utvecklare.
</p>

<p>
Det finns även en allmän sändlista för utvecklare: <email debian-devel@lists.debian.org>.
Vänligen följ denna <a href="https://lists.debian.org/debian-devel/">länk</a> för att
prenumerera på denna.
</p>


<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Problem med Debianinfrastrukturen</h2>

<p>
För att rapportera problem med en Debiantjänst kan du vanligen
<a href="Bugs/Reporting">rapportera ett fel</a> mot motsvarande
<a href="Bugs/pseudo-packages">pseudopaket</a>.

<p>Alternativt kan du skicka e-post till en av följande adresser:</p>

<define-tag btsurl>package: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
 <dt>Webbsideredaktionen
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org>
 <dt>Webbsidesöversättare
  <dd><a href="international/Swedish/bidragslamnare#web">Mer information</a>
 <dt>Administratörer för sändlistorna och arkiven
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org>
 <dt>Felrapporteringssystemets administratörer
  <dd><btsurl bugs.debian.org><br />
      <email owner@bugs.debian.org>
</dl>



<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">Trakasserier</h2>

<p>Debian är en gemenskap av folk som värderar respekt. Om du är ett offer för
olämpligt beteende, om någon i projektet har skadat dig, eller om du känner
dig trakasserad, vänligen kontakta gemenskapsgruppen:
<email community@debian.org>
</p>
