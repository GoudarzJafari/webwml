#use wml::debian::template title="Hämta Debian" BARETITLE=true
#use wml::debian::translation-check translation="96a44b00371cb1e75df59d859a900aab253ee412"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Denna sida har alternativ för att hämta och installera stabila utgåvan av Debian.


<ul>
<li> <a href="../CD/http-ftp/#mirrors">Hämtningsspeglingar</a> av installationsavbildningar
<li> <a href="../releases/stable/installmanual">Installationsmanualen</a> med detaljerade installationsinstruktioner
<li> <a href="../releases/stable/releasenotes">Versionsfakta</a>
<li> <a href="../devel/debian-installer/">ISO-avbildningar för Debians uttestningsutgåva</a>
<li> <a href="../CD/verify">Kontrollera äktheten på Debianavbildningar</a>
</ul>



<div class="line">
  <div class="item col50">
    <h2>Hämta en installeraravbildning</h2>
    <ul>
      <li>En <a href="netinst"><strong>liten installeraravbildning</strong></a>:
		 kan hämtas snabbt från internet och bör skrivas till en CD. För
		 att använda denna så behöver du en maskin med en internetuppkoppling
		 tillgänglig.
	<ul class="quicklist downlist">
	  <li><a title="Hämta installationsprogrammet för 64-bitars Intel- och AMD-maskiner"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bitars
	      PC netinst iso</a></li>
	  <li><a title="Hämta installationsprogrammet för normala 32-bitars Intel- och AMD-maskiner"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bitars
	      PC netinst iso</a></li>
	  <li><a title="Hämta CD-torrents för 64-bitars Intel- och AMD-maskiner"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bitars PC torrents (CD)</a></li>
	  <li><a title="Hämta CD-torrents för normala 32-bit Intel- och AMD-maskiner"
		 href="<stable-images-url/>/i386/bt-cd/">32-bitars PC torrents (CD)</a></li>
	</ul>
      </li>
      <li>En större <a href="../CD/"><strong>komplett
		installeraravbildning</strong></a>: innehåller fler paket, gör det lättare att installera
		på maskiner utan tillgång till internet.
	<ul class="quicklist downlist">
         <li><a title="Hämta installerare för 64-bitars Intel och AMD PC-maskiner"
                href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">64-bitars
             PC DVD-1 iso</a></li>
         <li><a title="Hämta installerare för normala 32-bitars Intel och AMD PC-maskiner"
                href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">32-bitars
             PC DVD-1 iso</a></li>
	  <li><a title="Hämta DVD-torrent för 64-bitars Intel- och AMD-maskiner"
	         href="<stable-images-url/>/amd64/bt-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso.torrent">64-bitars PC torrents (DVD)</a></li>
	  <li><a title="Hämta DVD-torrent för normala 32-bitars Intel- och AMD-maskiner"
            href="<stable-images-url/>/i386/bt-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso.torrent">32-bitars PC torrents (DVD)</a></li>

	</ul>
      </li>
    </ul>
  </div>
   <div class="item col50 lastcol">
    <h2>Pröva Debian innan du installerar</h2>
    <p>
	   Du kan prova Debian genom att starta ett live-system från en CD, DVD eller
		ett USB-minne utan att installera några filer på din dator. Du kan även
      köra den inkluderade <a href="https://calamares.io">\
		Calamares-installeraren</a>. Den finns bara för 64-bitars PC.
      Läs mer <a href="../CD/live#choose_live">information om denna metod</a>.
    </p>
    <ul class="quicklist downlist">
       <li><a title="Hämta Gnome live-ISO för 64-bitars Intel och AMD PC"
           href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
       <li><a title="Hämta Xfce live-ISO för 64-bitars Intel och AMD PC"
           href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
       <li><a title="Hämta KDE live-ISO för 64-bitars Intel och AMD PC"
           href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
       <li><a title="Hämta andra live-ISOs för 64-bitars Intel och AMD PC"
             href="<live-images-url/>/amd64/iso-hybrid/">Andra live-ISOs</a></li>
       <li><a title="Hämta live-torrents för 64-bitars Intel och AMD PC"
           href="<live-images-url/>/amd64/bt-hybrid/">live-torrents</a></li>
 </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Köpa cd-, dvd- eller USB-minnen från en
    försäljare av installationsmedia för Debian</a></h2>

  <p>
   Många distributörer säljer distributionen för mindre än femtio kronor
   eller fem euro plus frakt (men inte alltid internationellt).
  </p>

  <p>
   Här är de grundläggande fördelarna med cd-uppsättningarna:
  </p>

  <ul>
   <li>Du kan installera på maskiner utan internetanslutning.</li>
   <li>Du kan installera Debian utan att hämta alla paket själv.</li>
  </ul>

  <h2><a href="pre-installed">Köpa en dator med Debian
      förinstallerat</a></h2>
  <p>
   Det finns flera fördelar med detta:
  </p>

  <ul>
   <li>Du behöver inte installera Debian.</li>
   <li>Installationen är förkonfigurerad för att passa med maskinvaran.</li>
   <li>Återförsäljaren kan ge teknisk support.</li>
  </ul>
  </div>
<div class="item col50 lastcol">
    <h2>Använd en av Debians molnavbildningar</h2>
    <p>En officiell <a href="https://cloud.debian.org/images/cloud/"><strong>molnavbildning</strong></a>, byggd av molngruppen, kan användas på:</p>
    <ul>
      <li>din OpenStackleverantör, i qcow2- eller rawformat.
      <ul class="quicklist downlist">
      <li>64-bitars AMD/Intel (<a
         title="OpenStackavbildning för 64-bitars AMD/Intel qcow2"
         href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a
         title="OpenStackavbildning för 64-bitars AMD/Intel raw"
         href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
       <li>64-bitars ARM (<a
         title="OpenStackavbildning för 64-bitars ARM qcow2"
         href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a
         title="OpenStackavbildning för 64-bitars ARM raw"
         href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
       <li>64-bitars Little Endian PowerPC (<a
         title="OpenStackavbildning för 64-bitars Little Endian PowerPC qcow2"
         href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a
         title="OpenStackavbildning för 64-bitars Little Endian PowerPC raw"
         href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>en lokal QEMU virtuell maskin, i qcow2- eller raw-format.
      <ul class="quicklist downlist">
            <li>64-bitars AMD/Intel (<a title="QEMU-avbildning för 64-bitars AMD/Intel qcow2"
               href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>,
               <a title="QEMU-avbildning för 64-bitars AMD/Intel raw"
               href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)
            </li>
            <li>64-bitars ARM (<a title="QEMU-avbildning för 64-bitars ARM qcow2"
               href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>,
               <a title="QEMU-avbildning för 64-bitars ARM raw"
               href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)
            </li>
            <li>64-bitars Little Endian PowerPC (<a title="QEMU-avbildning för 64-bitars Little Endian PowerPC qcow2"
               href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>,
               <a title="QEMU-avbildning för 64-bitars Little Endian PowerPC raw"
               href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)
            </li>
      </ul>
      </li>
      <li>Amazon EC2, antingen som en maskinavbildning eller via AWS Marketplace.
      <ul class="quicklist downlist">
       <li><a title="Amazon Machine Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon Machine Images</a></li>
       <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
      </ul>
      </li>
      <li>Microsoft Azure, på Azure Marketplace.
      <ul class="quicklist downlist">
         <li><a title="Debian 12 på Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
         <li><a title="Debian 11 på Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
      </ul>
      </li>
    </ul>
  </div>
</div>
