#use wml::debian::cdimage title="Live-avbildningar"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="7791836a42fa0b102a9f31cfc78e2c4345504388"

<p>En <q>live-installeringsavbildning</q> innehåller ett Debiansystem som kan starta utan att
modifiera några filer på hårddisken, och tillåter även installation av Debian
från avbildningens innehåll.
</p>

<p><a name="choose_live"><strong>Passar en live-avbildning för mig?</strong></a> Här är några saker
att överväga som kan hjälpa dig att bestämma dig.
<ul>
<li><b>Varianter:</b> Live-avbildningarna kommer i olika varianter, varav
flera tillhandahåller val av skrivbordsmiljö (GNOME, KDE, LXDE, Xfce,
Cinnamon och MATE).
<li><b>Arkitektur:</b> För närvarande tillhandahålls endast avbildningar för
64-bitars PC (amd64).
<li><b>Installeraren:</b> Live-avbildningarna innehåller
den användarvänliga <a href="https://calamares.io">Calamaresinstalleraren</a>, ett
distributionsoberoende installerarramverk.
<li><b>Språk:</b> Avbildningarna innehåller inte en komplett uppsättning paket
för språkstöd.
Om du behöver indatametoder, typsnitt och andra språkpaket
för ditt språk, kommer du behöva installera dessa efteråt.
</ul>

</div>

<div class="line">
<div class="item col50">
<h2 id="live-install-stable">Officiella live-installationsavbildningar för den <q>stabila</q> utgåvan</h2>

<p>Dessa avbildningar är lämpliga för att testa ett Debiansystem och sedan
installera det från samma media. De kan skrivas till USB-minnen och
DVD-R(W)-media.</p>
    <ul class="quicklist downlist">
      <li><a title="Hämta Gnome live-ISO för 64-bitars Intel och AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Hämta Xfce live ISO för 64-bitars Intel och AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Hämta KDE live ISO för 64-bitars Intel och AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Hämta other live ISO för 64-bitars Intel och AMD PC"
            href="<live-images-url/>/amd64/iso-hybrid/">Andra live-ISOs</a></li>
      <li><a title="Hämta livetorrents för 64-bitars Intel och AMD PC"
          href="<live-images-url/>/amd64/bt-hybrid/">livetorrents</a></li>
    </ul>

</div>

<div class="item col50 lsatcol">
<h2 id="live-install-testing">Officiella live-installationavbildningar för <q>uttestningsutgåvan</q></h2>

<p>Dessa avbildningar är lämpliga för att testa ett Debiansystem och sedan
installera det från samma media. De är lämpliga att skriva till USB-minnen
och DVD-R(W)-media.</p>
    <ul class="quicklist downlist">
      <li>UTTESTNINGSAVBILDNINGAR:</li>
      <li><a title="Hämta Gnome live-ISO för 64-bitars Intel och AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Hämta Xfce live-ISO för 64-bitars Intel och AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Hämta KDE live-ISO för 64-bitars Intel och AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Hämta andra live-ISOs för 64-bitars Intel och AMD PC"
            href="<live-images-testing-url/>/amd64/iso-hybrid/">Andra live-ISOs</a></li>
    </ul>

</div>

</div>


<p>För information om vad dessa filer är och hur de används, se
vår <a href="../faq/">FAQ</a>.</p>

<p>Besök sidan för <a href="$(HOME)/devel/debian-live">Debian Live Project</a>
för mer information om Debian Live-systemet som tillhandahålls av dessa
avbildningar.</p>

