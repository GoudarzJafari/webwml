#use wml::debian::cdimage title="Nätverksinstallation från en minimal USB, cd"
#use wml::debian::translation-check translation="678a572e92767d98d57f67822e00905782bf9637"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p>På arkitekturerna i386 och amd64, kan alla USB/cd/dvd-avbildningar
<a href="https://www.debian.org/CD/faq/#write-usb">användas på en USB-pinne</a> också.</div>


<p>
En cd för <q>nätverksinstallation</q> eller <q>netinst</q>-cd
är en enkel cd som gör det möjligt att installera hela systemet.
Cd:n innehåller bara den minimala uppsättning programvara som behövs för att
installera grundsystemet och hämta resterande paket över Internet.
</p>

<p>
<strong>Vilka sorters nätverksanslutningar stöds under installationen?</strong>
Olika metoder stöds för detta, som Ethernet och WLAN (med vissa begränsningar)
</p>

<ul>
 <li>
  Officiella nätinstallationsavbildningar för den stabila utgåvan
  &ndash; <a href="#netinst-stable">se nedan</a>
 </li>

 <li>
  Avbildningar för <q>uttestningsutgåvan</q>, se
  <a href="$(DEVEL)/debian-installer/">Debian-Installer-sidan</a>.
 </li>
</ul>

<h2 id="netinst-stable">Officiella nätinstallationsavbildningar för den
stabila utgåvan</h2>

<p>
Dessa avbildningar innehåller 
installationsprogrammet och en liten uppsättning paket som gör det möjligt att 
installera ett (väldigt) grundläggande system.
</p>

<div class="line">
<div class="item col50">
<p><strong>CD för nätinstallation</strong></p>

  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>CD för nätinstallation (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)</strong></p>
         <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>


<p>För information om dessa filer och hur du använder dem, se
<a href="../faq/">frågor och svar</a>.</p>

<p>
När du har hämtat avbildningarna, se till att läsa den
<a href="$(HOME)/releases/stable/installmanual">
detaljerade informationen om installationsproceduren</a>.
</p>


