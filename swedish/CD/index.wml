#use wml::debian::cdimage title="Debian installationsmedia för USB, cd, dvd" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e"

<div class="tip">
<p>Om du vill installera Debian på en dator med internetuppkoppling
rekommenderar vi
<a href="netinst/">nätverksinstallation</a>
vilket är en mindre nedladdningen.</p> </div>

<div class="tip">
<p>På i386- och amd64-arkitekturerna kan alla USB/cd/dvd-avbildningar även
<a href="https://www.debian.org/CD/faq/#write-usb">användas på usb-minnen</a>.</p>
</div>

<ul>

  <li><a href="http-ftp/">Hämta USB-/cd-/dvd-avbildningar med hjälp av HTTP.</a></li>
  
  <li><a href="torrent-cd/">Hämta USB/cd/dvd-avbildningar med BitTorrent.</a>
  Bittorrents peer till peer-system låter många användare samarbeta för att
  ladda ner avbildningar samtidigt, vilket kan snabba upp din hämtning.
  </li>

  <li><a href="live/">Hämta liveavbildningar med hjälp av HTTP, FTP eller BitTorrent.</a>
  Liveavbildningar är för att starta ett livesystem utan att installera.
  Du kan använda detta för att testa Debian första och sedan installera
  innehållet på avbildningen.</li>


  <li><a href="vendors/">Köp färdig Debian-media</a>.</li>

  <li><a href="jigdo-cd/">Hämta cd-/dvd-avbildningar med jigdo.</a>
  Detta är endast för avancerade användare. 
  &rdquo;jigdo&rdquo;-systemet låter dig välja den
  snabbaste av 300 Debianspeglar över hela världen att hämta från.
  Bland funktionerna finns enkelt val av speglar och
  &rdquo;uppgradering&rdquo; av gamla avbildningar till den senaste
  utgåvan.
  Det är även det enda sättet att hämta alla Debians dvd-avbildningar.
  </li>
  
  <li>Om du har problem, vänligen se <a href="faq/">FAQ om
  Debian CDs/DVDs</a>.</li>

</ul>

<p>Officiella USV/cd/dvd-utgåvor är signerade så att du kan
<a href="verify">kontrollera att de är äkta</a>.</p>

<p>Debian är tillgängligt för olika datorarkitekturer (De flesta kommer
att behöva avbildningar för <q>amd64</q>, alltså 64-bitars PC-kompatibla
system.</p>

      <div class="cdflash" id="latest">Senaste officiella utgåvan av
      USB/cd/dvd-avbildningar för den stabila versionen:
        <strong><current-cd-release></strong>.
      </div>

<p>
Information om kända problem i installationsprogrammet finns i
<a href="$(HOME)/releases/stable/debian-installer/">installationsinformationen</a>.<br>
</p>
