#use wml::debian::translation-check translation="9ca54b709be026d48e87cb9ede9c4783615772c0"
<define-tag pagetitle>Äganderätt för domänen <q>debian.community</q></define-tag>
<define-tag release_date>2022-08-07</define-tag>
#use wml::debian::news

<p>
World Intellectual Property Organization (WIPO), under dess
policy för tvistlösning av domännamn (Uniform Domain-Name Dispute-Resolution
Policy - UDRP), beslutade att äganderätten för domänen
<q><a href="https://debian.community">debian.community</a></q> skall <a 
href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2022-1524">överföras 
till Debianprojektet</a>.
</p>

<p>
Den utsedda panelen fann att <q>det omtvistade domännamnet är identiskt med ett
varumärke där målsägande har rättigheter.</q>
</p>

<p>
I sitt beslut noterade panelen:
</p>

<blockquote>
<p>
[...] det omtvistade domännamnet är identiskt till DEBIAN-varumärket, vilket
medför en hög risk av underförstådd anknytning till målsägande, [...] Givet
att målsägande framträdande beskriver Debian på sin hemsida som en 'gemenskap'
och inte bara som ett operativsystem, [.community-] suffixet förstärker att det
omtvistade domännamnet kommer att leda till en webbplats som drivs eller stöds
av [Debian]. Det omtvistade domännamnet innehåller inga kritiska eller andra
termer för att skingra eller kvalificera detta felaktiga intryck.
</p>
</blockquote>

<p>
Panelen fortsatte att observera att:
</p>

<blockquote>
<p>
Bevisen som lämnats av målsägande visar att vissa inlägg presenterar
DEBIAN-varumärket med information om en ökänd sexkult, ökända sexförbrytare,
och förslavning av kvinnor, och ett inlägg visar fotografier av fysiska märken
som påstås vara på offrens genitaliehud. Skillnaderna på informationen från
målsägande till denna typ av information är konstruerat och omfattningen av
denna information är inte bara tillfällig på webbplatsen. Enligt panelens
uppfattning är dessa inlägg medvetet avsedda att skapa en falsk association
mellan DEBIAN-varumärket och stötande fenomen för att därmed smutskasta
varumärket.
</p>
</blockquote>

<p>
och drog vidare slutsatsen att:
</p>

<blockquote>
<p>
inget i Debians sociala kontrakt eller på annan plats indikerar att målsägande
någonsin har samtyckt till typen av falska associationer med varumärket som
publicerats av svarande på deras webbplats. Svarande påpekar att
DEBIAN-varumärket endast är registrerat gällande mjukvara. 
Men medan de relevanta inläggen attackerar medlemmar av målsägande som gör
DEBIAN-mjukvaran tillgänglig, till skillnad från själva mjukvaran, använder
dessa inlägg varumärket i kombination med det bestridda domännamnet på ett sätt
som avsiktligt försöker skapa falska associationer med själva varumärket.
</p>
</blockquote>

<p>
Debian är hängivet till korrekt användning av sina varumärken enligt deras
<a href="$(HOME)/trademark">varumärkespolicy</a> och kommer att fortsätta
att ta åtgärder när denna policy bestrids.
</p>

<p>
Innehållet i <q>debian.community</q> har nu ersatts av en 
<a href="$(HOME)/legal/debian-community-site">sida</a>
som förklarar situationen och besvarar ytterligare frågor.
</p>

<p>
WIPO's fullständiga beslutstext finns tillgänglig online:
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273">https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273</a>

<h2>Om Debian</h2>

<p>
	Debianprojektet är en sammanslutning av utvecklare av fri mjukvara som
	ger frivilligt av sin tid och insats för att producera det helt fria
	operativsystemet Debian.
</p>

<h2>Kontaktinformation</h2>

<p>
	För mer information, besök vänligen Debians webbplats på
	<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på 
	engelska) till &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>
