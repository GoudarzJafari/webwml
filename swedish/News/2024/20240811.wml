#use wml::debian::translation-check translation="175886d0881f4c17bea683840199ffa0e055b466"
<define-tag pagetitle>DebConf24 i Busan avslutas och datumen för DebConf25 tillkännages</define-tag>
<define-tag release_date>2024-08-11</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Lördagen 3 augusti 2024 avslutades den årliga Debian utvecklar- och
bidragslämnarkonferensen.
</p>

<p>
Mer än 339 deltagare som representerade 40 länder från hela världen samlades
för totalt 108 evenemang, bland annat 50 föreläsningar och diskussioner,
37 Bird of a Feath (BoF - informella möten mellan utvecklare och användare),
12 workshops, och aktiviteter för att förbättra vår distribution och
fri mjukvara (25 patchar skickades till Linuxkärnan), lära av våra mentorer
och jämlikar, bygga vår gemenskap och för att ha lite kul.
</p>

<p>
Konferensen föregicks av den årliga
<a href="https://wiki.debian.org/DebCamp">DebCamp</a> hackingsessionen som hölls
21 juli till 27 juli där Debianutvecklare och bidragslämnare sammankallades
för att fokusera på deras individuella Debianrelaterade projekt eller jobba
i grupper för att fokusera på personligt samarbete för att utveckla Debian.

Detta år innehöll en BootCamp som hölls för nybörjare med en GPG workshop
och fokus på en Introduktion till att skapa .deb-filer (Debianpaketering)
arrangerat av en grupp dedicerade mentorer som delade hands-on-erfarenhet i
Debian och erbjöd en djupare förståelse för hur man arbetar i och bidrar
till gemenskapen.
</p>

<p>
Den faktiska Debianutvecklarekonferensen startade söndagen 28 juli 2024.

Utöver den traditionella 'Bitar från DPL'-föreläsningen, den kontinuerliga
nyckelsigneringsfesten, blixtföreläsningar och tillkännagivandet av nästa
års DebConf25, fanns flera uppdateringssessioner som delades av interna
projekt och grupper.

Flera av de hållna diskussionssessionerna presenterades av våra tekniska
grupper inklusive de vanliga och användbara mötena med tekniska kommittén
och ftpgruppen och en uppsättning BoFs om paketeringspolicy och
Debianinfrastruktur, inklusive föreläsningar om APT och Debianinstalleraren
och en översikt om de första elva åren med reproducerbara byggen.
Internationalisering och lokalisering har varit ämnet i flera föreläsningar.
Programmeringsspråksgrupperna för Python, Perl, Ruby och Go så väl som
Medicingruppen delade även dom uppdateringar om sitt arbete och sina insatser.

Fler än femton BoFs och föreläsningar om gemenskapen, mångfald och
lokal uppsökande verksamhet markerade arbetet i de olika teamen som är
inblandade i den sociala aspekten av vår gemenskap. Detta år igen delade
Debian Brasilien strategier och aktioner för att attrahera och behålla
nya bidragslämnare och medlemmar och möjligheter både i Debian och F/OSS.
</p>

<p>
<a href="https://debconf24.debconf.org/schedule/">Schemat</a>
uppdaterades varje dag med planerade och ad-hoc-aktiviteter som introducerades
av deltagare under konferensens gång. Flera traditionella aktiviteter ägde
rum: en jobbmässa, en poesiföreställning, den traditionella ost- och vinfesten,
gruppbilderna och dagsutflykterna.
</p>

<p>
För de som inte har möjlighet att delta är de flesta av föreläsningarna och
sessionerna filmade för live-strömmar vilka görs tillgängliga senare genom
en länk i deras sammanfattning i
<a href="https://debconf24.debconf.org/schedule/">schemat</a>.
Nästan alla sessioner tillät fjärrdeltagande via IRC-meddelandeappar eller
samarbetstextdokument vilket tillät fjärrdeltagare att "vara i rummet" för att
ställa frågor eller dela kommentarer med talaren och den samlade publiken.
</p>

<p>
DebConf24 såg mer än 6.8 TiB (4.3 TiB i 2023) data strömmas, 91.25 timmar
(55 i 2023) schemalagda föreläsningar, 20 nätverksåtkomstpunkter, 1.6 km fiber
(1 trasig fiber...) och 2.2 km UTP-kabel utplacerad, mer än 20 länders
Geoip-tittare, 354 T-shirts, 3 dagsutflykterna, och upp till 200 mål mat
planerade per dag.

Alla dessa evenemang, aktiviteter, konversationer och strömmar tillsammans med
vår kärlek, intresse och deltagande i Debian och F/OSS gjorde verkligen denna
konferens till en framgång både här i Busan, Sydkorea och online runt hela
världen.
</p>

<p>
<a href="https://debconf24.debconf.org/">DebConf24s webbplats</a>
kommer att hållas aktiv för arkiveringsändamål och kommer att fortsätta att
erbjuda länkar till presentationerna och videos av föreläsningarna och
evenemangen.
</p>

<p>
Nästa år, kommer <a href="https://wiki.debian.org/DebConf/25">Debconf25</a>
att hållas i Brest, Frankrike, från måndag 7 juli till söndag 20 juli, 2025.
Som traditionen följer kommer de lokala organisatörerna i Frankrike starta
konferensen med DebCamp före konferensen med särskilt fokus på individuellt
och grupparbete för förbättring av distributionen.
</p>

<p>
DebConf arbetar för en säker och välkomnande miljö för alla
deltagare. Se
<a href="https://debconf24.debconf.org/about/coc/">webbsidan om förhållningsregler på webbplatsen för DebConf24</a>
för ytterligare detaljer om detta.
</p>

<p>
Debian tackar för åtagandet från flera <a href="https://debconf24.debconf.org/sponsors/">sponsorer</a>
för deras stöd för DebConf24, speciellt våra platinasponsorer:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://www.proxmox.com/">Proxmox</a>,
och <a href="https://www.river.com/">Wind River</a>.
</p>

<p>
Vi vill också tacka våra Video- och infrastruktursgrupper, DebConf24-
och DebConfkommitéerna, vår värdnation Sydkorea, och varje enstaka person
som har hjälpt att bidra till detta evenemang och till Debian i allmänhet.
Tack till er alla för ert arbete med att hjälpa Debian att fortsätta vara
"det universella operativsystemet". Vi ses nästa år!
</p>

<h2>Om Debian</h2>

<p>
Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
en stor mängd datortyper, kallar sig Debian det <q>universella
operativsystemet</q>.
</p>

<h2>Om DebConf</h2>

<p>DebConf är Debianprojektets utvecklarkonferens. Utöver ett fullspäckat
schema med tekniska, sociala och policytal, tillhandahåller DebConf en
möjlighet för utvecklare, bidragslämnare och andra intresserade att
mötas personligen och jobba tillsammans. Det är ägt rum årligen sedan
2000 på så vitt skiljda platser som Skottland, Argentina, och
Bosnien och Herzegovina. Mer information om DebConf finns tillgänglig
på <a href="http://debconf.org/">http://debconf.org</a>.</p>


<h2>Om Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> är en oberoende
molntjänstleverantör erkänd över hela Europa för sitt engagemang för
integritet, den lokala ekonomin och miljön. Med en tillväxt på 18% under
2023 utvecklar företaget en svit av samarbetsverktyg online och molnhosting,
streaming, marknadsföring och evenemangslösningar. Infomaniak använder
uteslutande förnybar energi, bygger egna datacenter och utvecklar sina lösningar
i Schweiz, utan att flytta. Företaget driver webbplatsen för den belgiska
radio- och TV-tjänsten (RTBF) och tillhandahåller streaming för mer än
3000 TV- och radiokanaler i Europa.
</p>

<h2>Om Proxmox</h2>
<p>
<a href="https://www.proxmox.com/">Proxmox</a> tillhandahåller kraftfull och
användarvänlig servermjukvara med öppen källkod. Företag av alla storlekar
och branscher använder Proxmox-lösningar för att distribuera effektiva och
förenklade IT-infrastrukturer, minimera den totala ägandekostnaden, och
undvika inlåsning av leverantörer. Proxmox erbjuder även kommersiell support,
utbildningstjänster och ett omfattande partnerekosystem för att säkerställa
affärskontinuitet för sina kunder. Proxmox Server Solution GmbH grundades
2005 och har sitt huvudkontor i Wien, Österrike. Proxmox bygger sina
produkterbjudanden ovanpå Debians operativsystem.
</p>

<h2>Om Wind River</h2>
<p>
<a href="https://www.windriver.com/">Wind River</a>. I nästan 20 år har
Wind River lett inom kommersiella Linux-lösningar med öppen källkod för
affärskritisk enterprise edge computing. Med expertis inom flyg-, bil-,
industri-, telekombranschen och mer är företaget engagerat i öppen källkod genom
initiativ som eLxr, Yocto, Zephyr och StarlingX.
</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök DebConf24 webbplats på
<a href="https://debconf24.debconf.org/">https://debconf24.debconf.org/</a>
eller skicka e-post (på engelska) till &lt;press@debian.org&gt;.</p>
