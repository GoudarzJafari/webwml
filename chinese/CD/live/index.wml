#use wml::debian::cdimage title="Live 安装映像"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="7791836a42fa0b102a9f31cfc78e2c4345504388"
#include "$(ENGLISHDIR)/releases/images.data"

<p><q>Live 安装映像</q>包含一个 Debian 系统，可以在不修改\
硬盘驱动器上的任何文件的情况下进行启动，并可根据\
映像内容安装 Debian。
</p>

<p><a name="choose_live"><strong>Live 映像适合我吗？</strong></a>这里有一些需要考虑的事，\
可能会帮助你作出决定。
<ul>
<li><b>风格：</b>Live 映像有几种<q>风格</q>，不同风格\
各自提供了一种桌面环境（GNOME、KDE、LXDE、Xfce、\
Cinnamon 或 MATE）。
<li><b>架构：</b>目前仅提供了用于 64 位 PC（amd64）的映像。
<li><b>安装程序：</b>Live 映像包含了\
对最终用户友好的 <a href="https://calamares.io">Calamares 安装程序</a>，\
这是一个和发行版无关的安装程序框架。
<li><b>大小：</b>每个映像都远小于全套的 \
DVD 映像，但大于网络安装[CN:介质:][HKTW:媒介:]。
<li><b>语言：</b>映像没有包含完整的语言\
[CN:支持:][HKTW:支援:]软件包。如果您需要用于您语言的输入法、字体和补充语言\
软件包，则您需要以后再安装它们。
</ul>

</div>

<div class="line">
<div class="item col50">
<h2 id="live-install-stable"><q>稳定（stable）</q>版本的官方 Live 安装映像</h2>

<p>这些映像可以让您先试用 Debian 系统，然后再通过相同的[CN:介质:][HKTW:媒介:]安装。\
映像可以写入 [CN:U 盘:][TW:USB 随身碟:][HK:USB 手指:]\
或者 DVD-R(W) [CN:介质:][HKTW:媒介:]。</p>
    <ul class="quicklist downlist">
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 Gnome live ISO"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 Xfce live ISO"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 KDE live ISO"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的其他 live ISO"
            href="<live-images-url/>/amd64/iso-hybrid/">其他 live ISO</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 live torrent 种子"
          href="<live-images-url/>/amd64/bt-hybrid/">live torrent 种子</a></li>
    </ul>


</div>

<div class="item col50 lsatcol">
<h2 id="live-install-testing"><q>测试（testing）</q>版本的官方 Live 安装映像</h2>

<p>这些映像可以让您先试用 Debian 系统，然后再通过相同的[CN:介质:][HKTW:媒介:]安装。\
映像可以写入 [CN:U 盘:][TW:USB 随身碟:][HK:USB 手指:]\
或者 DVD-R(W) [CN:介质:][HKTW:媒介:]。</p>
    <ul class="quicklist downlist">
      <li>测试版映像：</li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 Gnome live ISO"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 Xfce live ISO"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的 KDE live ISO"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-kde.iso">Live KDE</a></li>
      <li><a title="下载用于 64 位 Intel 和 AMD PC 的其他 live ISO"
            href="<live-images-testing-url/>/amd64/iso-hybrid/">其他 live ISO</a></li>
    </ul>

</div>

</div>

<p>有关这些[CN:文件:][HKTW:档案:]是什么，以及\
如何使用这些[CN:文件:][HKTW:档案:]，请参阅 \
<a href="../faq/">FAQ</a>。</p>

<p>有关 Debian Live 项目提供的 Debian Live 系统的更多信息，\
请参见 <a href="$(HOME)/devel/debian-live">Debian Live 项目网页</a>。
</p>
