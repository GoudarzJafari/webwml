#use wml::debian::template title="Debian &ldquo;squeeze&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"

<p>Debian <current_release_squeeze> was
released on <a href="$(HOME)/News/<current_release_newsurl_squeeze/>"><current_release_date_squeeze></a>.
<ifneq "6.0.0" "<current_release>"
  "Debian 6.0.0 was initially released on <:=spokendate('2011-02-06'):>."
/>
The release included many major
changes, described in 
our <a href="$(HOME)/News/2011/20110205a">press release</a> and 
the <a href="releasenotes">Release Notes</a>.</p>

<p><strong>Debian 6 has been superseded by
<a href="../wheezy/">Debian 7 (<q>wheezy</q>)</a>.
Security updates have been discontinued as of <:=spokendate('2014-05-31'):>.
</strong></p>

<p><strong>Squeeze had benefited from Long Term Support (LTS) until
the end of February 2016. The LTS was limited to i386 and amd64. For more information,
please refer to the <a
href="https://wiki.debian.org/LTS">LTS section of Debian Wiki</a>.
</strong></p>

<p>To obtain and install Debian, see
the installation information page and the
Installation Guide. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

<p>The following computer architectures are supported in this release:</p>

<ul>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/sparc/">SPARC</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/kfreebsd-amd64/">kFreeBSD 64-bit PC (amd64)</a>
<li><a href="../../ports/kfreebsd-i386/">kFreeBSD 32-bit PC (i386)</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
report other issues to us.</p>
