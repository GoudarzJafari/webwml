#use wml::debian::template title="How to join Debian" MAINPAGE="true"
#use wml::debian::recent_list

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> We are always looking for new contributors and volunteers. We strongly encourage <a href="$(HOME)/intro/diversity">participation by everyone</a>. Requirements: interest in Free Software and some free time.</p>
</aside>

<ul class="toc">
<li><a href="#reading">Reading</a></li>
<li><a href="#contributing">Contributing</a></li>
<li><a href="#joining">Joining</a></li>
</ul>


<h2><a id="reading">Reading</a></h2>

<p>
If you haven't already, you should read the sites linked on the <a
href="$(HOME)">Debian start page</a>. This will help you to get a better
understanding of who we are and what we're trying to accomplish. As a
potential Debian contributor, please pay particular attention to these
two pages:
</p>

<ul>
  <li><a href="$(HOME)/social_contract#guidelines">Debian Free Software Guidelines</a></li>
  <li><a href="$(HOME)/social_contract">Debian Social Contract</a></li>
</ul>

<p>
A lot of our communication happens on the Debian <a
href="$(HOME)/MailingLists/">mailing lists</a>. If
you want to get a feeling for the inner workings of
the Debian project, you should at least subscribe to <a
href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce</a>
and <a href="https://lists.debian.org/debian-news/">debian-news</a>. Both
lists are low-volume and document what's going on in the community. The
<a href="https://www.debian.org/News/weekly/">Debian Project News</a>,
also published on the debian-news list, summarizes recent discussions
from Debian-related mailing lists and blogs and provides links to them.
</p>

<p>
As a prospective developer, you should also subscribe to <a
href="https://lists.debian.org/debian-mentors/">debian-mentors</a>. Here
you can ask questions about packaging and infrastructure projects as
well as other developer-related issues. Please note that this list is
meant for new contributors, not users. Other interesting lists are <a
href="https://lists.debian.org/debian-devel/">debian-devel</a>
(technical development topics), <a
href="https://lists.debian.org/debian-project/">debian-project</a>
(discussions about non-technical issues in the project), <a
href="https://lists.debian.org/debian-release/">debian-release</a>
(coordinating Debian releases), <a
href="https://lists.debian.org/debian-qa/">debian-qa</a> (quality
assurance).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> <a href="$(HOME)/MailingLists/subscribe">Mailing List Subscription</a></button></p>

<p>
<strong>Tip:</strong> If you want to reduce the number of mails you
receive, especially on high-traffic lists, we offer email digests as a
summary email instead of individual messages. You can also visit the
<a href="https://lists.debian.org/">Mailing List Archives</a> to read
the messages of our lists in a web browser.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> You don't need to be an official Debian Developer (DD) to contribute. Instead, an existing DD can act as <a href="newmaint#Sponsor">sponsor</a> and help to integrate your work into the project.</p>
</aside>

<h2><a id="contributing">Contributing</a></h2>

<p>
Are you interested in maintaining packages? Then please have a look at
our <a href="$(DEVEL)/wnpp/">Work-Needing and Prospective Packages</a>
list. Here you'll find packages in need of a (new) maintainer. Taking
over an abandoned package is a great way to get started as a Debian
maintainer. Not only does it help our distribution, it also gives you
an opportunity to learn from the previous maintainer.
</p>

<p>
Here are some other ideas how you can contribute to Debian:
</p>

<ul>
  <li>Help us to write our <a href="$(HOME)/doc/">documentation</a>.</li>
  <li>Help to maintain the <a href="$(HOME)/devel/website/">Debian website</a>, produce content, edit or translate existing text. </li>
  <li>Join our <a href="$(HOME)/international/">translation team</a>.</li>
  <li>Improve Debian and join the <a href="https://qa.debian.org/">Debian Quality Assurance (QA) Team</a>.</li>
</ul>

<p>
Of course, there are plenty of other things you could do and we're
always looking for people to offer legal support or join our <a
href="https://wiki.debian.org/Teams/Publicity">Debian Publicity Team</a>.
Becoming a member of a Debian team is an excellent way to gain some
experience, before starting the <a href="newmaint">New Member</a> process.
It's also a good starting point if you're looking for a package
sponsor. So, find a team and jump right in!
</p>

<p style="text-align:center"><button type="button"><span class="fa fa-users fa-2x"></span> <a href="https://wiki.debian.org/Teams">List of Debian Teams</a></button></p>


<h2><a id="joining">Joining</a></h2>

<p>
So, you've contributed to the Debian project for some time and you want
to join Debian in a more official role? There are basically two options:
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> The Debian Maintainer (DM) role was introduced in 2007. Until then, the only official role was Debian Developer (DD). There are currently two independent processes for applying for either role.</p>
</aside>

<ul>
  <li><strong>Debian Maintainer (DM):</strong> This is the first step
  – as DM you can upload your own packages to the Debian archive
  (with some restrictions). Unlike Sponsored Maintainers, Debian
  Maintainers can maintain packages without a sponsor. <br>More information:
  <a href="https://wiki.debian.org/DebianMaintainer">Debian Maintainer
  Wiki</a></li>
  <li><strong>Debian Developer (DD):</strong> This is the traditional
  full membership role in Debian. A DD can participate in Debian
  elections. Uploading Debian Developers can upload any package to
  the archive. Before you become an Uploading DD, you should have
  a track record of maintaining packages for at least six months
  (for example, uploading packages as a DM, working inside a team or
  maintaining packages uploaded by sponsors). Non-uploading DDs have
  the same packaging rights as Debian Maintainers. Before you apply as a
  non-uploading DD, you should have a visible and significant track record
  of working inside the project. <br>More information: <a href="newmaint">New
  Members Corner</a></li>
</ul>

<p>
Regardless of the role you choose to apply for, you should be familiar
with Debian's procedures. This is why we strongly recommend reading
the <a href="$(DOC)/debian-policy/">Debian Policy</a> as well as the <a
href="$(DOC)/developers-reference/">Developer's Reference</a>.
</p>
