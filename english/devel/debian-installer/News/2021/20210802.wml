<define-tag pagetitle>Debian Installer Bullseye RC 3 release</define-tag>
<define-tag release_date>2021-08-02</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the third release candidate of the installer for Debian 11
<q>Bullseye</q>.
</p>

<p>
Let's start with some news regarding firmware support
(<a href="https://bugs.debian.org/989863">#989863</a>):
</p>

<ul>
  <li>We don't want to leave users in the dark if the installed system
    doesn't boot correctly (e.g. black screen, garbled
    display). We
    <a href="https://www.debian.org/releases/bullseye/amd64/ch02s02">mentioned that possibility</a>
    in the installation guide, and
    <a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">listed a few workarounds</a>
    that might help log in anyway.</li>
  <li>We also
    <a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">documented an isenkram-based procedure</a>
    which lets users detect and fix missing firmware on their systems,
    in an automated fashion. Of course, one has to weigh the pros and
    cons of using that tool since it's very likely that it will need
    to install non-free packages.</li>
</ul>

<p>
  In addition to those documentation efforts, the
  <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">non-free installer images that include firmware packages</a>
  have been improved so that they can anticipate the need for firmware
  in the installed system (e.g. firmware for AMD or Nvidia graphics
  cards, or newer generations of Intel audio hardware).
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>cdrom-detect:
    <ul>
      <li>Tell hw-detect not to warn and prompt about missing
        firmware: finding and mounting the ISO image happens so early
        that nothing could be done about missing firmware at this
        stage anyway (<a href="https://bugs.debian.org/991587">#991587</a>).</li>
    </ul>
  </li>
  <li>hw-detect:
    <ul>
      <li>Add support for installing additional firmware packages if
        items in the udev database match some modalias-based patterns,
        based on DEP-11 metadata (<a href="https://bugs.debian.org/989863">#989863</a>).</li>
      <li>Make sure to reload the appropriate kernel module after
        having injected some firmware: there might be some discrepancy
        as can be seen with <code>rtw_8821ce</code> vs. <code>rtw88_8821ce</code>
        (<a href="https://bugs.debian.org/973733">#973733</a>).</li>
      <li>Improve firmware-related logging by removing obsolete code,
        removing duplicates, adjusting format strings.</li>
      <li>Add support for ignoring some particular firmware files that
        might be neither actually required, nor packaged; start with
        <code>iwl-debug-yoyo.bin</code>, requested by <code>iwlwifi</code> (<a href="https://bugs.debian.org/969264">#969264</a>,
        <a href="https://bugs.debian.org/966218">#966218</a>).</li>
      <li>Make it possible for callers to disable warning and
        prompting about missing firmware.</li>
    </ul>
  </li>
  <li>debian-cd:
    <ul>
      <li>Add brltty and espeakup to all images from netinst up
        (<a href="https://bugs.debian.org/678065">#678065</a>).</li>
      <li>Generate firmware metadata from AppStream/DEP-11 metadata,
        and use it when building a media tree (<a href="https://bugs.debian.org/989863">#989863</a>).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Bump Linux kernel ABI to 5.10.0-8.</li>
    </ul>
  </li>
  <li>pcmciautils:
    <ul>
      <li>Update <code>/etc/pcmciautils/</code> dir to <code>/etc/pcmcia/</code> in udeb
        (<a href="https://bugs.debian.org/980271">#980271</a>).</li>
    </ul>
  </li>
  <li>udpkg:
    <ul>
      <li>Add locking for the status file, avoiding failures on the
        first installation step when multiple consoles are involved
        (<a href="https://bugs.debian.org/987368">#987368</a>).</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>


<ul>
  <li>linux:
    <ul>
      <li>arm64: Add pwm-rockchip to fb-modules udeb.</li>
      <li>arm64: Add fusb302, tcpm and typec to usb-modules udeb.</li>
      <li>armhf: Fix network detection on various i.MX6 boards
        (<a href="https://bugs.debian.org/982270">#982270</a>).</li>
      <li>armhf: Add mdio-aspeed to nic-modules (<a href="https://bugs.debian.org/991262">#991262</a>).</li>
      <li>s390x: Fix console name to match device (<a href="https://bugs.debian.org/961056">#961056</a>).</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 36 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
