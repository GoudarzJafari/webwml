#use wml::debian::template title="Platform for Sruthi Chandran" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

		<div id="content">
			<h1>DPL platform 2024</h1>(This an updated version of my 2021 platform)
			<h2>Sruthi Chandran</h2>
			<a href="mailto:srud@d.o">srud@d.o</a>
			<h2>Who am I?</h2>
			<p>
				I am a librarian turned Free Software enthusiast and Debian Developer
				from India.
			</p>
			<p>Some of my activities in Debian are:</p>
			<ul>
				<li>
					ruby, javascript, golang and font packages (totalling to around
					200) since 2016. (not very active nowadays)
				</li>
				<li>
					Community Team delegate since Aug 2020.
				</li>
				<li>
					Application Manager since Jul 2020.
				</li>
				<li>
					Outreach team since Aug/Sep 2020.
				</li>
				<li>
					Chief orga DebConf India 2023.
				</li>
				<li>
					DebConf Committee member since 2023.
				</li>
			</ul>
			<p>
				I have been mentoring people to contribute to Debian for many years
				now. I am involved in organizing numerous packaging workshops and
				other Debian events throughout India. I am also involved in
				organizing other free software events like
				<a href="https://camp.fsf.org.in/">Free Software Camp</a>.
			</p>
			<h2>Why am I contesting?</h2>
			<p>
				Concerned with the skewed gender ratios within the Free Software
				community (and Debian) I am working towards doing whatever I can
				to better the situation. How many times did we have a non-(cis)male
				candidate for DPL? If I am not wrong, when I contested in 2020, I was
				the second non-(cis)male DPL candidate till date. As with my last two
				rounds of contesting for DPL (2020 & 2021), this year too my primary
				goal of contesting is to bring the diversity issues for discussion.
				I believe I was successful in that at-least to a small extend in my
				previous rounds.
			</p>
			<p>
				I am aware that Debian is doing things to increase diversity within
				Debian, but as we can see, it is not sufficient. I am sad that there
				are only two women Debian Developer from a large country like India.
				I believe diversity is not something to be discussed only within
				Debian-women or Debian-diversity. It should come up for discussion
				in each and every aspect of the project.
			</p>
			<p>
				DPL elections is an important activity in our project and I plan to
				bring up the topic of diversity for discussion every election till
				we have a non-(cis)male DPL.
			</p>
			<p>
				I feel that one effective way to encourage diverse people to
				contribute is to have more visibility for diversity already within
				the community. As more and more women (both cis and trans), trans
				men, and genderqueer people who are already part of the project become
				more visible instead of staying hidden in some part of the project,
				more gender diverse people will feel comfortable joining our
				community. Geographic/ethnic diversity are also important areas
				which need attention.
			</p>
			<h2>What are my plans as DPL</h2>
			<h3>1. Diversity</h3>
			<p>
				Since diversity is my primary focus, let me start with that.
			</p>
			<h4><b>Diversity budget</b></h4>
			<p>
				In-spite of spending a good amount of money on diversity, we seem to
				not get the expected outcome. My first task as DPL would be to
				revisit the existing spending pattern to analyse why and where we
				are going wrong. When I say we should focus on diversity, I do not
				mean that we should blindly spend money in the name of diversity.
				I am personally aware of instances where the diversity budget
				expenditure did bad instead of any good.
			</p>
			<p>
				Another thing I would focus with respect to the diversity budget is
				to publicise it more so that people who deserve it get to know about
				it so that we do not end up spending it just for the sake of spending.
			</p>
			<h4><b>Diversity activities</b></h4>
			<p>
				I have heard that Debian-women project did some incredible work in
				improving women contributors till it went dormant. Now we have the
				Debian-diversity initiative, but this hasn’t gained much momentum
				yet. I plan to streamline their activities and if possible have a
				delegated team focusing on diversity. The delegated team shall
				co-ordinate all the diversity related activities in Debian. This
				team will also be involved in the decisions and expenditure of the
				diversity budget.
			</p>
			<h4><b>Local activities</b></h4>
			<p>
				Debian-localgroups which was conceptualised by our current DPL is a
				great idea to promote geographical diversity. I plan to use the
				potential of localgroups to organize more local events and have more
				diverse people contribute to Debian.
			</p>
			<h3>2. Outreach</h3>
			<p>
				My second focus area is outreach activities. I have been working with
				the Debian-outreach team for more than 3 years now. I feel outreach
				is one important activity which often does not get its due importance.
				Our outreach team end up being just co-ordinators for GSoC and Outreachy.
				I have a couple of ideas planned out for outreach.
			</p>
			<h4><b>Debian camp</b></h4>
			<p>
				Debian camp is a concept that I borrowed from
				<a href="https://camp.fsf.org.in/">Free Software Camp</a> for which I was
				one of the core organizers. Basically the concept is to have 3-4
				months long online mentorship program. The advantage of a Debian
				specific mentorship program is that we can tailor the program according
				to our specific needs.
			</p>
			<p>
				There will be two parts for the program. In the first part, the primary
				focus will be on the philosophy of Debian (and free software) and why
				one should contribute to Debian etc. We can have common sessions on
				these topics for the mentees. In the second part, the mentees will work
				with mentors on the projects of their choice.
			</p>
			<p>
				In this activity, I plan to have the Debian-outreach team as the
				co-ordinators/facilitators who work in collaboration with
				Debian-diversity, Debian-localgroups, Debian Academy etc. The
				Debian-outreach team will be doing the back-end activities of
				organizing, scheduling etc. while other teams will be helping out with
				resource persons/mentors/projects etc. I specifically intend to have
				Debian-localgroups involved so that we can have localised activities
				and not just in English.
			</p>
			<p>
				The question of whether to pay stipend or not could be discussed later
				and decided (we did not have stipend for the Free Software Camp and
				still it was a success).
			</p>
			<h4><b>DebConf Boot-camp</b></h4>
			<p>
				This is an in person camp which can happen during the DebCamp (this
				is an idea I borrowed from mollydb). I believe that there is nothing
				like in-person mentoring.
			</p>
			<p>
				Often we have new people who attend the DebConf are confused on what
				and how to start contributing. This bootstrapping activity can help
				them to find directions. I do believe that we have a lot of people
				interested to help out with this. Exact plan to have this worked out
				can be discussed and decided later.
			</p>
			<h4><b>Review of current activities</b></h4>
			<p>
				A thorough review of current outreach activities is long overdue. We
				need to examine the effectiveness of our GSoC and Outreachy
				participations, what we can do better and what we can do new.
			</p>
			<h3>3. Community</h3>
			<p>
				Debian community is a great community. But that does not mean we are
				perfect. Some points I would be interested to take up as DPL are:
			</p>
			<h4><b>Welcoming</b></h4>
			<p>
				Even when we say we are a welcoming community, for an outsider it
				might not appear so. There are a lot of reasons for this. The main
				reason being that online communication often leads to miscommunication
				and unintended flame wars. As a community, we need to work towards
				making Debian more welcoming to all – new comers and old timers alike.
				I am very keen on facilitating discussions on this as a DPL.
			</p>
			<h4><b>Change embracing</b></h4>
			<p>
				It is often observed that Debian as a community is reluctant to change
				most of the time. As a DPL, I would like to develop a culture of
				embracing change. I would encourage and facilitate people proposing
				new ideas and processes for improvement of Debian.
			</p>
			<h3>4. 	Debian as an organisation</h3>
			<h4><b>Debian Foundation (?)</b></h4>
			<p>
				In some of the previous years’ DPL campaigns, having Debian as a
				registered organisation has come up. In 2020, Brian Gupta’s platform
				focused on forming Debian Foundation and in 2022, Jonathan Carter
				mentioned about formal registration of Debian as an organisation.
				While organising DebConf23, I had to face some issues because Debian
				is not a registered organisation, That is when I started thinking
				about this concept seriously.
			</p>
			<p>
				So, as a DPL, I would be definitely interested in exploring the
				possibilities, advantages and disadvantages of having Debian
				registered. I am not saying that this is my main agenda, but it will
				definitely be brought up if I am elected.
			</p>
			<h4><b>Trusted organisations</b></h4>
			<p>
				As one of the main responsibilities of DPL is financial management,
				I would like to revisit our relationship with the existing trusted
				organisations, fund management procedure and if needed, explore the
				possibilities of having more TOs to reduce dependency on one or two.
				During DebConf23 organising, we had to face numerous fund distribution
				issues. Some of it definitely was specific to Indian scenario, but still
				I could think of a lot of improvement that can be done with respect to
				fund distribution through TOs.
			</p>
			<h3>5. Financial</h3>
			<h4><b>Accounting</b></h4>
			<p>
				From previous DPLs, I have heard that the current process of accounting
				of Debian assets is very complicated. As DPL, I would like to put some
				focus on streamlining and automating the accounting process across
				different TOs. I am thinking about having some process set up by which
				we can have a transparent and upto date asset information available.
			</p>
			<h3>6. Miscellaneous</h3>
			<h4><b> DPL helpers</b> (DPL committee?)</h4>
			<p>
				I have heard from the previous DPLs that the workload is too huge to be
				completed by a single person. During my last round of contesting,
				someone suggested the concept of having helpers for DPL. I agree with
				the thought process and I intend to have 1-2 people to do some work that
				can be offloaded from the DPL workload. This would help in reducing
				revert time and increasing efficiency.
			</p>
			<h2>Time commitment</h2>
			<p>
				One advantage I have over most of the previous and present DPL
				candidates is the amount of time I will be able to commit for my DPL
				activities. My work setup is such that I get to decide when and how
				much time I work. This means that I can give good priority to the DPL
				activities.
			</p>
			<div class="clr"></div>
		</div>
