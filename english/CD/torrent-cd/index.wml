#use wml::debian::cdimage title="Downloading Debian USB/CD/DVD images with BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<p><a href="https://en.wikipedia.org/wiki/BitTorrent">BitTorrent</a>
is a peer to peer download system optimised for large numbers of
downloaders. It puts minimal load on our servers because BitTorrent clients
upload pieces of files to others while downloading, thus spreading the load
across the network and making blazing fast downloads possible.
</p>
<div class="tip">
<p>The <strong>first</strong> USB/CD/DVD disk contains all the files necessary
to install a standard Debian system.<br />
</p>
</div>
<p>
You will need a BitTorrent client to download Debian USB/CD/DVD images
this way. The Debian distribution includes
<a href="https://packages.debian.org/aria2">aria2</a>,
<a href="https://packages.debian.org/transmission">transmission</a> or
<a href="https://packages.debian.org/ktorrent">KTorrent</a> tools.
Other operating systems (like Windows and macOS) are supported by <a
href="https://www.qbittorrent.org/download">qBittorrent</a> and <a
href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>
<h3>Official torrents for the <q>stable</q> release</h3>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>Be sure to have a look at the documentation before you install.
<strong>If you read only one document</strong> before installing, read our
<a href="$(HOME)/releases/stable/amd64/apa">Installation Howto</a>, a quick
walkthrough of the installation process. Other useful documentation includes:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installation Guide</a>,
    the detailed installation instructions</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer
    Documentation</a>, including the FAQ with common questions and answers</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Errata</a>, the list of known problems in the installer</li>
</ul>

# <h3>Official torrents for the <q>testing</q> distribution</h3>
# 
# <ul>
# 
#   <li><strong>CD</strong>:<br />
#   <full-cd-torrent>
#   </li>
# 
#   <li><strong>DVD</strong>:<br />
#   <full-dvd-torrent>
#   </li>
# 
# </ul>

<p>
If you can, please leave your client running after your download is complete,
to help others download images faster!
</p>
