#use wml::debian::template title="Comment pouvez-vous aider Debian&nbsp;?"
#use wml::debian::translation-check translation="3a5c68f115956c48aa60c6aa793eb4d802665b23" maintainer="Jean-Paul Guillonneau"

# Translators:
# Frédéric Bothamy, 2007.
# Simon Paillard, 2010.
# David Prévot, 2012-2014.
# Jean-Paul Guillonneau, 2017-2021
<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#coding">Codage et entretien de paquets</a></li>
    <li><a href="#testing">Test et suppression de bogues</a></li>
    <li><a href="#documenting">Écriture de documentation et étiquetage de paquets</a></li>
    <li><a href="#translating">Traduction et régionalisation</a></li>
    <li><a href="#usersupport">Assistance aux autres utilisateurs</a></li>
    <li><a href="#events">Organisation d’évènements</a></li>
    <li><a href="#donations">Don d’argent, de matériel ou de bande passante</a></li>
    <li><a href="#usedebian">Utilisation de Debian</a></li>
    <li><a href="#organizations">Comment une organisation peut aider</a></li>
  </ul>
</div>

<p>Debian n’est pas simplement un système d’exploitation, c’est une communauté.
Beaucoup de personnes avec des compétences très différentes contribuent au
projet : nos logiciels, les illustrations, le wiki et les autres documentations
sont le résultat d’un effort conjugué d’un grand groupe d'individus. Tout
le monde n’est pas développeur et il n'est nul besoin de savoir coder pour
participer. Il est possible d’aider de nombreuses façons pour améliorer Debian.
Si vous voulez nous rejoindre, voici quelques suggestions pour les utilisateurs
débutants et expérimentés.</p>

<h2><a id="coding">Codage et entretien de paquets</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>Vous voulez peut-être écrire une toute nouvelle application ou implémenter
une nouvelle fonction dans un programme existant. Si vous êtes développeur et
voulez contribuer à Debian, vous pouvez aider à préparer les logiciels
pour une installation facile dans Debian, nous appelons cela l’empaquetage.
Voici une liste de quelques idées sur la manière de débuter :</p>

<ul>
  <li>Empaqueter des applications, par exemple pour ceux qui ont une expérience
avec Debian ou qui s’estiment compétents. Pour plus d’information sur la manière
de dévenir responsable de paquet, visitez
<a href="$(HOME)/devel/">le coin du développeur Debian</a> ;</li>
  <li>Aider à entretenir les applications existantes, par exemple en contribuant
aux correctifs  ou en apportant des informations dans le
<a href="https://bugs.debian.org/">système de suivi des bogues de Debian</a>.
Sinon vous pouvez rejoindre une équipe de responsables ou intégrer un projet
logiciel sur <a href="https://salsa.debian.org/">Salsa</a> (notre instance de
GitLab) ;</li>
  <li>Nous aider en
<a href="https://security-tracker.debian.org/tracker/data/report">rapportant</a> et
<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">corrigeant des</a>
<a href="$(HOME)/security/">problèmes de sécurité</a> dans Debian ;</li>
  <li>Vous pouvez aussi aider en améliorant les
<a href="https://wiki.debian.org/Hardening">paquets</a>, les
<a href="https://wiki.debian.org/Hardening/RepoAndImages">dépôts et images</a> et les
<a href="https://wiki.debian.org/Hardening/Goals">autres composants</a> ;</li>
  <li>Intéressé par le <a href="$(HOME)/ports/">portage</a> de Debian dans une
architecture dont vous êtes familier ? Vous pouvez commencer un nouveau portage
ou contribuer à un portage existant ;</li>
  <li>Aider à améliorer les <a href="https://wiki.debian.org/Services">services</a>
relatifs à Debian ou en créer et entretenir de nouveaux
<a href="https://wiki.debian.org/Services#wishlist">suggérés ou demandés</a>
par la communauté.</li>
</ul>

<h2><a id="testing">Test et suppression de bogues</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>Comme tout autre projet logiciel, Debian a besoin des utilisateurs pour
tester le système d’exploitation et ses applications. Une manière de contribuer
est d’installer la dernière version et de rendre compte aux développeurs si
quelque chose ne fonctionne pas comme prévu. Nous avons aussi besoin
de personnes pour tester notre support d’installation, l’amorçage sécurisé et
le chargeur de démarrage U-Boot sur différents matériels :
</p>

<ul>
  <li>Vous pouvez utiliser notre <a href="https://bugs.debian.org/">système de
suivi des bogues Debian</a> pour rapporter n’importe quel problème trouvé dans
Debian. Assurez-vous d'abord que cela n’a pas déjà été fait ;</li>
  <li>Visitez le système de suivi et essayez de parcourir les bogues concernant
les paquets que vous utilisez. Peut-être que vous pouvez apporter des
informations complémentaires et reproduire les problèmes rencontrés ;</li>
  <li>Tester l’<a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">\
installateur et les images autonomes ISO</a>, la
<a href="https://wiki.debian.org/SecureBoot/Testing">prise en charge de\
l’amorçage sécurisé</a>, les
<a href="https://wiki.debian.org/LTS/TestSuites">mises à jour de LTS</a> et le
chargeur de démarrage <a href="https://wiki.debian.org/U-boot/Status">U-Boot</a>.</li>
</ul>

<h2><a id="documenting">Écriture de documentation et étiquetage de paquets</a></h2>

<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>Si vous avez rencontré un quelconque problème avec Debian et que vous ne
pouvez pas écrire du code pour le résoudre, peut être que la prise de notes et
la retranscription de votre solution est une option pour vous. De cette façon
vous pouvez aider d’autres utilisateurs ayant un problème similaire. Toute
la documentation Debian est écrite par des membres de la communauté et vous
pouvez aider de plusieurs façons :</p>

<ul>
  <li>Rejoindre le <a href="$(HOME)/doc/ddp">Projet de documentation Debian</a>
pour aider à la documentation officielle de Debian ;</li>
  <li>Contribuer au <a href="https://wiki.debian.org/">Wiki de Debian</a> ;</li>
  <li>Étiqueter et catégoriser les paquets sur le site web
<a href="https://debtags.debian.org/">Debtags</a>, de façon à ce que les utilisateurs
puissent trouver aisément le logiciel qu’ils recherchent.</li>
</ul>

<h2><a id="translating">Traduction et régionalisation</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>
Votre langue naturelle n’est pas l’anglais, mais vous avez assez de compétences
dans la langue anglaise pour comprendre et traduire des logiciels ou des
informations concernant Debian telles que les pages web, la documentation !
Pourquoi ne pas rejoindre l’équipe de traduction et traduire les applications
dans votre langue maternelle. Nous recherchons aussi des gens pour vérifier
l’existence de traductions et rapporter des bogues si nécessaire :
</p>

# Translators, link directly to your group's pages
<ul>
  <li>Toute chose se rapportant à l’internationalisation de Debian est
discutée sur la <a href="https://lists.debian.org/debian-i18n/">liste de
diffusion debian-i18n</a> ;</li>
  <li>Vous êtes un locuteur naturel d’une langue non prise en charge par
Debian ! Prenez contact à l’aide de la page
<a href="$(HOME)/international/">Debian International</a>.</li>
</ul>

<h2><a id="usersupport">Assistance aux autres utilisateurs</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>Vous pouvez aussi contribuer au projet en aidant d’autres utilisateurs. Le
projet utilise divers moyens pour gérer cela, par exemple, les listes de
diffusion dans différentes langues et les canaux IRC. Pour plus d’informations,
visitez nos pages d’<a href="$(HOME)/support">assistance</a> :</p>

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<ul>
  <li>Le projet Debian utilise beaucoup de
<a href="https://lists.debian.org/">listes de diffusion</a>. Certaines
s’adressent aux développeurs et d’autres aux utilisateurs. Les utilisateurs
expérimentés peuvent aider les autres à travers les
 <a href="$(HOME)/support#mail_lists">listes de diffusion pour les
utilisateurs</a> ;</li>
  <li>Des gens de partout clavardent en temps réel sur IRC (Internet Relay
Chat). Visitez le canal <tt>#debian</tt> ou <tt>#debian-fr</tt> sur
<a href="https://www.oftc.net/">OFTC</a> pour communiquer avec d’autres
utilisateurs.</li>
</ul>

<h2><a id="events">Organisation d’évènements</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>Mis à part la conférence annuelle de Debian (DebConf), de plus petites
assemblées et rencontres réelles ont lieu dans plusieurs pays chaque année.
Y prendre part ou aider à organiser un <a href="$(HOME)/events/">évènement</a>
est une grande opportunité de rencontrer des développeurs et d’autres
utilisateurs de Debian :</p>

<ul>
  <li>Donner un coup de main lors des <a href="https://debconf.org/">conférences
Debian</a> annuelles, par exemple en enregistrant des
<a href="https://video.debconf.org/">vidéos</a> de débats et présentations,
accueillir les participants et aider les orateurs, organiser des évènements
spéciaux pendant la DebConf (tel le buffet convivial), aider à l’installation
 et au démontage, etc. ;</li>
  <li>De plus, il existe plusieurs conférences
<a href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a>, des rencontres
locales organisées par des membres du projet ;</li>
  <li>Vous pouvez aussi créer ou rejoindre un
<a href="https://wiki.debian.org/LocalGroups">groupe Debian local</a> avec
des rencontres régulières ou d’autres activités ;</li>
  <li>Aussi, renseignez-vous sur les autres évènements tels que
<a href="https://wiki.debian.org/DebianDay">les anniversaires de Debian</a>, les
<a href="https://wiki.debian.org/ReleaseParty">fêtes de publication</a>,
les <a href="https://wiki.debian.org/BSP">chasses aux bogues</a>, les
<a href="https://wiki.debian.org/Sprints">rencontres informelles de
développement</a>, ou <a href="https://wiki.debian.org/DebianEvents">d’autres
évènements</a> autour du globe.</li>
</ul>

<h2><a id="donations">Don d’argent, de matériel ou de bande passante</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>Tous les dons au projet Debian sont gérés par le chef du projet Debian
(Debian Project Leader — DPL). Avec votre aide nous pouvons acheter du matériel,
des domaines, des certificats de chiffrement, etc. Nous utilisons aussi des
fonds pour parrainer les DebConf et MiniDebConf, les rencontres de
développement, une présence à d’autres évènements et d’autres choses :</p>

<ul>
  <li>Vous pouvez <a href="$(HOME)/donations">donner</a> le l’argent, des
équipements ou fournir des services au projet Debian ;</li>
  <li>Nous sommes constamment à la recherche de
<a href="$(HOME)/mirror/">miroirs</a> dans tous les pays du monde ;</li>
  <li>Pour nos portages Debian, nous nous appuyons sur notre
<a href="$(HOME)/devel/buildd/">réseau de serveurs d'empaquetage
automatique</a>.</li>
</ul>

<h2><a id="usedebian">Utilisation de Debian et promotion</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>Parcourez le monde et parlez de Debian et de sa communauté. Recommandez
son système d’exploitation aux utilisateurs et montrez comment l’installer.
Utilisez tout simplement Debian et prenez-y du plaisir, c’est probablement la
meilleure façon de donner un retour au projet Debian :</p>

<ul>
  <li>Aidez à promouvoir Debian par des conférences et faites-en la
démonstration à d’autres utilisateurs ;</li>
  <li>Contribuez à notre
<a href="https://www.debian.org/devel/website/">site web</a> et aidez-nous
à améliorer la notoriété de Debian ;</li>
  <li>Faites des <a href="https://wiki.debian.org/ScreenShots">captures
d’écran</a> et <a href="https://screenshots.debian.net/upload">téléverser
</a>-les sur
<a href="https://screenshots.debian.net/">screenshots.debian.net</a> de façon
que nos utilisateurs puissent voir à quoi ressemble un logiciel dans Debian
avant de l’utiliser ;</li>
  <li>Vous pouvez autoriser les
 <a href="https://packages.debian.org/popularity-contest">soumissions
popularity-contest</a> de façon à ce que nous sachions quels paquets sont les
plus populaires et les plus utiles pour tout un chacun.</li>
</ul>

<h2><a id="organizations">Comment une organisation peut aider</a></h2>

<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>
Que vous soyez une organisation éducationnelle, commerciale, gouvernementale
ou à but non lucratif, il existe de nombreuses façons de nous aider avec vos moyens :
</p>

<ul>
  <li>Par exemple, votre organisation peut simplement
<a href="$(HOME)/donations">donner</a> de l’argent ou du matériel ;</li>
  <li>Peut-être aimeriez-vous
<a href="https://www.debconf.org/sponsors/">parrainer</a> nos conférences ;</li>
  <li>Votre organisation peut fournir
<a href="https://wiki.debian.org/MemberBenefits">des produits ou fournir des
services aux contributeurs de Debian</a> ;</li>
  <li>Nous recherchons aussi des
<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">\
hébergements gratuits</a> ;</li>
  <li>Naturellement, créer des miroirs pour nos
<a href="https://www.debian.org/mirror/ftpmirror">logiciels</a>, les
<a href="https://www.debian.org/CD/mirroring/">supports d’installation</a> ou
les <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">vidéos de
conférences</a> est aussi grandement apprécié ;</li>
  <li>Peut-être que vous pouvez envisager de vendre des
<a href="https://www.debian.org/events/merchandise">produits</a> Debian,
des <a href="https://www.debian.org/CD/vendors/">supports d’installation</a> ou
des <a href="https://www.debian.org/distrib/pre-installed">systèmes
pré-installés</a> ;</li>
  <li>Si votre organisation propose une
<a href="https://www.debian.org/consultants/">expertise</a> sur Debian ou un
<a href="https://wiki.debian.org/DebianHosting">hébergement</a>, faites-le
nous savoir.</li>
</ul>

<p>
Nous sommes aussi intéressés par des
<a href="https://www.debian.org/partners/">partenariats</a>. Si vous pouvez
promouvoir Debian en <a href="https://www.debian.org/users/">fournissant un
témoignage</a>, en l’utilisant sur vos serveurs ou vos bureaux ou même en
encourageant vos employés à participer à notre projet, ce serait fantastique !
Peut-être que vous pouvez envisager d’enseigner sur le système d’exploitation
de Debian et sa communauté, orientant votre équipe vers une contribution
durant les heures de travail ou en l’envoyant à un de nos
<a href="$(HOME)/events/">évènements</a>.</p>

