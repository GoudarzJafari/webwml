#use wml::debian::translation-check translation="fffc6ec789ce1ef078e186818578a488cc92c60c" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bookworm RC1</define-tag>
<define-tag release_date>2023-04-03</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la première version candidate de l’installateur pour Debian 12 <q>Bookworm</q>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>debian-cd :
  <ul>
    <li>utilisation de liens physiques à la place de liens symboliques pour
    les paquets de microprogrammes
    (<a href="https://bugs.debian.org/1031696">nº 1031696</a>) ;</li>
    <li>suppression totale de la prise en charge de loadlin.</li>
  </ul>
  </li>
  <li>debian-installer :
  <ul>
    <li>rafraîchissement de l'écran d'aide avec une taille de disque minimale
    pour l'installation et de meilleurs liens pour la documentation
    (<a href="https://bugs.debian.org/1033193">nº 1033193</a>).</li>
  </ul>
  </li>
  <li>debian-installer-utils :
  <ul>
    <li>inclusion de l'entrée xterm dans terminfo pour toutes les architectures,
     corrigeant des problèmes de nano dans l'installateur graphique
     (<a href="https://bugs.debian.org/1026027">nº 1026027</a>).</li>
  </ul>
  </li>
  <li>grub-installer :
  <ul>
    <li>heuristique de base pour la question de force-efi-extra-removable ;</li>
    <li>ajout d'une question pour l'exécution de grub-install avec --no-nvram,
    en utilisant une logique similaire.</li>
  </ul>
  </li>
  <li>hw-detect :
  <ul>
    <li>déduplication de la liste des fichiers de microprogrammes requis et
    pas seulement de la liste des modules requérants
    (<a href="https://bugs.debian.org/1031631">nº 1031631</a>) ;</li>
    <li>implémentation de la prise en charge du microcode quand /proc/cpuinfo
    contient un champ vendor_id avec une des valeurs suivantes
    (<a href="https://bugs.debian.org/1029804">nº 1029804</a>) :
    <ul>
      <li>installation d'amd64-microcode pour AuthenticAMD ;</li>
      <li>installation d'intel-microcode pour GenuineIntel ;</li>
      <li>activation de non-free-firmware en conséquence ;</li>
      <li>réalisation de l'installation au moyen de finish-install, assurance
      qu'apt-setup a été configuré et utilisation d'apt-install pour la
      résolution de dépendances.</li>
    </ul>
    </li>
    <li>optimisation de l'installation du paquet de microprogrammes :
    traitement des déclenchements de dpkg une seule fois après que tous les
    paquets ont été installés, menant à un appel unique d'update-initramfs ;</li>
    <li>correction d'une condition au sujet des appels mountmedia
    (<a href="https://bugs.debian.org/1032377">nº 1032377</a>) ;</li>
    <li>correction du retrait des paquets de microprogrammes refusés
    (<a href="https://bugs.debian.org/1032377">nº 1032377</a>) ;</li>
    <li>ajout d'un cas particulier pour le module mhi
    (<a href="https://bugs.debian.org/1032140">nº 1032140</a>), utilisant les
    informations des propriétaires (par exemple ath11k_pci et qrtr_mhi pour
    certaines cartes sans fil Atheros) ;</li>
    <li>correction de la gestion des estampilles temporelles de dmesg ;</li>
    <li>correction de l'extraction du nom de paquet lors du retrait d'un
    paquet de microprogrammes (par exemple, l'installation échoue parce qu'il
    était corrompu) ;</li>
    <li>construction de /var/log/firmware-summary sous la forme d'un résumé
    sur 3 colonnes des paquets de microprogrammes (et de microcodes) installés
    (<a href="https://bugs.debian.org/1029849">nº 1029849</a>) ;</li>
    <li>correction du retrait de fichiers temporaires dans /target après
    l'installation de paquets de microprogrammes
    (<a href="https://bugs.debian.org/1033035">nº 1033035</a>) ;</li>
    <li>détermination du nom de paquet en utilisant le champ Package plutôt
    que de faire confiance au nom de fichier lors de l'installation de paquets
    de microprogrammes (<a href="https://bugs.debian.org/1033035">nº 1033035</a>) ;</li>
    <li>assurance de ne pas inclure l'option potentielle -n lors de la
    configuration des variables IFACES dans check-missing-firmware
    (<a href="https://bugs.debian.org/1033035">nº 1033035</a>).</li>
  </ul>
  </li>
  <li>installation-report :
  <ul>
    <li>stockage des informations sur les microprogrammes dans
    /var/log/installer/firmware-summary
    (<a href="https://bugs.debian.org/1033325">nº 1033325</a>). Voir
    l'entrée 2.87 du journal des modifications pour des détails ;</li>
    <li>inclusion de firmware-summary dans les rapports d'installation.</li>
  </ul>
  </li>
  <li>localechooser :
  <ul>
    <li>retrait d'AN (Antilles néerlandaises) de la liste de pays ; déplacement
    de CW (Curaçao) vers la région Caraïbes.</li>
  </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
  <ul>
    <li>activation de sound-modules et de speakup sur les images gtk pour arm64.</li>
  </ul>
  </li>
  <li>linux :
  <ul>
    <li>correction du rechargement du module mt7921e après le déploiement des
    fichiers de microprogrammes
    (<a href="https://bugs.debian.org/1029116">nº 1029116</a>) ;</li>
    <li>ajout de michael_mic à crypto-modules pour ath11k et d'autres
    (<a href="https://bugs.debian.org/1032140">nº 1032140</a>) ;</li>
    <li>ajout de qrtr_mhi à nic-wireless pour ath11k
    (<a href="https://bugs.debian.org/1032140">nº 1032140</a>) ;</li>
    <li>ajout du pilote DRM ast à fb-modules sur ppc64el
    (<a href="https://bugs.debian.org/990016">nº 990016</a>) ;</li>
    <li>ajout des paquets udeb sound et speakup sur arm64 et armhf
    (<a href="https://bugs.debian.org/1031289">nº 1031289</a>).</li>
  </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 41 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres médias d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
