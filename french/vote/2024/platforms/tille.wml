#use wml::debian::template title="Programme d'Andreas Tille" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="b60b19fa0445955876c166b6c2a991f9609146e1" maintainer="Jean-Pierre Giraud"

<div class="main">
<table class="title">
<tr><td style="vertical-align: top;">
<h1 class="titlemain"><big><b>Andreas Tille</b></big><br>
    <small>Programme de DPL</small><br>
    <small>19-03-2024</small>
</h1>

<a href="mailto:tille@debian.org"><tt>tille@debian.org</tt></a><br />
<a href="https://people.debian.org/~tille/">Page de développeur Debian</a><br />
<a href="https://people.debian.org/~tille/talks/">Communications liées à Debian</a><br />
<a href="http://fam-tille.de"><tt>Page d'accueil privée</tt></a><br />

</td>
<td width=50%>&nbsp;</td>
<td style="vertical-align: bottom;">
<div style="min-height: 210px;">
<img style="float: left; padding-right: 10px;" src="20140827_2.jpg" alt="Image of Andreas Tille" width="140">
</div>
</td>
</tr>
</table>

<h2 class="section">1. Introduction</h2>

<p>Je m'appelle Andreas Tille. Il m'a fallu plus de vingt-cinq ans avec le
compte Debian <em>tille</em> pour me présenter au poste de DPL.</p>

<p>
Je suis marié et un grand-père fier (grâce à mon fils et à ma belle-fille).
Je suis aussi le père adoptif de deux filles (grâce à Debian) et une de ces
filles m'a rendu grand-père également. Ma formation de physicien a entretenu
chez moi un vif intérêt pour les applications pratiques des solutions des
technologies de l'information en science. Depuis toujours, j'ai une passion
pour divers sports et en particulier la natation. Préoccupé par notre
environnement et la crise climatique, je me suis consacré à planter de
nombreux arbres. Je suis un fervent cycliste et j'ai choisi de ne pas avoir
de voiture, priorisant les méthodes de transport durables.
</p>

<p>
Pour moi, la liberté, c'est entre autres choses, pouvoir ne pas être
disponible en permanence. C'est pour cela que j'ai choisi, par exemple de
ne pas posséder de smartphone. Et donc il est important que vous sachiez,
que, en tant que potentiel DPL, il y a des moments où je ne suis pas en
ligne et ne peux pas être joint. J'accorde beaucoup de valeur à la
liberté et je suis reconnaissant du privilège de faire des choix qui
correspondent à mes valeurs.
</p>

<h2 class="section">2. Pourquoi je suis candidat au poste de DPL</h2>

<h4>Réponse courte</h4>

<p>
Debian a tenu une part importante d'à peu près la moitié de ma vie.
Même si contribuer à des paquets a constitué la part principale de mon
implication, je ressens le besoin de restituer encore davantage à mes
amis et à la communauté.
</p>

<h4>Réponse longue</h4>
<p>
Après avoir initié le projet Debian Med, j'ai appris beaucoup sur la
gestion d'une équipe de bénévoles. Bien que j'ai souvent été considéré
comme le chef de file de Debian Med, j'ai généralement contesté le terme
de « chef » dans la mesure où je n'étais pas élu pour cette fonction (et
qu'il est vraiment inutile d'avoir un poste de chef dans une petite
équipe). Néanmoins, j'ai réellement rempli les tâches qui incombent à
un chef, comme établir un climat convivial entre les membres de l'équipe
et prendre soin d'assurer une saine croissance à l'équipe. J'ai appris
qu'il était plus difficile de conduire une équipe de bénévoles qu'une
équipe de salariés, et je considère que c'est un vrai défi de motiver
des bénévoles pour qu'ils apprécient de travailler pour l'équipe.
</p>
<p>
En évaluant mes capacités à construire une équipe efficace, j'attribue
une certaine valeur à deux qualités que je considère déterminantes pour
parvenir au succès : ma formation de physicien et mon expérience
d'athlète. Ces qualités personnelles ont joué un rôle significatif pour
forger ma capacité à piloter et collaborer efficacement, contribuant à
mes réussites dans la constitution et la gestion d'équipes. En tant que
physicien, je possède un sens aigu pour trouver des solutions logiques
et raisonnables. Ces compétences analytiques me permettent de prendre
des décisions éclairées et à relever des défis complexes de façon
efficace. Par ailleurs mon expérience de sportif m'a inculqué un sens
profond de la ténacité et de la détermination. Je m'engage à mettre en
application ces deux qualités en tant que DPL, exploitant mon esprit
analytique pour prendre les décisions stratégiques et ma persévérance
pour faire avancer des initiatives et surmonter les obstacles.
</p>
<p>
Comme vous pouvez le constater à la lecture de la <a
href="https://people.debian.org/~tille/index.html#statistics">\
section statistiques de ma page de développeur</a>, j'ai téléversé de
nombreux paquets et corrigé beaucoup de bogues. Mais Debian ne se limite
pas au versement de paquets. C'est aussi résoudre des problèmes
techniques et de socialité, venir en aide aux débutants et s'assurer
que nous restons pertinents dans l'univers des technologies de
l'information.
</p>
<p>
Pendant les DebConf en particulier, mais aussi à l'occasion d'autres
événements, j'ai rencontré beaucoup de gens très savants avec lesquels
j'aimerais travailler et auprès de qui j'aurais plaisir à prendre 
conseil pour les nouvelles tâches que je pourrais avoir à entreprendre.
</p>


<h4>Ce qui me fait peur dans cette candidature de DPL</h4>
<p>
J'aime Debian parce qu'elle me permet, comme bénévole, de choisir les
tâches que j'aime accomplir. Par chance, il y a eu tellement de tâches
difficiles que j'ai aimé faire au fils des années. J'ai travaillé la
plupart du temps dans l'ombre – même si la présentation de diverses
communications m'a de temps en temps mis sous le feu des projecteurs.
Être mis en avant n'est pas quelque chose qui me plaît vraiment, et
c'est une des raisons pour lesquelles j'ai résisté des années aux
sollicitations de mes amis dans Debian me suggérant de me présenter
comme DPL.
</p>
<p>
Je prévois aussi qu'il y a plusieurs tâches non techniques qui attendent
le DPL et qui peuvent être moins agréables que le travail que je réalise
actuellement dans Debian. Cependant, j'approche ces défis avec optimisme.
La perspective de constituer un comité consultatif auprès du DPL, auprès
de qui je pourrais trouver conseil et soutien lors du traitement de
questions difficiles, soulage l'appréhension que je peux ressentir à
propos de ces tâches.
</p>
<p>
Enfin, je suis un peu préoccupé par la charge de travail que je laisserai
à mes coéquipiers dans la mesure où je prévois d'interrompre mon travail
de téléversement pour me consacrer pleinement à mes tâches de DPL.
</p>


<h4>Bref résumé</h4>
<p>
Je ne suis pas sûr qu'il soit possible de lancer des modifications
significatives dans Debian en un seul mandat de DPL, bien que j'en
reconnaisse la nécessité sous différents aspects. J'ai l'intention
d'être réaliste et sincère dans mes engagements et d'éviter de faire des
promesses que je ne pourrai pas tenir. Je privilégie jeter des bases
solides permettant aux DPL à venir de mettre en œuvre réellement les
changements nécessaires. Je m'engage ici à mettre à profit l'expérience
que j'ai accumulée au bénéfice de ces futurs DPL.
</p>

<h2 class="section">3. Programme</h2>

<h3 class="subsection">3.1. S'assurer que Debian reste pertinente dans un écosystème de systèmes d'exploitation en mutation</h3>

<h4>Perception externe</h4>
<p>
Parfois je me demande si Debian n'est pas victime de son propre succès
qui lui vaut d'être la distribution avec le plus de dérivées. J'ai
rencontré maintes fois des nouveaux venus à Linux qui n'avaient jamais
entendu parler de Debian, mais qui savaient ce qu'était Ubuntu. Même si
dans une certaine mesure cela convient à ma personnalité exposée plus
haut, préférant rester dans l'ombre, si Debian était plus largement
connue des gens qui ne se considèrent pas comme des experts de Linux,
cela attirerait probablement plus de contributeurs.
</p>

<h4>Tendre la main pour apprendre</h4>
<p>
J'essaierai d'établir des contacts avec les autres distributions. En ce
qui concerne les dérivées, j'aimerais créer une sorte de liste des
souhaits de ce que nous, en tant que distribution amont, pourrions mieux
faire ou que nous pourrions potentiellement apprendre. J'envisage aussi
de parler avec les distributions qui reposent sur des bases techniques
différentes comme ArchLinux, Fedora, OpenSUSE, Nix, etc., pour voir ce
qu'elles peuvent nous apprendre pour résoudre des problèmes en termes
d'organisation du travail et d'infrastructure. Peut-être pourrons-nous
en tirer certaines conclusions, par exemple, sur pourquoi ArchWiki est
réputé pour sa bonne documentation, ce qui n'est pas le cas de
wiki.debian.org.
</p>

<h4>Se préparer pour le futur</h4>
<p>
Avec la publication de Trixie, nous sommes confrontés à la transition de
time_t 64 bits. Cela vise à assurer que les architectures 32 bits dans
Trixie et au-delà soient capables de gérer des horodatages se référant à
des dates postérieures à 2038. Dans la mesure où les autres
distributions ont décidé d'abandonner les architectures 32 bits, Debian
pourrait même être la distribution plus adéquate pour travailler
élégamment sur des applications matérielles particulières. C'est un
autre défi que nous avons à relever pour préparer Debian au futur.
</p>

<h4>Norme d'empaquetage</h4>
<p>
Pour maintenir la norme réputée d'excellence de Debian, je suis
déterminé à m'occuper des domaines où des améliorations peuvent être
entreprises. Même si la grande qualité de nos paquets est appréciée, il
y a une liste de « paquets qui sentent mauvais » (voir <a
href="https://trends.debian.net/">Debian Trends</a>). Mon but est de
réduire les obstacles pour la mise à jour ces paquets et nous assurer
qu'ils se conforment aux normes actuelles d'empaquetage. Cela inclut
encourager les contributeurs à entretenir des dépôts Git dans Salsa, ce
qui est la méthode par défaut pour entretenir un paquet, plutôt que dans
des espaces spécifiques à leur équipe.
</p>

<h4>Outreach</h4>
<p>
Je suis déterminé à investir mon énergie dans des projets d'ouverture
visant les plus jeunes développeurs pour assurer à Debian sa pertinence
future. En collaborant avec la nouvelle génération et en favorisant son
autonomie, nous pouvons encourager l’innovation et maintenir la position
de Debian comme force de premier plan dans l'écosystème du logiciel
libre. À travers des initiatives de mentorat et de formation, nous
pouvons inciter les nouveaux développeurs à contribuer, assurant pour
les années à venir l'impact durable de Debian.
</p>

<h3 class="subsection">3.2. Travail des équipes d'infrastructure et d'empaquetage</h3>

<h4>Équipes d'infrastructure</h4>
<p>
Debian dépend largement du travail des équipes d'infrastructure telles
que DSA, l'équipe de publication, l'équipe ftpmaster et d'autres encore.
Si je suis élu comme DPL, je collaborerai avec ces équipes pour
identifier et résoudre leurs problèmes. Dans le passé, j'ai proposé des
améliorations pour le travail de l'équipe ftpmaster. Je reconnais que
l'amélioration du processus d'intégration des nouveaux paquets est une
des raisons qui motivent ma candidature comme DPL.
</p>

<h4>Réduire le travail manuel</h4>
<p>
Je réfléchis aussi aux moyens pour rationaliser et automatiser les
tâches partout où cela est possible. Un exemple qui me vient à l'esprit
ce sont les processus pour retirer des paquets propres à certaines
architectures, tâches qui actuellement reposent sur une intervention
manuelle de l'équipe ftpmaster déclenchée par un rapport de bogue. Même
si j'apprécie la valeur d'un regard supplémentaire pour vérifier les
dépendances du passé, maintenant nous avons les autopkgtests qui
signalent les problèmes potentiels de chaînes de dépendances. Je crois
qu'implémenter un outil pratique peut réduire la charge de travail
manuel, accélérer les processus et fournir aux responsables de paquet
un meilleur contrôle.
</p>

<h4>Coopération</h4>
<p>
En plus de mon travail personnel d'empaquetage, j'ai toujours eu à
cœur de m'occuper soigneusement de mon équipe. Pour évaluer la
durabilité d'une équipe, j'ai établi quelques indicateurs sur les
équipes. Cela a montré que, dans Debian, les équipes sont très
diverses. Cela va d'équipes avec beaucoup de membres coopérant de
façon très étroite, des équipes nombreuses qui ne font que partager un
espace d'équipe commun et suivant une politique commune à des équipes 
qui sont moins qu'une poignée de participants actifs qui font tout le
travail. J'espère que je pourrai faire la promotion d'une culture
d'équipe coopérative avec beaucoup de contributeurs actifs, y compris
en créant un environnement convivial pour les nouveaux membres de
l'équipe. Dans l'équipe des Blends nous sommes parfois parvenus à
former et impliquer aussi bien des développeurs amont que des
utilisateurs des logiciels que nous empaquetons. C'est pour cela que
je suis très favorable à la promotion des Blends.
</p>

<h4>Culture d'équipe</h4>
<p>
J'ai acquis de solides connaissances dans plusieurs équipes de Debian.
Je suis un grand partisan des équipes dans la mesure où elles diminuent
les obstacles à la mise à jour et à la correction des paquets qui ne
sont plus « privés ». Cependant j'ai aussi pu observer des paquets qui
devenaient « orphelins d'équipe » quand le responsable original passait
à d'autres tâches sans le dire tout en comptant sur les autres membres
de l'équipe pour entretenir le travail original. Pour remédier à cela,
j'aimerais promouvoir une culture d'équipe plus solide pour suivre les
problèmes de chaque membre dans toute l'équipe.
</p>

<h4>Construire la redondance</h4>
<p>
J'imagine un futur où chaque tâche cruciale dans Debian – que ce soit
l'entretien des infrastructures ou la gestion de paquets principaux –
est gérée par au moins deux individus pour assurer une assistance et
une prise en charge complète. L'histoire nous a montré des exemples où
les contributeurs ont eu à prioriser leurs engagements personnels ou
des circonstances imprévues par rapport à leurs responsabilités dans
Debian, comme poursuivre des ultra-marathons ou composer avec les
exigences de la parentalité. Comme les bénévoles peuvent inévitablement
être amenés à abandonner leurs tâches dans Debian, il est essentiel que
nous établissions des mécanismes pour gérer efficacement ce type de
transitions. C'est pourquoi j'ai des réserves sur le modèle
« traditionnel » de paquet propriété d'un responsable unique.
</p>

<p>
Autrement dit, si vous pensez que la maintenance de paquets par une
personne seule est la bonne manière pour Debian d'affronter des
problèmes futurs vous pourrez sûrement me trouver sous l'option 
« Aucune de celles qui précèdent » lors du vote.
</p>

<h4>Normes d'empaquetage, sauvetage de paquets</h4>
<p>
Je suis un fervent partisan de l'adoption de normes d'empaquetage
générales pour Debian qui viseraient à rationaliser la chaîne opératoire
et à offrir un accès plus aisé à la fois aux contributeurs qui changent
d'équipe et aux nouveaux venus. J'envisage l'implémentation de normes
telles que rendre obligatoire l'entretien des paquets sur Salsa,
l'utilisation de Salsa CI pour l'intégration continue, la garantie
d'autopkgtests pour les paquets et l'exploitation d'outils de nettoyage
entre autres. À long terme, je crois que ces efforts pourraient
faciliter le versement de paquets directement à partir de Salsa,
améliorant encore l'efficacité et la collaboration dans l'écosystème de
Debian. Comme condition préalable, je préconise l'implémentation de
notre <a href="https://wiki.debian.org/PackageSalvaging">mécanisme de sauvetage</a>
pour transférer les paquets qui ne sont pas encore hébergés dans Salsa
dans les dépôts des équipes, chaque fois que c'est possible, et à défaut
dans « <tt>debian</tt>-team ».
</p>


<h3 class="subsection">3.3. Ouverture et promotion d'un environnement convivial dans Debian</h3>

<h4>Rencontres en personne</h4>
<p>
Ayant œuvré dans Debian pendant presque la moitié de ma vie, j'ai eu le
grand plaisir de rencontrer beaucoup de gens merveilleux. J'apprécie
l'environnement social établi par Debian et je suis résolu à le
conforter encore plus. Comme j'attache de l'importance aux rencontres
en personne telles que la DebConf, les MiniDebConf et les réunions
d'équipe, je les soutiendrai autant que je pourrai. Dans la continuité
de la réunion de discussion <a
href="https://debconf23.debconf.org/talks/80-face-to-face-debian-meetings-in-a-climate-crisis/">\
Face-to-face Debian meetings in a climate crisis</a> de la DebConf23,
j'encouragerai chacun à réduire ses voyages en avions autant que
possible. Heureusement, j'ai noté de toute façon la tendance à préférer
les trajets terrestres aux trajets aériens parmi les membres de la
communauté Debian.
</p>

<h4>Ouverture</h4>
<p>
Une des choses dont je suis le plus fier dans mon travail pour Debian
est le fait que mon projet préféré, Debian Med, a attiré en moyenne un
nouveau développeur par an depuis qu'il existe. Cela représente à peu
près 2 % du nombre des développeurs Debian. Cela a été réalisé en
participant aux projets d'ouverture et en trouvant nos propres moyens
pour attirer des contributeurs dans deux directions (en amont et en
aval). Si je suis élu DPL, je continuerai à aller activement à la
rencontre de nouveaux contributeurs et j'ai quelques pistes sur comment
le faire.
</p>

<h4>Tâches modestes</h4>
<p>
Par exemple, j'envisage l'organisation d'une chasse aux bogues permanente
où les contributeurs se rassembleraient pour corriger des bogues au
hasard, explorant ainsi tous les recoins de Debian. Puisant dans mon
expérience (je suis un des 10 premiers chasseurs de bogue de Debian), je
suis motivé à mettre en œuvre des initiatives de ce type. Il pourrait
être simple de créer un script pour sélectionner un bogue au hasard dans
le système de suivi des bogues (BTS) et le présenter comme objet d'un
canal Matrix dédié. Cette approche pourrait présenter aux nouveaux venus
une occasion facilement accessible de contribuer, servant de facteur
incitatif. De plus, je crois que proposer des conseils sur comment
trouver de l'aide dans Debian est crucial pour attirer et soutenir les
nouveaux contributeurs. Je suis complètement déterminé à faciliter une
telle entreprise, autant que mes responsabilités comme DPL me laissent
suffisamment de temps pour le faire.
</p>

<h4>Encourager la collaboration</h4>
<p>
Dans le même esprit, j'envisage d'implémenter autopkgtests pour un
paquet aléatoire chaque jour sur un autre canal Matrix dédié. Je vise à
exploiter l'expertise des étudiants d''Outreachy que j'ai guidés dans ces
tâches au cours de ces dernières années. Leur précieux discernement et
leur expérience seront déterminants pour assurer le succès de cette
initiative. En plus j'espère attirer d'autres contributeurs expérimentés
de Debian pour qu'ils s'associent à ces activités. En rassemblant des
nouveaux venus et des contributeurs de longue date, nous pouvons
encourager la collaboration et renforcer l'efficacité de nos
initiatives.
</p>

<h4>Diversité</h4>
<p>
Dans la communauté Debian, il existe une distribution inégale en termes
de représentation des genres et de diversité géographique. Actuellement,
il y a une surreprésentation notable des contributeurs masculins
originaires des pays habituellement considérés comme industrialisés.
</p>

<h4>Inclusivité</h4>
<p>
J'ai pu observer que la composition de notre communauté de développeur
reflète souvent la diversité présente dans notre base d'utilisateurs,
bien que ce soit à des degrés divers. Par conséquent, il est raisonnable
de penser que travailler activement à accroître la représentation des
groupes sous-représentés parmi les contributeurs Debian peut servir à
mieux ajuster le projet aux besoins divers et au point de vue de notre
base d'utilisateurs. En encourageant une plus grande inclusivité et
diversité dans notre équipe de contributeurs, nous pouvons améliorer la
pertinence et l'efficacité de notre projet en assurant qu'il demeure
accessible et bénéfique pour un éventail plus large d'individus et de
communautés.
</p>

<h4>Diminuer les obstacles</h4>
<p>
Dans le cadrer de mon engagement à promouvoir l'inclusivité et la
diversité dans la communauté Debian, j'explore activement les façons de
faciliter les premières contributions des nouveaux venus. Même si nous
avons fait des progrès significatifs pour réduire les disparités
géographiques grâce à nos efforts de traductions, des améliorations sont
encore possibles pour renforcer cette entreprise. Aussi, j'ai
l'intention d'offrir notre soutien à l'équipe de localisation pour
s'assurer que nous continuons à accroître l'accessibilité des
contributeurs de toutes les régions. De plus, pour combattre le
déséquilibre entre les genres, il est crucial de reconnaître les
divers facteurs sociaux en jeu. Par exemple, j'ai rencontré l'argument
que dans de nombreuses cultures, les femmes ont moins de temps libre
que les hommes ce qui peut faire obstacle à leur possibilité de
participer à des projets de logiciel libre. Comme solutions potentielles,
on pourrait envisager des tâches telles que la chasse aux bogues,
l'écriture d'autopkgtest et d'autres missions de courte durée qui
nécessitent un engagement de temps minimal. Cette approche vise à
diminuer les obstacles pour débuter et pour encourager la participation
de personnes avec des formations diverses et des contraintes de temps,
dans la mesure où ces tâches sont autonomes et ne nécessitent pas une
maintenance continue.
</p>

<h4>Rejoindre Debian : plaisir et avantage</h4>
<p>
La page <a href="https://wiki.debian.org/L10n">wiki de l'équipe de
traduction</a> mentionne explicitement l'existence de tâches courtes
mais utiles, soulignant que même ne consacrer qu'une heure par semaine
peut contribuer de façon significative. De la même manière, les idées
que j'ai présentées plus haut, telles que le « bogue du jour » et
« l'autopkgtest » du jour, offrent des tâches légères et autonomes qui
peuvent être réalisées dans un court laps de temps. En outre, la simple
observation de la manière dont les autres abordent ces tâches de manière
collaborative peut servir de point d'entrée pour les nouveaux venus. En
définitive, mon but est faire de l'engagement dans Debian une expérience
plaisante et gratifiante.
</p>


<h3 class="subsection">3.4. Composer avec les critiques constructives : accepter les critiques pour croître et réussir
</h3>

<h4>Apprendre de ceux qui sont partis</h4>
<p>
Dans le paysage animé du monde du logiciel libre, Debian est confrontée
inévitablement à des critiques – une preuve de son importance. Même si
la prolifération de distributions dérivées peut de prime abord paraître
positive, elle signifie aussi qu'il y a des besoins non satisfaits dans
Debian elle-même, soulevant implicitement remarques et critiques. Des
exemples passés, tel que le départ de Joey Hess et Michael Stapelberg,
contributeurs de longue date, souligne l'importance de s'occuper des
critiques. Par exemple, la <a
href="https://michael.stapelberg.ch/posts/2019-03-10-debian-winding-down/">\
formulation complète des raisons de quitter Debian</a> par Michael
Stapelberg fait office de source précieuse de perspective et de
réflexion pour la communauté. Je vais garder cet article sous la main,
en particulier les mots très forts sur les efforts à faire pour plus
d'uniformisation et pour un tournant culturel pour passer de « ce
paquet est mon domaine, comment osez-vous y toucher » à un sentiment
de propriété partagée. Je souscris aussi pleinement à la nécessité
d'une chaîne opératoire unique pour que tout Debian puisse profiter de
modifications.
</p>

<h4>Consultation d'experts</h4>
<p>
Je trouve dans cet article plusieurs idées convaincantes et, je le
pense, très pertinentes. Je suis impatient d'identifier, avec la
communauté Debian, des problèmes qui durent depuis longtemps et qui
n'ont pas encore été résolus. Je m'engage à faciliter des discussions
avec des experts qualifiés pour rechercher activement des solutions à
ces défis.
</p>


<h2 class="section">4. Merci pour votre confiance</h2>

<p>
Si vous choisissez de voter pour moi, je m'engage à faire preuve de
transparence sur mon travail. J'ai l'intention de tenir un journal
quotidien dans un dépôt Git public, sous réserve que les informations
puissent être partagées publiquement. En plus, j'enverrai un résumé
mensuel à debian-devel-announce pour vous tenir informés de mes
activités et de leur évolution.
</p>

<p>
Comme piliers de notre communauté, votre soutien m'est primordial.
Debian tient une place importante dans ma vie et je m'engage à
vous servir avec empressement. J'ai confiance en votre jugement pour
choisir le bon chef du projet. Merci d'avoir consacré du temps pour
lire mon programme et de m'accorder votre suffrage.
</p>


<h2> A. Journal des modifications </h2>

<p> Les versions de ce programme sont gérées dans un <a href="https://salsa.debian.org/tille/dpl-platform">dépôt git.</a> </p>

<ul>
<li><a href="https://salsa.debian.org/tille/dpl-platform/tags/0.7">0.7</a>: Programme pour l'élection du DPL de 2024.</li>
</ul>

<br>

</div>
