#use wml::debian::template title="Programme de Sruthi Chandran" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="11258c237aa554ca0e421076582f08445e2ad5d9" maintainer="Jean-Pierre Giraud"

		<div id="content">
			<h1>Programme de DPL 2024</h1>(Ceci est une version mise à jour de mon programme de 2021)
			<h2>Sruthi Chandran</h2>
			<a href="mailto:srud@d.o">srud@d.o</a>
			<h2>Qui suis-je ?</h2>
			<p>
				Je suis une bibliothécaire devenue une passionnée du Logiciel Libre et
				une développeuse Debian en Inde.
			</p>
			<p>Quelques-unes de mes activités dans Debian consistent en :</p>
			<ul>
				<li>
					entretien de paquets ruby, javascript, golang et fontes (en tout
					autour de 200 paquets) depuis 2016. (Pas très active actuellement) ;
				</li>
				<li>
					déléguée de l’équipe communauté depuis août 2020 ;
				</li>
				<li>
					responsable de candidature depuis juillet 2020 ;
				</li>
				<li>
					membre l'équipe Outreach depuis août/septembre 2020 ;
				</li>
				<li>
					cheffe de l'organisation de DebConf India 2023 ;
				</li>
				<li>
					membre du Comité DebConf depuis 2023.
				</li>
			</ul>
			<p>
				J’ai parrainé des personnes pour contribuer à Debian depuis plusieurs
				années maintenant. Je me suis impliquée dans l’organisation de
				nombreux ateliers d’empaquetage et dans d’autres évènements concernant
				Debian dans toute l’Inde. Je me suis impliquée aussi dans
				l’organisation d’autres évènements concernant le logiciel libre tel le
				<a href="https://camp.fsf.org.in/">Free Software Camp</a>.
			</p>
			<h2>Pourquoi je me présente ?</h2>
			<p>
				Préoccupée par le ratio hommes-femmes déséquilibré dans la communauté
				du logiciel libre (et dans Debian), je m'efforce de faire toutes les
				petites choses que je peux pour améliorer la situation. Combien de fois
				avons-nous eu une candidature non masculine (cisgenre) au poste de
				responsable du projet ? Si je ne me trompe pas, lorsque je me suis 
				présentée en 2020, j'étais la seconde candidate non masculine (cisgenre)
				au poste de DPL à ce jour. Comme pour mes deux premières candidatures au 
				poste de DPL (2020 et 2021), cette année aussi mon principal objectif est
				de soumettre les problèmes de diversité à la discussion. Je crois que
				j’ai réussi au moins en partie lors de mes précédentes campagnes.
			</p>
			<p>
				Je suis consciente que Debian réalise des choses pour accroître la
				diversité, mais comme nous pouvons le constater, cela n’est pas
				suffisant. Je suis triste qu'il n'y ait que deux femmes développeuses
				Debian dans un pays aussi grand que l’Inde. Je crois que la diversité
				n’est pas quelque chose à discuter dans Debian-women ou 
				Debian-diversity. Cela devrait venir dans la discussion pour chaque
				aspect du projet.
			</p>
			<p>
				L’élection du DPL est une activité importante dans notre projet et
				je projette de mettre en avant le thème de la diversité à chaque
				élection jusqu’à ce que nous ayons un DPL non masculin (cisgenre).
			</p>
			<p>
				Il me semble qu’une manière efficace d’encourager des personnes les
				plus diverses à contribuer est d’avoir plus de visibilité sur la
				diversité déjà présente dans la communauté. Plus les femmes (aussi
				bien cisgenres que transgenres), les hommes trans et les personnes
				non-binaires qui font déjà partie du projet deviendront davantage
				visibles plutôt que de rester cachés quelque part dans le projet, plus
				les personnes de genre différent se sentiront bien pour rejoindre
				notre communauté. La diversité ethnique ou géographique est aussi un
				point important méritant notre attention.
			</p>
			<h2>Quels sont mes projets comme DPL</h2>
			<h3>1. Diversité</h3>
			<p>
				Puisque la diversité est ma principale préoccupation,
				commençons par cela.
			</p>
			<h4><b>Budget Diversité</b></h4>
			<p>
				En dépit de la dépense importante faite pour la diversité, il ne
				semble pas que le résultat attendu soit là. Ma première tâche en
				tant que DPL sera de revoir le modèle de dépense existant pour
				analyser pourquoi et où nous nous trompons. Lorsque je dis que
				nous devons nous focaliser sur la diversité, je ne veux pas dire
				que nous devons dépenser de l’argent aveuglément au nom de la
				diversité. Je suis moi-même consciente de cas où les dépenses pour
				la diversité ont fait du mal plutôt qu’un bien quelconque.
			</p>
			<p>
				Une autre chose que je voudrais mettre en avant tout en respectant
				le budget de diversité est de le médiatiser davantage pour que les
				personnes qui le méritent le sachent et pour que nous n’arrivions
				pas à le dépenser juste par souci de le faire.
			</p>
			<h4><b>Activités Diversité</b></h4>
			<p>
				J’ai entendu que le projet Debian-women a réalisé des choses
				incroyables en améliorant la contribution des femmes jusqu’à ce qu’il
				s’endorme. Désormais, nous avons l’initiative Debian-diversity, mais
				n’avons pas encore gagné un grand dynamisme. Je projette de
				rationaliser son activité et, si possible, de déléguer une équipe
				se concentrant sur la diversité. Celle-ci devra coordonner toutes
				les activités relatives à la diversité dans Debian. Elle sera aussi
				impliquée dans les décisions et les dépenses du budget diversité.
			</p>
			<h4><b>Activités locales</b></h4>
			<p>
				Debian-localgroups qui a été conceptualisé par notre DPL actuel est
				une très bonne idée pour promouvoir la diversité géographique. Je
				prévois d’utiliser son potentiel pour organiser plus d’évènements
				locaux et pour avoir des personnes plus diverses contribuant à Debian.
			</p>
			<h3>2. Sensibilisation</h3>
			<p>
				Ma deuxième préoccupation concerne les activités de sensibilisation. Je
				travaille avec l’équipe Debian-outreach depuis plus de trois ans
				maintenant. Je pense que la promotion est une des actions importantes
				qui n’est pas assez valorisée. Notre équipe outreach est devenue
				seulement une coordination pour GSoC et Outreachy. J’ai quelques
				idées pour la sensibilisation.
			</p>
			<h4><b>Debian camp</b></h4>
			<p>
				Debian camp est un concept que j’ai emprunté
				au <a href="https://camp.fsf.org.in/">Free Software Camp</a> pour lequel
				je faisais partie des organisateurs principaux. Fondamentalement, le
				concept est un programme de parrainage en ligne de trois à quatre mois.
				L’avantage d’un programme spécifique à Debian est d’adapter le programme
				à nos besoins.
			</p>
			<p>
				Il y aurait deux parties dans le programme. En premier lieu, la
				préoccupation principale concernerait la philosophie de Debian (et du
				logiciel libre) et les raisons pour lesquelles il faut contribuer
				à Debian, etc. Nous pourrions avoir des sessions communes sur ces sujets
				avec les stagiaires. En second lieu, les stagiaires travailleraient avec
				leurs mentors sur les projets de leur choix.
			</p>
			<p>
				Dans cette activité, je prévois que l’équipe Debian-outreach
				travaillerait comme coordinatrice/facilitatrice avec les équipes
				Debian-diversity, Debian-localgroups, Debian Academy, etc. L’équipe
				Debian-outreach réaliserait en toile de fond les activités d’organisation,
				de prévision, etc., tandis que les autres équipes aideraient avec leurs
				ressources en personnes, mentors, projets, etc. J’envisage particulièrement
				que les Debian-localgroups soient impliqués de façon à avoir des activités
				régionalisées et pas uniquement en anglais.
			</p>
			<p>
				La question de verser ou non des bourses pourrait être discutée et
				décidée plus tard (nous n’avions pas de bourses pour le Free Software
				Camp et cependant ce fut un succès).
			</p>
			<h4><b>DebConf Boot-camp</b></h4>
			<p>
				C’est une rencontre physique qui pourrait se produire pendant le
				Debcamp (c’est une idée que j’ai empruntée à mollydb). Je crois qu’il
				n’y a rien de tel qu’un mentor présent physiquement.
			</p>
			<p>
				Souvent nous avons des nouvelles personnes qui assistent à la
				DebConf et qui sont perplexes quant à la façon de contribuer. Cette
				activité de démarrage peut les aider à trouver une voie. Je crois que
				beaucoup seraient intéressés à nous aider de cette façon. La manière
				précise de le mettre en pratique pourrait être discutée et décidée
				plus tard.
			</p>
			<h4><b>Examen de nos activités actuelles</b></h4>
			<p>
				Un examen minutieux de nos activités de promotion est souhaitable
				depuis longtemps. Nous avons besoin d’examiner l’efficacité de notre
				participation à GSoC et Outreachy, que pouvons-nous faire mieux et que
				pouvons-nous faire de neuf.
			</p>
			<h3>3. Communauté</h3>
			<p>
				La communauté Debian est une grande communauté. Mais cela ne signifie
				pas que nous sommes parfaits. Quelques points que j'aimerais développer
				en tant que DPL sont :
			</p>
			<h4><b>Accueil</b></h4>
			<p>
				Même si nous disons que nous sommes une communauté accueillante, pour une
				personne externe cela peut ne pas apparaître ainsi. Il y a des tas de
				raisons. La principale est que la communication en ligne amène à une
				mauvaise communication et à des échanges virulents. En tant que communauté,
				nous avons besoin d’avancer pour rendre Debian plus accueillante, pour
				les nouveaux venus, comme pour les anciens. Je suis très enthousiaste à
				l’idée de pouvoir faciliter les discussions en tant que DPL.
			</p>
			<h4><b>Acceptation du changement</b></h4>
			<p>
				Il a été souvent observé que Debian en tant que communauté est
				réticente au changement la plupart du temps. Comme DPL, je voudrais
				développer une culture d’acceptation du changement. Je voudrais
				encourager et faciliter la proposition d’idées nouvelles et le processus
				d’amélioration de Debian.
			</p>
			<h3>4. 	Debian comme organisation</h3>
			<h4><b>Une fondation Debian (?)</b></h4>
			<p>
				Lors de certaines des campagnes de DPL de ces dernières années, l'idée
				que Debian devienne une organisation enregistrée a émérgé. En 2020,
				le programme de Brian Gupta se focalisait sur la création d'une
				Fondation Debian et, en 2022, Jonathan Carter mentionnait
				l'enregistrement formel de Debian comme organisation. Lors de
				l'organisation de DebConf23, j'ai été confrontée à certains problèmes
				parce que Debian n'était pas une organisation enregistrée. C'est à ce
				moment que j'ai commencé à réfléchir à ce concept sérieusement.
			</p>
			<p>
				Aussi, en tant que DPL, je serai assurément intéressée à explorer les
				possibilités, les avantages et les désavantages de l'enregistrement de
				Debian. Je ne dis pas que cela est ma préoccupation principale, mais
				ce sujet sera assurément soulevé si je suis élue.
			</p>
			<h4><b>Organismes habilités</b></h4>
			<p>
				Dans la mesure où une des responsabilités principales du DPL est la
				gestion financière, je souhaiterais réexaminer nos relations avec les
				organismes habilités existants, les procédures de gestion de fonds et,
				si nécessaire, explorer la possibilité d'avoir plus d'organismes
				habilités pour réduire notre dépendance à un ou deux d'entre eux. Pendant
				l'organisation de la DebConf23, nous avons fait face à de nombreux
				problèmes de distribution de fonds. Certains étaient assurément
				spécifiques au cas de figure indien, mais je pense encore qu'il
				pourrait y avoir de nombreuses améliorations dans la distribution de
				fonds au travers des organismes habilités.
			</p>
			<h3>5. Finances</h3>
			<h4><b>Comptabilité</b></h4>
			<p>
				D'après les dires des précédents DPL, le traitement comptable actuel
				des actifs de Debian est très compliqué. En tant que DPL, j'aimerais
				m'occuper particulièrement de la rationalisation et de l'automatisation
				du traitement comptable au sein des divers organismes habilités. Je
				pense qu'il faut mettre en place des processus grâce auxquels nous
				disposerons d'informations transparentes et à jour sur nos actifs.
			</p>
			<h3>6. Divers</h3>
			<h4><b>Assistants du DPL</b> (Comité DPL ?)</h4>
			<p>
				Les anciens DPL disent que la charge de travail est trop importante pour
				qu'elle puisse être accomplie par une personne seule. Durant ma
				dernière campagne, quelqu'un a suggéré le concept que le DPL ait des
				assistants. Je suis d'accord avec cette manière de penser et j'ai
				l'intention de m'entourer d'une ou deux personnes pour réaliser
				certaines tâches et alléger la charge de travail du DPL. Cela aiderait
				à réduire le temps de réponse et à augmenter l'efficacité.
			</p>
			<h2>Engagement en temps</h2>
			<p>
				Un des avantages que j’ai sur la plupart des anciens ou actuels candidats
				est le temps dont je dispose pour mes activités de DPL. Mon organisation
				de travail est telle que je décide quand et pour combien de temps je
				travaille. Cela signifie que je pourrais donner une grande priorité
				à mes activités de DPL.
			</p>
			<div class="clr"></div>
		</div>
