#use wml::debian::translation-check translation="3349f1cfd8d3f3eceed8b216922ec76404aa15f5" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Clôture de DebConf23 à Kochi et annonce du lieu de DebConf24</define-tag>
<define-tag release_date>2023-09-18</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Hier, dimanche 17 septembre 2023, la conférence annuelle des développeurs
et contributeurs Debian s'est achevée.
</p>

<p>
Plus de 474 participants venus de 35 pays du monde entier se sont rassemblés
pour 89 événements combinant communications, séances de discussion ou sessions
spécialisées (BoF), ateliers et activités pour soutenir l'avancement de notre
distribution, apprendre de nos mentors et de nos pairs, bâtir notre communauté
et passer des moments agréables.
</p>

<p>
La conférence a été précédée par la session de travail annuelle du
<a href="https://wiki.debian.org/DebCamp">DebCamp</a> du 3 au
9 septembre où les développeurs et les contributeurs Debian se sont réunis
en se concentrant sur leurs projets individuels liés à Debian ou sur des
rencontres d'équipes destinées à une collaboration directe au développement
de Debian.

En particulier, cette année, il y a eu des rencontres pour faire avancer le
développement de Mobian/Debian, des constructions reproductibles et de Python
dans Debian. Il y a eu également une formation intensive pour les nouveaux
venus organisée par une équipe dédiée de mentors qui ont partagé leur
expérience pratique dans Debian et offert une compréhension approfondie de
la manière de travailler et de contribuer au sein de la communauté.
</p>

<p>
La conférence annuelle des développeurs Debian proprement dite a débuté
dimanche 10 septembre 2023. En plus des traditionnelles « brèves du chef du
projet », une séance permanente de signature de clés, les présentations
éclair et l'annonce de la conférence de l'année prochaine, DebConf24,
diverses séances de nouvelles informations de plusieurs projets et d'équipes
internes se sont tenues.

Beaucoup des sessions de discussions accueillies ont été présentées par des 
équipes techniques pour mettre en lumière leur travail centré sur le Long Term
Support (LTS), les outils Android, les distributions dérivées de Debian,
l'installateur Debian, Debian Image et l'équipe Debian Science. Les équipes
consacrées aux langages de programmation Python, Perl et Ruby ont aussi fait
part de leur travail et de leurs actions.

Deux des communautés locales les plus importantes, Debian Brasil et Debian
India, ont exposé, avec leurs guides pratiques sur leur implication dans des
actions communautaires, comment leurs collaborations respectives à Debian
ont fait progresser le projet et comment ils ont attiré de nouveaux membres
et offert des possibilités à la fois pour Debian, pour le logiciel libre et
dans le domaine scientifique.
</p>

<p>
Le <a href="https://debconf23.debconf.org/schedule/">programme</a>
a été mis à jour chaque jour avec des activités planifiées et ponctuelles
introduites par les participants pendant toute la durée de la conférence.
Plusieurs activités qui n'avaient pu avoir lieu les années précédentes du
fait de la pandémie mondial de la COVID-19 ont été célébrées à leur retour
dans le programme de la conférence : une bourse d'emploi, la soirée poésie
et scène ouverte, la fête traditionnelle du fromage et du vin, les photos
de groupe et la journée consacrée aux excursions.
</p>

<p>
Pour tous ceux qui n'ont pu participer, la plupart des communications et des
sessions ont été filmées pour une diffusion en direct avec des vidéos
rendues disponibles sur le
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/">\
site web des réunions Debian</a>.
Presque toutes les sessions ont facilité la participation à distance au moyen
d'applications de messagerie IRC ou l'édition de texte collaboratif en ligne
qui ont permis aux participants distants d'être <q>présents dans la salle</q>
pour poser des questions ou faire part de leurs observations à l'orateur ou à
l'assistance.
</p>

<p>
DebConf23 a vu plus de 4,3 Tio de données diffusées, 55 heures de
communications programmées, 23 points d'accès réseau, 11 commutateurs réseau,
75 kg de matériel importé, 400 m de ruban adhésif utilisés, 1 463 heures de
visionnage de flux vidéo, 461 T-shirts distribués, des spectateurs localisés
par Geoip dans 35 pays, 5 excursions et une moyenne de 169 repas prévus par
jour.

Toutes ces manifestations, activités, conversations et diffusions alliées à
notre passion, notre intérêt et notre implication pour Debian et le logiciel
libre et à source libre ont fait à coup sûr de cette conférence un succès
total, aussi bien à Kochi en Inde qu'en ligne dans le monde entier.
</p>

<p>
Le site web de <a href="https://debconf23.debconf.org/">DebConf23</a>
restera actif à fin d'archive et continuera à offrir des liens vers les
présentations et vidéos des communications et des événements.
</p>

<p>
L'an prochain, <a href="https://wiki.debian.org/DebConf/24">DebConf24</a> se
tiendra à Haïfa, en Israël.
Comme à l'accoutumée, les organisateurs débuteront les travaux en Israël,
avant la DebConf, par un DebCamp avec un accent particulier mis sur le
travail individuel ou en équipe pour améliorer la distribution.
</p>

<p>
DebConf s'est engagée à offrir un environnement sûr et accueillant pour tous
les participants. 
Consultez <a href="https://debconf23.debconf.org/about/coc/">la page web à
propos du Code de conduite sur le site de DebConf23</a> pour plus de détails
à ce sujet.
</p>

<p>
Debian remercie les nombreux
<a href="https://debconf23.debconf.org/sponsors/">parrains</a>
pour leur engagement dans leur soutien à DebConf23, et en particulier nos
parrains de platine :
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://www.proxmox.com/">Proxmox</a>,
et <a href="https://www.siemens.com/">Siemens</a>.
</p>

<p>
Nous souhaitons aussi remercier les équipes Vidéo et Infrastructure, les
comités DebConf23 et DebConf, notre nation hôte, l'Inde et toutes les
personnes qui ont contribué à cette manifestation et à Debian en général.

Merci à tous pour votre travail qui aide Debian à demeurer <q>le système
d'exploitation universel</q>.

À l'an prochain !
</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est devenu
l'un des plus importants et des plus influents projets à code source ouvert.
Des milliers de volontaires du monde entier travaillent ensemble pour créer et
maintenir les logiciels Debian. Traduite en soixante-dix langues et gérant un
grand nombre de types d'ordinateurs, la distribution Debian est conçue pour
être le <q>système d'exploitation universel</q>.
</p>

<h2>À propos de DebConf</h2>
<p>
DebConf est la conférence des développeurs du projet Debian. En plus d'un
programme complet de présentations techniques, sociales ou organisationnelles,
DebConf fournit aux développeurs, aux contributeurs et à toutes personnes
intéressées, une occasion de rencontre et de travail collaboratif interactif.
DebConf a eu lieu depuis 2000 en des endroits du monde aussi variés que
l'Écosse, l'Argentine et la Bosnie-Herzegovine. Plus de renseignements sont
disponibles sur <a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>À propos d'Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> est un acteur clé du marché
européen de l'informatique dématérialisée (<q>cloud</q>) et le principal
développeur des technologies Web en Suisse. Son objectif est d'être une
alternative européenne indépendante des géants du Web et s'est attaché à un Web
éthique et durable qui respecte la vie privée et crée des emplois locaux.
Infomaniak développe des solutions pour l'informatique dématérialisée (IaaS,
PaaS, VPS), des outils de productivité pour la collaboration en ligne et des
services de diffusion vidéo et radio en direct.
</p>

<h2>À propos de Proxmox</h2>
<p>
<a href="https://www.proxmox.com/">Proxmox</a> développe des logiciels de
serveur à code source ouvert puissants mais restant faciles à utiliser. La
gamme de produits de Proxmox, comprenant la virtualisation de serveur, la
sauvegarde et la sécurité du courriel, aide les entreprises quels que soient
leur taille, leur secteur d'activité ou d'industrie à simplifier leurs
infrastructures informatiques. Les solutions de Proxmox sont basées sur
l'excellente plateforme Debian et nous sommes heureux de pouvoir payer notre
dette à la communauté en parrainant DebConf23.
</p>

<h2>À propos de Siemens</h2>
<p>
<a href="https://www.siemens.com/">Siemens</a> est une entreprise de
technologie centrée sur l'industrie, les infrastructures et le transport.
Depuis des usines performantes en matière d'utilisation des ressources, des
chaînes d'approvisionnement résilientes, des bâtiments et des réseaux plus
intelligents à des transports plus propres et plus confortables et des
services de santé avancés, l'entreprise crée des technologies avec l'objectif
d'apporter une réelle valeur ajoutée à ses clients. En alliant monde réel et
monde numérique, Siemens donne à ses clients les moyens de transformer leur
industrie et leur marché, en les aidant à améliorer le quotidien de milliards
de personnes.
</p>

<h2>Plus d'informations</h2>

<p>
Pour plus d'informations, veuillez consulter la page internet de
DebConf23 à l'adresse
<a href="https://debconf23.debconf.org/">https://debconf23.debconf.org/</a>
ou écrire à &lt;press@debian.org&gt;.
</p>
