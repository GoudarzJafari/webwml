#use wml::debian::projectnews::header PUBDATE="2022-07-16" SUMMARY="Bienvenue sur les Nouvelles du projet Debian, DebConf22 a débuté, date du gel de Bookworm, annonces de sécurité Debian, nouvelles des publications de Debian, Élections et votes, Outreach, Événements, Rapports, Contributeurs"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="ee107e946f2ad10ff55cb1d418e1185fe697f471" maintainer="Jean-Pierre Giraud"

# Status: [frozen]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<h2>Bienvenue sur les Nouvelles du projet Debian !</h2>

<shortintro issue="premier"/>

<p>Nous espérons que vous apprécierez cette édition des Nouvelles du projet
Debian.</p>

<h2>La DebConf22 a débuté !</h2>

<p>
Après deux éditions en ligne, en 2020 et en 2021, la conférence annuelle de
Debian revient à son format habituel : la <a href="https://debconf22.debconf.org/">DebConf22</a>
se tient à Prizren, au Kosovo, du dimanche 17 au dimanche 24 juillet 2022.</p>

<p>
Le <a href="https://debconf22.debconf.org/schedule/">programme complet</a> 
comprend des conférences de 45 et 20 minutes et des réunions d'équipe
(« BoF »), des ateliers et une bourse d'emploi ainsi qu'une grande variété
d'autres événements. Une diffusion vidéo sera aussi
<a href="https://debconf22.debconf.org/">disponible sur le site web de DebConf22</a>.
</p>
<p>
Vous pouvez aussi suivre la couverture en directe de nouvelles concernant la
DebConf22 sur <a href="https://micronews.debian.org">https://micronews.debian.org</a>
ou sur le profil @debian de votre réseau social favori.
</p>
<p>
Debian remercie les nombreux <a href="https://debconf22.debconf.org/sponsors/">parrains</a>
pour leur engagement à soutenir la DebConf22, en particulier nos parrains de
Platine :
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://itp-prizren.com">ITP Prizren</a>
and <a href="https://google.com">Google</a>.
</p>

<h2>Calendrier (préliminaire) du gel de <q>Bookworm</q></h2>

<p>
Le développement de la prochaine publication de Debian se poursuit et l'équipe
de publication de Debian
<a href="https://lists.debian.org/debian-devel/2022/03/msg00251.html">a proposé</a>
un calendrier pour son gel :
</p>

<p>
janvier 2023 (12 janvier 2023) : étape 1 - gel des transitions et de la chaîne de compilation<br/>
février 2023 (12 février 2023) : étape 2 - gel doux<br/>
mars 2023 (12 mars 2023) : étape 3 - gel dur - pour les paquets clés et les paquets sans autopkgtests<br/>
date encore inconnue : étape 4 - gel complet
</p>

<introtoc/>

<toc-display/>

<p>Pour d'autres nouvelles, veuillez consulter le blog officiel de Debian
<a href="https://bits.debian.org">Bits from Debian</a> et suivre
<a href="https://micronews.debian.org">https://micronews.debian.org</a> qui
alimente aussi (par RSS) le profil @debian de plusieurs réseaux sociaux.</p>


<toc-add-entry name="security">Annonces de sécurité Debian importantes</toc-add-entry>

<p> L'équipe de sécurité de Debian diffuse au jour le jour les
(<a href="$(HOME)/security/2022/">annonces de sécurité pour 2022</a>).
Nous vous conseillons de les lire avec attention et de vous inscrire à la
<a href="https://lists.debian.org/debian-security-announce/">liste de diffusion
correspondante</a> pour garder votre système à jour contre toutes les
vulnérabilités. Le premier novembre 2021, l'annonce de sécurité Debian
numéro 5000 a été publié. Merci à l'équipe de sécurité pour ses efforts et son
soutien continu durant toutes ces années !</p>

<<p>Certaines annonces récemment publiées concernent ces paquets :
<a href="$(HOME)/security/2022/DSA-5185-1">mat2</a>,
<a href="$(HOME)/security/2022/DSA-5184-1">xen</a>,
<a href="$(HOME)/security/2022/DSA-5183-1">wpewebkit</a>,
<a href="$(HOME)/security/2022/DSA-5182-1">webkit2gtk</a>,
<a href="$(HOME)/security/2022/DSA-5181-1">request-tracker4</a>,
<a href="$(HOME)/security/2022/DSA-5180-1">chromium</a>,
<a href="$(HOME)/security/2022/dsa-5179-1">php7.4</a>,
<a href="$(HOME)/security/2022/dsa-5178-1">intel-microcode</a>,
<a href="$(HOME)/security/2022/dsa-5177-1">ldap-account-manager</a>,
<a href="$(HOME)/security/2022/dsa-5176-1">blender</a>,
<a href="$(HOME)/security/2022/dsa-5175-1">thunderbird</a>,
<a href="$(HOME)/security/2022/dsa-5174-1">gnupg2</a>.

<p>Le site web de Debian <a href="https://www.debian.org/lts/security/">archive</a> maintenant également
les annonces de sécurité produites par l'équipe de suivi à long terme Debian
(Debian LTS) et envoyées à la 
 <a href="https://lists.debian.org/debian-lts-announce/">liste de diffusion debian-lts-announce</a>. 
</p>

<toc-add-entry name="bullseye">Nouvelles de Debian <q>Bullseye</q> et <q>Buster</q></toc-add-entry>

<p><b>Mise à jour de Debian 11 et Debian 10 : 11.4 et 10.12 publiées</b></p>

<p>Depuis sa <a href="https://www.debian.org/News/2021/20210814">publication initiale</a>
le 14 août 2021, le projet Debian a publié quatre mises à jour de la
distribution stable Debian 11 (nom de code <q>Bullseye</q>), la dernière
<a href="https://www.debian.org/News/2022/20220709">annoncée</a> le 9 juillet 2022.</p>

<p>Le projet Debian a aussi <a href="https://www.debian.org/News/2022/2022032602">annoncé</a>
la douzième mise à jour de la distribution oldstable Debian 10 (nom de code <q>Buster</q>)
le 23 mars 2022 vers la version 10.12.</p>

<p>Ces versions intermédiaires ajoutent des corrections à des problèmes de sécurité,
tout en réglant quelques problèmes importants. Les annonces de sécurité ont déjà été
publiées séparément.

La mise à niveau d'une installation vers une de ces révisions se fait en faisant
pointer l'outil de gestion des paquets vers l'un des nombreux miroirs HTTP de Debian.
Une liste complète des miroirs est disponible à l'adresse :
<a href="https://www.debian.org/mirror/list">https://www.debian.org/mirror/list</a>
</p>

<toc-add-entry name="lts">Nouvelles sur Debian LTS</toc-add-entry>

<p>L'équipe du suivi à long terme (LTS) de Debian 
<a href="https://lists.debian.org/debian-lts-announce/2022/07/msg00002.html">a annoncé</a>
que la prise en charge de Debian 9 <q>Stretch</q> a atteint sa fin de vie le
1er juillet 2022. Un sous-ensemble de paquets de <q>Stretch</q> sera pris en
charge par des intervenants extérieurs. Vous trouverez des informations
détaillées sur la page <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>.
</p>

<p>
L'équipe LTS prendra le relais de l'équipe en charge de la sécurité pour
Debian 10 <q>Buster</q> durant le mois d'août 2022, tandis que la dernière mise
à jour de <q>Buster</q> sera publié ce même mois. Debian 10 recevra aussi un
suivi à long terme de cinq ans à compter de sa publication initiale et qui
prendra fin le 30 juin 2024. </p>

<toc-add-entry name="bugs">Plus de 1 000 000 de rapports de bogue</toc-add-entry>
<p>
Le 18 novembre 2021 à 12 h 06 m 14s UTC, Debian a atteint ce score avec le bogue
<a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1000000">nº 1 000 000</a>.
Merci à tous nos développeurs, contributeurs et utilisateurs qui nous ont aidés
à atteindre et corriger cette étape.
</p>

<toc-add-entry name="elections">Élection du responsable du projet et votes</toc-add-entry>

<p>
Plusieurs décisions ont été prises sur le processus de résolution générale de
Debian et d'autres types d'élection durant ces derniers mois et d'autres
discussions ou décisions sont prévues pour les prochains mois. En voici un
rapide résumé :
</p>

<p><b>Résolution générale : modifier le processus de résolution</b><p>
<p>Après une période de propositions et de débats débutée en 2021, le projet a
adopté officiellement plusieurs modifications dans la Constitution de
Debian afin d'amender le processus de résolution en janvier 2022. Ces
modifications de la constitution tentent de corriger plusieurs problèmes
décelés en séparant le processus de décision du Comité technique de celui des
résolutions générales dans la mesure où ils répondent à des besoins différents,
en fixant une période de débat maximale pour les résolutions, l'allongement
automatique de la période de débat des résolutions générales si les options de
vote sont modifiées, en modifiant le processus de résolution général pour
traiter équitablement toutes les options de vote ainsi que d'autres
améliorations ou clarifications.
</p>

<p>Toutes les informations sur ce vote se trouvent dans la
<a href="https://www.debian.org/vote/2021/vote_003">page web concernant cette résolution générale</a> 
et les modifications ont été appliquées en publiant la version 1.8 de la
Constitution de Debian.</p>

<p><b>Résolution générale : secret du vote</b></p>
<p>Après une période de propositions et de débats débutée en 2021, le projet a
adopté officiellement une modification dans la Constitution de Debian afin de
cacher l'identité des développeurs lorsqu'ils participent à certains scrutins,
tout en permettant les vérifications. Toutes les informations sur ce vote se
trouvent dans la 
<a href="https://www.debian.org/vote/2022/vote_001">page web concernant cette résolution générale</a>.
</p>

<p><b>Élection 2022 du responsable du projet Debian : Jonathan Carter réélu</b></p>

<p>
Jonathan Carter a été
<a href="https://bits.debian.org/2022/04/dpl-elections-2022.html">réélu</a>
pour un nouveau mandat. Il a envoyé en avril un message de 
<a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00006.html">Brèves du responsable du projet</a>
et une nouvelle information sur les tâches du responsable du projet est
<a href="https://debconf22.debconf.org/talks/5-bits-from-the-dpl/">programmée</a>
le premier jour de la DebConf22.</p>

<p><b>Discussion ouverte sur les microprogrammes</b></p>

<p>Steve McIntyre <a href="https://lists.debian.org/debian-devel/2022/04/msg00130.html">a lancé une discussion</a>
dans le projet afin de recueillir des avis et des propositions sur la manière
d'améliorer la prise en charge des microprogrammes (<q>firmware</q>) dans
Debian. Les échanges d'idées et d'opinions se poursuivent encore et il y aura une
<a href="https://debconf22.debconf.org/talks/43-fixing-the-firmware-mess/">rencontre dans la DebConf22</a>
dédiée à ce sujet. Les évolutions dans le processus de prise de décision peut
impliquer une proposition de résolution générale dans le futur, afin d'obtenir
un mandat clair du projet sur comment procéder.
</p>

<toc-add-entry name="outreach">Activités de l'équipe Debian Outreach</toc-add-entry>

<p>
Debian continue à participer aux programmes Outreachy et Google Summer of Code,
et nous sommes ravis d'annoncer que 
<a href="https://bits.debian.org/2022/05/welcome-outreachy-interns-2022.html">deux stagiaires</a> 
travailleront à l'amélioration de l'intégration du gestionnaire de paquets Yarn
à Debian pour la session de mai à août 2022 d'Outreachy, et
<a href="https://bits.debian.org/2022/05/welcome-gsoc2022-interns.html">trois stagiaires</a>
se consacreront à l'amélioration des outils du kit de développement d'Android
dans Debian et de l'assurance qualité des applications médicales et de biologie
au sein de Debian pour l'édition 2022 du Google Summer of Code.
</p>

<p>
Durant le DebCamp de la DebConf22, une <a href="https://lists.debian.org/debconf-announce/2022/07/msg00003.html">formation intensive</a> 
a été préparée pour accueillir les nouveaux venus et les introduire à Debian et
pour qu'ils acquièrent une expérience pratique d'utilisation de notre cher
système d'exploitation et de contribution au projet.
</p>

<p>
Si vous voulez aider aux efforts pour développer Debian et y favoriser la
diversité, n'hésitez pas à rejoindre l'équipe Debian Outreach ! Vous pouvez
suivre au jour le jour les nouveaux contributeurs et l'organisation des
programmes sur la
<a href="http://lists.debian.org/debian-outreach/">liste de diffusion debian-outreach</a>
et dialoguer avec nous sur le canal IRC #debian-outreach. Vous pouvez aussi
jeter un coup d’œil ou vous joindre aux autres équipes qui œuvrent au
développement de la communauté : l'équipe des
<a href="https://wiki.debian.org/LocalGroups">Groupes locaux Debian </a>
(centrée sur le soutien aux initiatives des groupes locaux avec de
l'infrastructure, des objets promotionnels, le partage d'idées, etc.), l'équipe
<a href="https://wiki.debian.org/Welcome">Welcome</a>, focalisée sur l'aide aux
nouveaux venus pour qu'ils trouvent le moyen de commencer à utiliser Debian ou
à contribuer au projet, et bien sûr les
<a href="https://mentors.debian.net/">mentors de Debian</a> pour assister les
personnes qui commencent à contribuer à Debian dans le domaine de l'empaquetage
ou des infrastructures.</p>

<toc-add-entry name="events">Chasses aux bogues, événements, MiniDebCamps et MiniDebConfs</toc-add-entry>


<p><b>Événements à venir</b></p>

<p>Le mardi 16 août prochain, la communauté Debian célébrera le vingt-neuvième
anniversaire de Debian. Certains événements sont déjà programmés, jetez un œil
à la <a href="https://wiki.debian.org/DebianDay/2022">page du wiki</a>
correspondante pour en apprendre plus sur eux ou pour ajouter à la liste votre
initiative locale pour faire connaître votre célébration de la journée Debian.
</p>

<p>
Il y aura un <a href="https://lists.debian.org/debian-events-eu/2022/06/msg00003.html">stand de présentation de Debian</a>
partagé avec <a href="https://blends.debian.org/edu/">Debian Edu</a> au
<a href="https://www.froscon.org/">FrOSCon</a>, les 20 et 21 août 2022, à Sankt
Augustin, en Allemagne.
</p>

<p>
Du 14 au 16 octobre 2022, une 
<a href="https://lists.debian.org/debian-devel-announce/2022/06/msg00002.html">chasse aux bogues</a> 
se tiendra à Karlsrhue, hébergée (et généreusement parrainée par)
<a href="https://www.unicon.com/">Unicon GmbH</a> dans la perspective de la
première étape du gel de Bookworm programmée pour le début de 2023. Pour
faciliter l'organisation de l'événement, nous vous remercions de bien vouloir
vous inscrire sur la
<a href="https://wiki.debian.org/BSP/2022/10/de/Karlsruhe">page du wiki</a> si
vous êtes intéressés.
</p>

<p><b>Événements passés</b></p>


<p>La <a href="https://wiki.debian.org/DebianEvents/de/2021/MiniDebConfRegensburg">MiniDebConf 2021 Regensburg</a>,
en Allemange, s'est tenue les 2 et 3 octobre 2021. Avec plus de cinquante-cinq
participant et plus de 20 communications et communications éclairs, cet
événement a rencontré un véritable succès. Les
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-Regensburg/">enregistrements vidéos</a>
sont disponibles.
</p>

<p>
Debian a tenu un <a href="https://stands.fosdem.org/stands/debian/">stand virtuel</a>
au FOSDEM 2022 qui s'est déroulé en ligne les 5 et 6 février 2022.
</p>

<p>
L'équipe Clojure de Debian a tenu une
<a href="https://veronneau.org/clojure-team-2022-sprint-report.html">rencontre en ligne</a>
de deux jours réunissant cinq de ses membres, les 13 et 14 mai 2022, pour
améliorer divers aspects de l'écosystème Clojure dans Debian.
</p>

<p>
La tant attendue <a href="https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg">Debian Reunion Hamburg 2022</a>
s'est tenue du lundi 23 mai au 30 mai 2022 en débutant par cinq jours de
programmation suivis de deux jours de communications. Les participants − plus de
soixante − étaient heureux de se rencontrer, de programmer et de discuter
ensemble et de partager plein d'autres activités. Pour ceux qui n'ont pu suivre
la diffusion en direct, les
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2022/Debian-Reunion-Hamburg/">enregistrements vidéos</a>
sont disponibles.
</p>

<p>Pendant la Debian Reunion, trois membres du groupe Perl de Debian Perl se
sont réunis pour une
<a href="https://bits.debian.org/2022/07/debian-perl-sprint-2022.html">Rencontre Perl (officieuse) de Debian</a>
pour poursuivre le travail de développement de Perl pour Bookworm et pour
travailler aux tâches d'assurance qualité sur les plus de 3800 paquets que
maintient l'équipe.
</p>

<toc-add-entry name="reports">Comptes-rendus</toc-add-entry>

<p><b>Comptes-rendus mensuels de Freexian sur Debian Long Term Support (LTS)</b></p>

<p>Freexian publie <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">des comptes-rendus mensuels</a>
sur le travail réalisé par les contributeurs salariés pour le suivi à long terme
de sécurité de Debian.
</p>

<p><b>État d'avancement des compilations reproductibles</b></p>

<p>Suivez le <a
href="https://reproducible-builds.org/blog/">blog des compilations reproductibles</a>
pour obtenir un compte-rendu de leur travail sur le cycle de <q>Bullseye</q>.
</p>

<toc-add-entry name="help">Demandes d'aide !</toc-add-entry>

<p><b>Paquets qui ont besoin de travail</b></p>

<wnpp link="https://lists.debian.org/debian-devel/2022/07/msg00111.html"
        orphaned="1261"
        rfa="180" />

<p><b>Bogues pour débutants</b></p>

<p>
Debian utilise l'étiquette <q>newcomer</q> (débutant) pour signaler les
bogues qui sont adaptés aux nouveaux contributeurs comme point d'entrée pour
travailler sur des paquets particuliers.

Il y a <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">199</a>
bogues marqués <q>newcomer</q> disponibles.
</p>


<toc-add-entry name="code">Code, développeurs et contributeurs</toc-add-entry>
<p><b>Nouveaux responsables de paquets depuis le 18 mars 2021</b></p>

<p>
Bienvenue à Ganesh Pawar, Job Snijders, Hugo Torres de Lima, Saakshi Jain,
Ajayi Olatunji, Eberhard Beilharz, Ayoyimika Ajibade, Mingyu Wu, Imre Jonk,
harish chavre, Jan Gru, Felix C. Stegerman, Kai-Heng Feng, Brian Thompson,
Heinrich Schuchardt, Alex David, Cézar Augusto de Campos, Antoine Le Gonidec,
Fabrice Creuzot, Pavit Kaur, Mickael Asseline, Lin Qigang, Takuma Shibuya,
Daniel Duan, karthek, Tom Teichler, Marius Vlad, Dave Lambley, Dave Jones, Jan
Gruber, Erik Maciejewski, Daniel Salzman, Caleb Adepitan, Faustin Lammler, Linus
Vanas, Prateek Ganguli, Tian Qiao, Taavi Väänänen, Andrea Pappacoda, Carsten
Schoenert, Ricardo Brandao, Joshua Peisach, Filip Strömbäck, Victor Raphael
Santos Souza, Luiz Amaral, Roman Lebedev, Paolo Pisati, S. 7, Frédéric Danis,
Mark King, Ben Westover, Chris Talbot, Bartek Fabiszewski, Matteo Bini, Lance
Lin, Shlomi Fish, William 'jawn-smith' Wilson, Carlos F. Sanz, Ileana Dumitrescu,
Marcus Hardt, Victor Westerhuis, Christophe Maudoux, Agathe Porte, Martin Dosch,
Krzysztof Aleksander Pyrkosz, Hannes Matuschek, Lourisvaldo Figueredo Junior,
Thomas E. Dickey, Ruffin White, Chris MacNaughton, Ed J, Thiago Pezzo,
Matthieu Baerts, Ryan Gonzalez, Maxim W., Katharina Drexel, Josenilson Ferreira
da Silva, Felix Dörre, Fukui Daichi, Timon Engelke, Maxime Chambonnet,
Christopher Obbard, Martin Guenther, Nick Rosbrook, Daniel Grittner, Bo Yu,
Michael Ikwuegbu, Heather Ellsworth, Israel Galadima, Francois Gindraud, Tobias
Heider, Leandro Ramos, Erik Hulsmann, Sebastian Crane, Eivind Naess, Vignesh
Raman, clesia_roberto, Jack Toh, Calvin Wan, Matthias Geiger, Robert Greener,
Lena Voytek, Glen Choo, Helmar Gerloni, Vinay Keshava, Michel Alexandre Salim,
Lev Borodin, Fab Stz, Matt Barry, Travis Wrightsman, Nathan Pratta Teodosio,
Philippe Swartvagher, Dennis Filder, Robin Alexander, Christoph Hueffelmann,
David Heidelberg, Juri Grabowski, Anupa Ann Joseph, Aaron Rainbolt, and Braulio
Henrique Marques Souto.
</p>

<p><b>Nouveaux responsables Debian</b></p>

<p>
Bienvenue à Markus Schade, Jakub Ružička, Evangelos Ribeiro Tzaras,
Hugh McMaster, Douglas Andrew Torrance, Marcel Fourné, Marcos Talau,
Sebastian Geiger, Clay Stan, Daniel Milde, David da Silva Polverari,
Sunday Cletus Nkwuda, Ma Aiguo, Sakirnth Nagarasa, Lukas Matthias Märdian,
Paulo Roberto Alves de Oliveira, Sergio Almeida Cipriano Junior, Julien Lamy,
Kristian Nielsen, Jeremy Paul Arnold Sowden, Jussi Tapio Pakkanen,
Marius Gripsgard, Martin Budaj, Peymaneh, Tommi Petteri Höynälänmaa, Lu YaNing,
Mathias Gibbens, Markus Blatt, Peter Blackman, Jan Mojžíš, Philip Wyett,
Thomas Ward, Fabio Fantoni, Mohammed Bilal, and Guilherme de Paula Xavier Segundo.
</p>

<p><b>Nouveaux développeurs Debian</b></p>
<p>
Bienvenue à Jeroen Ploemen, Mark Hindley, Scarlett Moore, Baptiste Beauplat,
Gunnar Ingemar Hjalmarsson, Stephan Lachnit, Timo Röhling, Patrick Franz,
Christian Ehrhardt, Fabio Augusto De Muzio Tobich, Taowa, Félix Sipma,
Étienne Mollier, Daniel Swarbrick, Hanno Wagner, Aloïs Micard, Sophie Brun,
Bastian Germann, Gürkan Myczko, Douglas Andrew Torrance, Mark Lee Garrett,
Francisco Vilmar Cardoso Ruviaro , Henry-Nicolas Tourneur et Nick Black.
</p>

<p><b>Contributeurs</b></p>

<p>
1064 personnes et 10 équipes sont actuellement recensées dans la page des
<a href="https://contributors.debian.org/">contributeurs</a> Debian pour 2022.
</p>

<toc-add-entry name="quicklinks">Liens rapides vers des médias sociaux de Debian</toc-add-entry>

<p>
Voici un extrait du fil
<a href="https://micronews.debian.org">micronews.debian.org</a>
d'où nous avons retiré les sujets déjà commentés dans ce numéro des
Nouvelles du projet Debian. Vous pouvez sauter cette rubrique si vous
suivez déjà <b>micronews.debian.org</b> ou le profil <b>@debian</b> sur un
réseau social (Pump.io, GNU Social, Mastodon ou Twitter).
Ces sujets sont livrés non formatés par ordre chronologique du plus
récent au plus ancien.
</p>

<ul>

<li>debuginfod.debian.net (le service Debian qui permet aux développeurs de se
soustraire à l'obligation d'installer les paquets debuginfo pour déboguer les
programmes) est de retour en ligne ! 
<a href="https://lists.debian.org/debian-devel-announce/2022/07/msg00001.html">https://lists.debian.org/debian-devel-announce/2022/07/msg00001.html</a>
</li>

<li>Dans "How Google got to rolling Linux releases for Desktops", 
Margarita Manterola et d'autres amis de Debian exposent le passage à un modèle
de publication continue (<q>rolling release</q> basée sur Debian pour la
distribution Linux destinée à répondre aux besoins internes de Google
<a href="https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops">https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops</a>
</li>

<li>Sprint de l'équipe Python pendant le #DebCamp de #DebConf22 (aujourd'hui vendredi 15)
<a href="https://wiki.debian.org/DebConf/22/Sprints">https://wiki.debian.org/DebConf/22/Sprints</a></li>

<li>Bienvenue au Innovation and Training Park (ITP) de Prizren au Kosovo (photo
de Lenharo) #DebCamp #DebConf22</li>

<li>Riseup-vpn (un service VPN facile, rapide et sûr de riseup.net, qui envoie
tout votre trafic à travers une connexion chiffrée) accepté dans unstable
<a href="https://tracker.debian.org/news/1344547/accepted-riseup-vpn-02111ds-1-source-amd64-into-unstable-unstable/">https://tracker.debian.org/news/1344547/accepted-riseup-vpn-02111ds-1-source-amd64-into-unstable-unstable/</a></li>

<li>Sprint de Debian Reproducible Builds pendant le #DebCamp de #DebConf22
<a href="https://lists.debian.org/debconf-discuss/2022/07/msg00035.html">https://lists.debian.org/debconf-discuss/2022/07/msg00035.html</a></li>

<li>Sprint de Mobian/Debian sur téléphone mobile pendant le #DebCamp de #DebConf22
<a href="https://lists.debian.org/debconf-discuss/2022/07/msg00034.html">https://lists.debian.org/debconf-discuss/2022/07/msg00034.html</a></li>

<li>#DebConf22 est a débuté ! #DebCamp s'est tenu du 10 au 16 juillet 2022 <a href="https://debconf22.debconf.org/about/debcamp/">https://debconf22.debconf.org/about/debcamp/</a></li>

<li>Brèves du Comité technique <a href="https://lists.debian.org/debian-devel-announce/2022/07/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/07/msg00000.html</a></li>

<li>Nouveaux développeurs et mainteneurs de Debian (mars et avril 2022) <a href="https://bits.debian.org/2022/05/new-developers-2022-04.html">https://bits.debian.org/2022/05/new-developers-2022-04.html</a></li>

<li>La version 4.6.1.0 de la Charte Debian publiée <a href="https://lists.debian.org/debian-devel-announce/2022/05/msg00003.html">https://lists.debian.org/debian-devel-announce/2022/05/msg00003.html</a></li>

<li>Qu'est-ce qu'un DebCamp ? Puis-je y participer ? <a href="https://debconf22.debconf.org/about/debcamp">https://debconf22.debconf.org/about/debcamp</a></li>

<li>La période de participation des développeurs Debian à la consultation a été prolongée jusqu'au 7 mai 2022 <a href="https://lists.debian.org/debian-devel-announce/2022/05/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/05/msg00000.html</a></li>

<li>Misc Developer News (nº 57) : Debian Reunion Hamburg 2022, paquets Python : outil de construction pep517, nouvelle liste de diffusion Debian : debian-math <a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00010.html">https://lists.debian.org/debian-devel-announce/2022/04/msg00010.html</a></li>

<li>Freexian et l'équipe LTS ont lancé une enquête avec un questionnaire pour
aider à la fois le responsable du projet Debian et les entités externes qui
apportent un financement à définir une politique sur quels types de travaux il est
acceptable que des gens soient payés. <a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00002.html">https://lists.debian.org/debian-devel-announce/2022/04/msg00002.html</a></li>

<li>Nouveaux développeurs et mainteneurs de Debian (janvier et février 2021) <a href="https://bits.debian.org/2022/03/new-developers-2022-02.html">https://bits.debian.org/2022/03/new-developers-2022-02.html</a></li>

<li>Salsa (salsa.debian.org, l'instance Gitlab de Debian) est de retour en ligne <a href="https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html">https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html</a> - merci aux administrateurs de DSA et de Salsa !</li>

<li>Salsa (salsa.debian.org, l'instance Gitlab de Debian) est actuellement en panne. Nous espérons qu’elle sera bientôt de retour, merci d'être patient</li>

<li>"CI for the Debian kernel team" par Ben Hutchings <a href="https://www.decadent.org.uk/ben/blog/ci-for-the-debian-kernel-team.html">https://www.decadent.org.uk/ben/blog/ci-for-the-debian-kernel-team.html</a></li>

<li>La transition vers Perl 5.34 et en cours dans unstable ! <a href="https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html</a></li>

<li>Le <q>Debian Junior Pure Blend</q> est en train d'être relancé et accepte
toute contributions ! Rejoignez l'initiative pour faire de Debian le système
d'exploitation dont les enfants aiment se servir
<a href="https://lists.debian.org/debian-jr/2022/02/msg00000.html">https://lists.debian.org/debian-jr/2022/02/msg00000.html</a></li>

<li>Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2021) <a href="https://bits.debian.org/2022/01/new-developers-2021-12.html">https://bits.debian.org/2022/01/new-developers-2021-12.html</a></li>
</ul>

<toc-add-entry name="continuedpn">Continuer à lire les Nouvelles du projet Debian</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">S'inscrire ou se désinscrire</a> de la liste de diffusion Debian News</p>

#use wml::debian::projectnews::footer editor="l'équipe en charge de la publicité avec des contributions de  Laura Arjona Reina, Jean-Pierre Giraud" translator="Jean-Pierre Giraud, l\'équipe francophone de traduction"
