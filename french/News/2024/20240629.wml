#use wml::debian::translation-check translation="e3699f036461e1416232bc8af1a6f9a475163598" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 12.6</define-tag>
<define-tag release_date>2024-06-29</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la sixième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction aide "Correction de lectures concurrentes des attributs étendus">
<correction amavisd-new "Gestion de plusieurs paramètres de limites qui contiennent des valeurs conflictuelles [CVE-2024-28054] ; correction de situation de compétition dans postinst">
<correction archlinux-keyring "Passage à des trousseaux de clés préconstruits ; synchronisation avec l'amont">
<correction base-files "Mise à jour pour cette version">
<correction bash "Reconstruction pour corriger un Built-Using obsolète">
<correction bioawk "Désactivation des constructions parallèles pour corriger des échecs aléatoires">
<correction bluez "Correction de problèmes d'exécution de code à distance [CVE-2023-27349 CVE-2023-50229 CVE-2023-50230]">
<correction cdo "Désactivation des extensions de HIRLAM pour éviter de provoquer des problèmes avec les fichiers de données ICON">
<correction chkrootkit "Reconstruction pour corriger un Built-Using obsolète">
<correction cjson "Correction de l'absence de vérifications de NULL [CVE-2023-50471 CVE-2023-50472]">
<correction clamav "Nouvelle version amont stable ; correction d'un possible problème de dépassement de tas [CVE-2024-20290], d'un possible problème d'injection de commande [CVE-2024-20328]">
<correction cloud-init "Déclaration de conflicts et replaces sur le paquet versionné introduit dans Bullseye">
<correction comitup "Assurance que le service n'est pas masqué dans la postinstallation">
<correction cpu "Fourniture d'exactement une définition de globalLdap dans le greffon de LDAP">
<correction crmsh "Création d'un répertoire et d'un fichier de journalisation à l'installation">
<correction crowdsec-custom-bouncer "Reconstruction pour corriger un Built-Using obsolète">
<correction crowdsec-firewall-bouncer "Reconstruction avec une version de golang-github-google-nftables avec une correction de la prise en charge de l'architecture petit-boutiste">
<correction curl "Protocoles par défaut abandonnés quand ils sont désélectionnés [CVE-2024-2004] ; correction de fuite de mémoire [CVE-2024-2398]">
<correction dar "Reconstruction pour corriger un Built-Using obsolète">
<correction dcmtk "Nettoyage correct après une suppression complète">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 6.1.0-22 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debvm "debvm-create : faire une connexion d'installation ; bin/debvm-waitssh : --timeout=N fonctionne ; bin/debvm-run : exécution dans les environnements sans TERM défini ; correction de resolv.conf dans Stretch">
<correction dhcpcd5 "privsep : messages de longueur zéro autorisés ; correction d'un redémarrage incorrect du serveur lors des mises à niveau">
<correction distro-info-data "Déclaration des intentions pour Bullseye/Bookworm ; correction de données anciennes ; ajout d'Ubuntu 24.10">
<correction djangorestframework "Restauration des fichiers statiques manquants">
<correction dm-writeboost "Correction d'erreur de construction avec le noyau 6.9 et les rétroportages">
<correction dns-root-data "Mise à jour de root.hints ; mise à jour des informations de sécurité expirées">
<correction dpdk "Nouvelle version amont stable">
<correction ebook-speaker "Prise en charge des noms d’utilisateur de plus de 8 caractères lors de l'énumération des groupes">
<correction emacs "Correction de sécurité [CVE-2024-30202 CVE-2024-30203 CVE-2024-30204 CVE-2024-30205] ; remplacement de package-keyring.gpg expiré par la version actuelle">
<correction extrepo-data "Mise à jour des informations sur le dépôt">
<correction flatpak "Nouvelle version amont stable">
<correction fpga-icestorm "Compatibilité avec yosys restaurée">
<correction freetype "Désactivation de la prise en charge de COLRv1 activé de façon non intentionnelle par l'amont ; correction de la vérification de l'existence de la fonction lors de l'appel de get_colr_glyph_paint()">
<correction galera-4 "Nouvelle version amont de correction de bogues ; mise à jour de la clé de signature de la version amont ; échec des tests relatifs aux dates évités">
<correction gdk-pixbuf "ANI : rejet des fichiers avec de multiples morceaux anih [CVE-2022-48622] ; ANI : rejet des fichiers avec de multiples morceaux INAM ou IART ; ANI : validation de la taille des morceaux anih">
<correction glewlwyd "Correction d'un potentiel dépassement de tampon pendant la validation d'accréditation FIDO2 [CVE-2023-49208] ; correction de redirection non contrôlée au moyen de redirect_uri [CVE-2024-25715]">
<correction glib2.0 "Correction d'une (rare) fuite de mémoire">
<correction glibc "Suppression de la correction de l'appel permanent des destructeurs dans l'ordre inverse des constructeurs à cause de problèmes imprévus de compatibilité d'applications ; correction d'une corruption de DTV due à la réutilisation d'un identifiant de module TLS suivant dlclose avec un TLS inutilisé">
<correction gnutls28 "Correction du plantage certtool lors de la vérification d'une chaîne de certificats comptant plus de certificats [CVE-2024-28835] ; correction d'une attaque par canal auxiliaire dans l'ECDSA déterministe [CVE-2024-28834] ; correction d'une fuite de mémoire ; correction de deux erreurs de segmentation">
<correction golang-github-containers-storage "Reconstruction pour corriger un Built-Using obsolète">
<correction golang-github-google-nftables "Correction de la fonction AddSet() sur les architectures petit-boutistes">
<correction golang-github-openshift-imagebuilder "Reconstruction pour corriger un Built-Using obsolète">
<correction gosu "Reconstruction pour corriger un Built-Using obsolète">
<correction gpaste "Correction de conflit avec les versions anciennes de libpgpaste6">
<correction gross "Correction d'un dépassement de pile [CVE-2023-52159]">
<correction hovercraft "Dépendance à python3-setuptools">
<correction icinga2 "Correction d'une erreur de segmentation sur ppc64el">
<correction igtf-policy-bundle "Traitement du changement de politique S/MIME de CAB Forum ; application des mises à jour accumulées aux ancres de confiance">
<correction intel-microcode "Atténuation de vulnérabilité de sécurité [CVE-2023-22655 CVE-2023-28746 CVE-2023-38575 CVE-2023-39368 CVE-2023-43490] ; atténuation pour INTEL-SA-01051 [CVE-2023-45733], INTEL-SA-01052 [CVE-2023-46103], INTEL-SA-01036 [CVE-2023-45745,  CVE-2023-47855] et de problèmes fonctionnels non précisés sur divers processeurs Intel">
<correction jose "Correction d'un problème de déni de service potentiel [CVE-2023-50967]">
<correction json-smart "Correction d'une récursion excessive menant à un dépassement de pile [CVE-2023-1370] ; correction d'un déni de service au moyen d'une requête contrefaite [CVE-2021-31684]">
<correction kio "Correction de problèmes de perte de fichier et de potentiel verrouillage dans CIFS">
<correction lacme "Correction de la logique de validation postémission">
<correction libapache2-mod-auth-openidc "Correction d'une absence de validation d'entrée menant à un déni de service [CVE-2024-24814]">
<correction libesmtp "Ajout de breaks et replaces aux versions anciennes de la bibliothèque">
<correction libimage-imlib2-perl "Correction de la construction du paquet">
<correction libjwt "Correction d'une attaque temporelle par canal auxiliaire [CVE-2024-25189]">
<correction libkf5ksieve "Fuite de mots de passe évitée dans les journaux côté serveur">
<correction libmail-dkim-perl "Ajout d'une dépendance à libgetopt-long-descriptive-perl">
<correction libpod "Gestion correcte des conteneurs supprimés">
<correction libreoffice "Correction de la création de copie de sauvegarde pour des fichiers sur des partages samba montés ; pas de suppression de libforuilo.so dans -core-nogui">
<correction libseccomp "Ajout de la prise en charge des appels système jusqu'à Linux 6.7">
<correction libtommath "Correction de dépassement d'entier [CVE-2023-36328]">
<correction libtool "Conflit avec libltdl3-dev ; correction de vérification de l'opérateur += dans func_append">
<correction libxml-stream-perl "Correction de la compatibilité avec IO::Socket::SSL &gt;= 2.078">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 22">
<correction linux-signed-amd64 "Nouvelle version amont stable ; passage de l'ABI à la version 22">
<correction linux-signed-arm64 "Nouvelle version amont stable ; passage de l'ABI à la version 22">
<correction linux-signed-i386 "Nouvelle version amont stable ; passage de l'ABI à la version 22">
<correction lua5.4 "debian/version-script : exportation de symboles supplémentaires manquants pour lua 5.4.4">
<correction lxc-templates "Correction de l'option <q>mirror</q> de lxc-debian">
<correction mailman3 "Dépendance alternative à cron-daemon ; correction de l'URL postgresql:// dans le script postinstallation">
<correction mksh "Gestion de merged /usr dans /etc/shells ; correction de plantage avec des <q>bashismes</q> imbriqués ; correction des arguments pour la commande dot ; distinction de non défini et de vide dans<q>typeset -p</q>`">
<correction mobian-keyring "Mise à jour de la clé de l'archive Mobian">
<correction ms-gsl "Marquage des constructeurs not_null comme noexcept">
<correction nano "Correction de problèmes de chaîne de format ; correction de <q>with --cutfromcursor, défaire une justification peut supprimer une ligne</q> ; correction d'un problème de lien symbolique malveillant ; correction d'exemple de liaisons dans nanorc">
<correction netcfg "Gestion du routage pour les masques réseau à adresse unique">
<correction ngircd "Respect de l'option <q>SSLConnect</q> pour les connexions entrantes ; validation du certificat de serveur sur les liens de serveur (S2S-TLS) ; METADATA : correction de la désactivation de <q>cloakhost</q>">
<correction node-babel7 "Correction de la construction avec nodejs 18.19.0+dfsg-6~deb12u1 ; ajout de breaks et replaces sur les paquets node-babel-* obsolètes">
<correction node-undici "Exportation correcte des types typescript">
<correction node-v8-compile-cache "Correction de tests quand une version plus récente de nodejs est utilisée">
<correction node-zx "Correction de tests peu fiables">
<correction nodejs "Tests peu fiables évités pour mipsel/mips64el">
<correction nsis "Utilisateurs non privilégiés empêchés de supprimer le répertoire du désinstallateur [CVE-2023-37378] ; correction d'une régression en désactivant des déplacements restreints ; construction reproductible pour arm64">
<correction nvidia-graphics-drivers "Restauration de la compatibilité avec les nouvelles constructions du noyau Linux ; remplacement des paquets de nvidia-graphics-drivers-tesla ; ajout du nouveau paquet nvidia-suspend-common ; assouplissement de la dépendance de construction de dh-dkms pour la compatibilité avec Bookworm ; nouvelle version amont stable [CVE-2023-0180 CVE-2023-0183 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199 CVE-2023-25515 CVE-2023-25516 CVE-2023-31022 CVE-2024-0074 CVE-2024-0075 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-graphics-drivers-tesla "Restauration de la compatibilité avec les nouvelles constructions du noyau Linux">
<correction nvidia-graphics-drivers-tesla-470 "Restauration de la compatibilité avec les nouvelles constructions du noyau Linux ; arrêt de la construction de nvidia-cuda-mps ; nouvelle version amont stable ; corrections de sécurité [CVE-2022-42265 CVE-2024-0074 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-modprobe "Préparation du passage aux pilotes LTS des séries 535">
<correction nvidia-open-gpu-kernel-modules "Mise à jour vers les pilotes LTS des séries 535 [CVE-2023-0180 CVE-2023-0183 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199 CVE-2023-25515 CVE-2023-25516 CVE-2023-31022 CVE-2024-0074 CVE-2024-0075 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-persistenced "Passage aux pilotes LTS des séries 535 ; mise à jour de la liste des pilotes pris en charge">
<correction nvidia-settings "Ajout de la construction pour ppc64el ; nouvelle version LTS amont">
<correction nvidia-xconfig "Nouvelle version LTS amont">
<correction openrc "Scripts non-exécutables ignorés dans /etc/init.d">
<correction openssl "Nouvelle version amont stable ; correction de problèmes de consommation de temps excessive [CVE-2023-5678 CVE-2023-6237], problème de corruption de registre de vecteur sur PowerPC [CVE-2023-6129], plantage du décodage de PKCS12 [CVE-2024-0727]">
<correction openvpn-dco-dkms "Construction pour Linux &gt;= 6.5 ; installation du répertoire compat-include ; correction d'une asymétrie du compteur de références">
<correction orthanc-dicomweb "Reconstruction pour corriger un Built-Using obsolète">
<correction orthanc-gdcm "Reconstruction pour corriger un Built-Using obsolète">
<correction orthanc-mysql "Reconstruction pour corriger un Built-Using obsolète">
<correction orthanc-neuro "Reconstruction pour corriger un Built-Using obsolète">
<correction orthanc-postgresql "Reconstruction pour corriger un Built-Using obsolète">
<correction orthanc-python "Reconstruction pour corriger un Built-Using obsolète">
<correction orthanc-webviewer "Reconstruction pour corriger un Built-Using obsolète">
<correction orthanc-wsi "Reconstruction pour corriger un Built-Using obsolète">
<correction ovn "Nouvelle version amont stable ; correction d'une validation insuffisante des paquets BSD entrants [CVE-2024-2182]">
<correction pdudaemon "Dépendance à python3-aiohttp">
<correction php-composer-class-map-generator "Chargement des dépendances système obligé">
<correction php-composer-pcre "Ajout de breaks et replaces manquant sur composer (&lt;&lt; 2.2)">
<correction php-composer-xdebug-handler "Chargement des dépendances système obligé">
<correction php-doctrine-annotations "Chargement des dépendances système obligé">
<correction php-doctrine-deprecations "Chargement des dépendances système obligé">
<correction php-doctrine-lexer "Chargement des dépendances système obligé">
<correction php-phpseclib "Protection de isPrime() et randomPrime() pour BigInteger [CVE-2024-27354] ; limitation de longueur d'OID dans ASN1 [CVE-2024-27355] ; correction de BigInteger getLength() ; suppression des modificateurs de visibilité des variables statiques">
<correction php-phpseclib3 "Chargement des dépendances système obligé ; protection de isPrime() et randomPrime() pour BigInteger [CVE-2024-27354] ; limitation de longueur d'OID dans ASN1 [CVE-2024-27355] ; correction de getLength() de BigInteger">
<correction php-proxy-manager "Chargement des dépendances système obligé">
<correction php-symfony-contracts "Chargement des dépendances système obligé">
<correction php-zend-code "Chargement des dépendances système obligé">
<correction phpldapadmin "Correction de la compatibilité avec PHP 8.1+">
<correction phpseclib "Chargement des dépendances système obligé ; protection de isPrime() et randomPrime() pour BigInteger [CVE-2024-27354] ; limitation de longueur d'OID dans ASN1 [CVE-2024-27355] ; correction de getLength() de BigInteger">
<correction postfix "Nouvelle version amont stable">
<correction postgresql-15 "Nouvelle version amont stable ; visibilité des entrées pg_stats_ext et pg_stats_ext_exprs au propriétaire de la table [CVE-2024-4317]">
<correction prometheus-node-exporter-collectors "Pas d'atteinte au réseau miroir ; correction de blocage avec d'autres exécutions d'apt update">
<correction pymongo "Correction d'un problème de lecture hors limites [CVE-2024-5629]">
<correction pypy3 "Élimination des caractères de contrôle C0 et d'espace dans urlsplit [CVE-2023-24329] ; contournement des protections de connexion TLS évité sur les sockets fermés [CVE-2023-40217] ; tempfile.TemporaryDirectory : correction d'un bogue de lien symbolique dans le nettoyage [CVE-2023-6597] ; protection de zipfile contre la bombe de décompression <q>quoted-overlap</q> [CVE-2024-0450]">
<correction python-aiosmtpd "Correction d'un problème de dissimulation SMTP [CVE-2024-27305] ; correction d'un problème d'injection de commande STARTTLS non chiffrée [CVE-2024-34083]">
<correction python-asdf "Suppression de la dépendance inutile à asdf-unit-schemas">
<correction python-channels-redis "Assurance de la fermeture des <q>pools</q> lors de la fermeture d'une boucle dans le <q>core</q>">
<correction python-idna "Correction d'un problème de déni de service [CVE-2024-3651]">
<correction python-jwcrypto "Correction d'un problème de déni de service [CVE-2024-28102]">
<correction python-xapian-haystack "Suppression de la dépendance à django.utils.six">
<correction python3.11 "Correction d'un plantage d'utilisation de mémoire après libération lors de la dés-allocation d'un objet <q>frame</q> ; protection de zipfile contre la bombe de décompression <q>quoted-overlap</q> [CVE-2024-0450] ; tempfile.TemporaryDirectory : correction d'un bogue de lien symbolique dans le nettoyage [CVE-2023-6597] ; correction de <q>os.path.normpath(): troncature de chemin à un octet NULL</q> [CVE-2023-41105] ; contournement des protections de connexion TLS évité sur les sockets fermés [CVE-2023-40217] ; élimination des caractères de contrôle C0 et d'espace dans urlsplit [CVE-2023-24329] ; déréférencement potentiel de pointeur NULL évité dans filleutils">
<correction qemu "Nouvelle version amont stable ; corrections de sécurité [CVE-2024-26327 CVE-2024-26328 CVE-2024-3446 CVE-2024-3447]">
<correction qtbase-opensource-src "Correction d'une régression sur le correctif pour le CVE-2023-24607 ; utilisation évitée des certificats CA système quand ils ne sont pas désirés [CVE-2023-34410] ; correction de dépassement de tampon [CVE-2023-37369] ; correction d'une boucle infinie dans une expansion d'entité XML récursive [CVE-2023-38197] ; correction d'un dépassement de tampon avec un fichier image KTX contrefait [CVE-2024-25580] ; correction de vérification de dépassement d'entier de HPack [CVE-2023-51714]">
<correction rails "Déclaration de breaks et replaces sur le paquet ruby-arel obsolète">
<correction riseup-vpn "Utilisation du paquet de certificats système par défaut, restaurant la possibilité de se connecter à une terminaison utilisant un certificat LetsEncrypt">
<correction ruby-aws-partitions "Inclusion assurée des fichiers partitions.json et partitions-metadata.json dans le paquet binaire">
<correction ruby-premailer-rails "Suppression de la dépendance de construction à ruby-arel qui est obsolète">
<correction rust-cbindgen-web "Nouveau paquet source pour prendre en charge la construction des nouvelles versions de Firefox ESR">
<correction rustc-web "Nouveau paquet source pour prendre en charge la construction des navigateurs web">
<correction schleuder "Correction d'une validation insuffisante d'analyse d'argument ; correction de l'importation de clés à partir des pièces jointes envoyées par Thunderbird et gestion des courriels sans autre contenu ; recherche de mots clés seulement au début des courriels ; validation des adresses de courriel converties en minuscules lors de la vérification des abonnés ; prise en compte de l'en-tête From pour trouver les adresses de réponse">
<correction sendmail "Correction d'un problème de dissimulation SMTP [CVE-2023-51765]">
<correction skeema "Reconstruction pour corriger un Built-Using obsolète">
<correction skopeo "Reconstruction pour corriger un Built-Using obsolète">
<correction software-properties "software-properties-qt : ajout de conflicts et replaces à software-properties-kde pour faciliter les mises à niveau à partir de Bullseye">
<correction supermin "Reconstruction pour corriger un Built-Using obsolète">
<correction symfony "Chargement des dépendances système obligé ; DateTypTest : assurance que l'année soumise est un choix acceptable">
<correction systemd "Nouvelle version amont stable ; correction de problèmes de déni de service [CVE-2023-50387 CVE-2023-50868] ; libnss-myhostname.nss : installation après <q>files</q> ; libnss-mymachines.nss : installation avant <q>resolve</q> et <q>dns</q>">
<correction termshark "Reconstruction pour corriger un Built-Using obsolète">
<correction tripwire "Reconstruction pour corriger un Built-Using obsolète">
<correction tryton-client "Envoi limité aux contenus compressés dans les sessions authentifiées">
<correction tryton-server "Attaques de type <q>zip-bomb</q> évitées à partir des sources non authentifiées">
<correction u-boot "Correction de orion-timer pour l'amorçage de sheevaplug et des plateformes apparentées">
<correction uif "Prise en charge des noms d'interface VLAN">
<correction umoci "Reconstruction pour corriger un Built-Using obsolète">
<correction user-mode-linux "Reconstruction pour corriger un Built-Using obsolète">
<correction wayfire "Ajout de dépendances manquantes">
<correction what-is-python "Déclaration de breaks et replaces sur python-dev-is-python2 ; correction de la manipulation de version dans les règles de construction">
<correction wpa "Correction d'un problème de contournement d'authentification [CVE-2023-52160]">
<correction xscreensaver "Désactivation des avertissements à propos des anciennes versions">
<correction yapet "Pas d'appel d'EVP_CIPHER_CTX_set_key_length() dans crypt/blowfish et crypt/aes">
<correction zsh "Reconstruction pour corriger un Built-Using obsolète">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2023 5575 webkit2gtk>
<dsa 2023 5580 webkit2gtk>
<dsa 2023 5589 nodejs>
<dsa 2024 5609 slurm-wlm-contrib>
<dsa 2024 5616 ruby-sanitize>
<dsa 2024 5618 webkit2gtk>
<dsa 2024 5619 libgit2>
<dsa 2024 5620 unbound>
<dsa 2024 5621 bind9>
<dsa 2024 5623 postgresql-15>
<dsa 2024 5624 edk2>
<dsa 2024 5625 engrampa>
<dsa 2024 5626 pdns-recursor>
<dsa 2024 5627 firefox-esr>
<dsa 2024 5628 imagemagick>
<dsa 2024 5630 thunderbird>
<dsa 2024 5631 iwd>
<dsa 2024 5632 composer>
<dsa 2024 5633 knot-resolver>
<dsa 2024 5635 yard>
<dsa 2024 5637 squid>
<dsa 2024 5638 libuv1>
<dsa 2024 5640 openvswitch>
<dsa 2024 5641 fontforge>
<dsa 2024 5642 php-dompdf-svg-lib>
<dsa 2024 5643 firefox-esr>
<dsa 2024 5644 thunderbird>
<dsa 2024 5645 firefox-esr>
<dsa 2024 5646 cacti>
<dsa 2024 5650 util-linux>
<dsa 2024 5651 mediawiki>
<dsa 2024 5653 gtkwave>
<dsa 2024 5655 cockpit>
<dsa 2024 5657 xorg-server>
<dsa 2024 5658 linux-signed-amd64>
<dsa 2024 5658 linux-signed-arm64>
<dsa 2024 5658 linux-signed-i386>
<dsa 2024 5658 linux>
<dsa 2024 5659 trafficserver>
<dsa 2024 5661 php8.2>
<dsa 2024 5662 apache2>
<dsa 2024 5663 firefox-esr>
<dsa 2024 5664 jetty9>
<dsa 2024 5665 tomcat10>
<dsa 2024 5666 flatpak>
<dsa 2024 5669 guix>
<dsa 2024 5670 thunderbird>
<dsa 2024 5672 openjdk-17>
<dsa 2024 5673 glibc>
<dsa 2024 5674 pdns-recursor>
<dsa 2024 5677 ruby3.1>
<dsa 2024 5678 glibc>
<dsa 2024 5679 less>
<dsa 2024 5680 linux-signed-amd64>
<dsa 2024 5680 linux-signed-arm64>
<dsa 2024 5680 linux-signed-i386>
<dsa 2024 5680 linux>
<dsa 2024 5682 glib2.0>
<dsa 2024 5682 gnome-shell>
<dsa 2024 5684 webkit2gtk>
<dsa 2024 5685 wordpress>
<dsa 2024 5686 dav1d>
<dsa 2024 5688 atril>
<dsa 2024 5690 libreoffice>
<dsa 2024 5691 firefox-esr>
<dsa 2024 5692 ghostscript>
<dsa 2024 5693 thunderbird>
<dsa 2024 5695 webkit2gtk>
<dsa 2024 5698 ruby-rack>
<dsa 2024 5699 redmine>
<dsa 2024 5700 python-pymysql>
<dsa 2024 5702 gst-plugins-base1.0>
<dsa 2024 5704 pillow>
<dsa 2024 5705 tinyproxy>
<dsa 2024 5706 libarchive>
<dsa 2024 5707 vlc>
<dsa 2024 5708 cyrus-imapd>
<dsa 2024 5709 firefox-esr>
<dsa 2024 5711 thunderbird>
<dsa 2024 5712 ffmpeg>
<dsa 2024 5713 libndp>
<dsa 2024 5714 roundcube>
<dsa 2024 5715 composer>
<dsa 2024 5717 php8.2>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction phppgadmin "Problèmes de sécurité ; incompatible avec la version de PostgreSQL de Bookworm">
<correction pytest-salt-factories "Nécessaire uniquement pour salt qui ne fait pas partie de Bookworm">
<correction ruby-arel "Obsolète, intégré à ruby-activerecord, incompatible avec ruby-activerecord 6.1.x">
<correction spip "Incompatible avec la version de PHP de Bookworm">
<correction vasttrafik-cli "Retrait de l'API">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Mises à jour proposées à la distribution stable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
