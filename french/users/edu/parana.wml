# From: Marcos Castilho <marcos@inf.ufpr.br>

<define-tag pagetitle>D&eacute;partement d'informatique &ndash; universit&eacute; f&eacute;d&eacute;rale de Paran&aacute;, Br&eacute;sil</define-tag>
<define-tag webpage>http://www.inf.ufpr.br/</define-tag>

#use wml::debian::users
#use wml::debian::translation-check translation="c270aa0cfc57d6f5f792bfda05c0a3161f8b0690" maintainer="Jean-Paul Guillonneau"

<p>
Nous utilisons Debian/GNU Linux partout et fournissons un miroir de Debian
depuis 1999 ainsi qu’un miroir de Debian Security depuis 2011. Notre centre
de données gère le filtrage et le routage d’un réseau de 20 Gb, un serveur DNS
d’autorité et un serveur de courriels, tout cela en utilisant des variantes
stables et instables de Debian. En utilisant des machines Debian hautement
optimisées, nous sommes capables de fournir plus de 40 miroirs pour des
projets aux codes source ouverts, en étant pour cela le plus grand miroir non
commercial de l’hémisphère sud et un des plus grands du monde.
</p>
<p>
Avec Debian nous fournissons aussi une infrastructure de virtualisation pour
des projets de recherche. Nous possédons un stockage de 1 Pbit à travers
plusieurs implémentations de système de fichiers distribué, sur plus de
1700 cœurs de traitement et de plus de 10 Tb de RAM. Nous gérons plus de
150 machines virtuelles, toutes exécutant Debian, fournissant divers services
pour la communauté tels que l’édition collaborative de document, les plateformes
d’apprentissage, les appels vidéos, la gestion de versions et les bases de
données scientifiques. Pour la recherche de pointe, nous gérons aussi une grappe
avec plus de 46 k de cœurs CUDA avec Debian. Pour les étudiants, nous
fournissons un amorçage à travers des installations de réseau, activant plus de
370 terminaux exécutant Debian et ses dérivés.
</p>

<p>
Debian nous fournit la stabilité, la sécurité et un entretien aisé. Notre
infrastructure est principalement gérée par C3SL (Centro de Computação Científica
e Software Livre) qui est un groupe de recherche centré sur le
développement de solutions au code source ouvert et responsable de la gestion et
de l’hébergement de plusieurs projets gouvernementaux.
</p>
