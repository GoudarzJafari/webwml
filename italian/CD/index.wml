#use wml::debian::cdimage title="Debian supporti per l'installazione su USB, CD, DVD" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e" maintainer="Luca Monducci"

<div class="tip">
<p>A coloro che desiderano solo installare Debian su un computer che
dispone di una connessione Internet si raccomanda
l'<a href="netinst/">installazione via rete</a> per scaricare solo il
minimo indispensabile.</p>
</div>

<div class="tip">
<p>Tutte le immagini per USB/CD/DVD per le architetture i386 e amd64 possono
<a href="https://www.debian.org/CD/faq/#write-usb">essere usate anche su
una chiavetta USB</a>.</p>
</div>

<ul>

  <li><a href="http-ftp/">Scaricare le immagini per USB/CD/DVD via
  HTTP.</a></li>

  <li><a href="torrent-cd/">Scaricare le immagini per USB/CD/DVD con
  BitTorrent.</a>
  Il sistema <em>peer to peer</em> Bittorrent permette di scaricare le immagini
  da parte di molti utenti in contemporanea e in maniera cooperativa, può
  aumentare la velocità del download.</li>

  <li><a href="live/">Scaricare le immagini live mediante HTTP, FTP o
  BitTorrent.</a>
  Le immagini live permettono di avviare un sistema senza farne l'installazione.
  Possono essere usate per provare prima Debian e successivamente farne
  l'installazione.</li>

  <li><a href="vendors/">Acquistare i supporti Debian già pronti.</a></li>

  <li><a href="jigdo-cd/">Scaricare le immagini per USB/CD/DVD con jigdo.</a>
  Adatto solo agli utenti esperti.
  Il sistema <em>jigdo</em> permette di scegliere il mirror Debian più
  veloce per il download, tra i 300 disponibili in tutto il
  mondo. Permette una facile selezione del mirror e l'aggiornamento
  delle vecchie immagini all'ultima release disponibile. Inoltre, è
  l'unico modo per scaricare tutte le immagini dei DVD Debian.</li>

  <li>In caso di problemi, consultare le <a href="faq/">FAQ sui CD/DVD
  Debian.</a>.</li>

</ul>

<p>Le immagini ufficiali per USB/CD/DVD sono firmate digitalmente, ciò
permette di <a href="verify">verificarne l'autenticità</a>.</p>

<p>Debian è disponibile per varie architetture hardware, i più avranno bisogno
delle immagini per <q>amd64</q>, cioè per i sistemi PC a 64 bit.</p>

  <div class="cdflash" id="latest">
    Ultima release ufficiale delle immagini per USB/CD/DVD <q>stable</q>:
    <strong><current-cd-release></strong>.
  </div>

<p>Le informazioni sui problemi relativi all'installazione possono essere
trovate nella pagina <a
href="$(HOME)/releases/stable/debian-installer/">relativa</a>.</p>
