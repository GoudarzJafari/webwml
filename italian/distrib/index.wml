#use wml::debian::template title="Scaricare Debian" BARETITLE=true
#use wml::debian::translation-check translation="96a44b00371cb1e75df59d859a900aab253ee412" maintainer="Giuseppe Sacco"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Questa pagina contiene i modi per scaricare e installare Debian Stable.</p>

<ul>
<li> <a href="../CD/http-ftp/#mirrors">Scaricare dai mirror</a> le immagini per l'installazione
<li> <a href="../releases/stable/installmanual">Guida all'installazione</a> con le istruzioni d'installazione dettagliate
<li> <a href="../releases/stable/releasenotes">Note di rilascio</a>
<li> <a href="../devel/debian-installer/">Immagini ISO per Debian testing</a>
<li> <a href="../CD/verify">Verificare l'autenticità delle immagini Debian</a>
</ul>


<div class="line">
 <div class="item col50">
  <h2>Scaricare un file immagine</h2>
  <ul>
      <li>Un <a href="netinst"><strong>piccolo file immagine di
      installazione</strong></a>:
      è rapido da scaricare e va copiato su un supporto rimovibile.
      Per usare questo tipo di file immagine, si necessiterà di una
      macchina con accesso ad Internet.
      <ul class="quicklist downlist">
	  <li><a title="scarica l'installatore per PC 64-bit"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">netinst
		 iso 64-bit PC</a></li>
	  <li><a title="scarica l'installatore per i comuni PC 32-bit"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">netinst
		 iso 32-bit PC</a></li>
	  <li><a title="scarica il CD via torrent per PC AMD e Intel a 64-bit"
                href="<stable-images-url/>/amd64/bt-cd/">torrent per netinst per PC a 64-bit</a></li>
	  <li><a title="scarica il CD via torrent per PC AMD e Intel a 32-bit"
                href="<stable-images-url/>/i386/bt-cd/">32-bit PC netinst torrents</a></li>
	</ul>
      </li>
      <li>Un <a href="../CD/"><strong>file immagine di installazione
      completo</strong></a>: più grande rispetto al precedente, contiene
      un maggior numero di pacchetti e rende quindi più semplice
      l'installazione di Debian su macchine prive di una connessione a
      Internet.
	<ul class="quicklist downlist">
         <li><a title="scarica l'installer per PC a 64-bit Intel e AMD PC"
                href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">ISO
                DVD-1 per PC a 64-bit</a></li>
         <li><a title="scarica l'installer per PC a 32-bit Intel e AMD PC"
                href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">ISO
                DVD-1 per PC a 32-bit</a></li>
	  <li><a title="scarica i torrent per il DVD di installazione per PC 64-bit"
	         href="<stable-images-url/>/amd64/bt-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso.torrent">torrent per PC 64-bit (DVD)</a></li>
	  <li><a title="scarica i torrent per il DVD di installazione per PC 32-bit"
		 href="<stable-images-url/>/i386/bt-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso.torrent">torrent per PC 32-bit (DVD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2>Provare Debian live prima dell'installazione</h2>
    <p>
     È possibile provare Debian avviando un sistema live da un CD, un DVD
     o una chiave USB senza dover installare nessun file sul computer.
     Si potrà avviare l'installazione dal sistema live usando <a
     href="https://calamares.io">Calamares</a>. Disponibile solo per PC
     64-bit. Puoi consultare <a href="../CD/live#choose_live">ulteriori
     informazioni su questo metodo</a>.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Scarica Gnome live ISO per PC 64-bit Intel a AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Scarica Xfce live ISO per PC 64-bit Intel a AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Scarica KDE live ISO per PC 64-bit Intel a AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Scarica altre ISO live ISO per PC 64-bit Intel a AMD"
          href="<live-images-url/>/amd64/iso-hybrid/">Altre ISO live</a></li>
      <li><a title="Scarica i torrent per Debian Live per PC 64-bit"
          href="<live-images-url/>/amd64/bt-hybrid/">torrent per live</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2>È anche possibile <a href="../CD/vendors/">acquistare
    CD, DVD o chiavette USB da uno dei rivenditori di supporti
    per l'installazione</a></h2>

    <p>
      Molti di questi venditori ti permetteranno di acquistare la
      distribuzione per meno di 5 dollari USA (poco meno di 5 Euro), 
      più le spese di spedizione (non sempre internazionali).
    </p>

    <p>Ecco alcuni vantaggi dei CD:</p>

    <ul>
     <li>è possibile installare Debian su macchine prive di connessione a
     Internet</li>
     <li>si può installare Debian
      senza dover scaricarsi tutti i pacchetti</li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Comprare un computer con Debian gi&agrave;
    installata</a></h2>
    <p>Vi sono una serie di vantaggi da considerare legati a questo
    metodo:</p>
    <ul>
     <li>non si deve installare Debian</li>
     <li>l'installazione è già configurata per l'hardware</li>
     <li>il rivenditore può fornire supporto tecnico</li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2>Usare un'immagine Debian in cloud</h2>
    <p>Una <a href="https://cloud.debian.org/images/cloud/"><strong>immagine cloud</strong></a>, realizzata dal gruppo cloud, può essere usata in:</p>
    <ul>
      <li>il proprio provider OpenStack, nei formati «qcow2» o «row».
        <ul class="quicklist downlist">
          <li>AMD/Intel 64-bit (<a title="Immagine OpenStack qcow2 per AMD/Intel 64-bit"
          	href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a
          	title="Immagine OpenStack raw per AMD/Intel 64-bit" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
	  <li>ARM 64-bit (<a title="Immagine OpenStack qcow2 per ARM 64-bit"
	  	href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a
	  	title="Immagine OpenStack raw per ARM 64-bit" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
          <li>PowerPC Little Endian 64-bit (<a title="Immagine OpenStack qcow2 per PowerPC little endian 64-bit"
          	href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a
          	title="Immagine OpenStack raw per PowerPC little endian 64-bit"
          	href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
        </ul>
      </li>
      <li>Una macchina virtuale QEMU locale, in formato qcow2 o raw.
      <ul class="quicklist downlist">
          <li>AMD/Intel 64-bit (<a title="QEMU image for 64-bit AMD/Intel qcow2"
          	href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a
          	title="Immagine QEMU per AMD/Intel 64-bit" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
	  <li>ARM 64-bit (<a title="QEMU image for 64-bit ARM qcow2"
	  	href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a
	  	title="Immagine QEMU raw per ARM 64-bit" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
          <li>PowerPC Little Endian 64-bit (<a title="Immagine QEMU qcow2 per PowerPC little endian 64-bit"
          	href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a
          	title="Immagine QEMU raw per PowerPC little endian 64-bit"
          	href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, sia come immagine di macchina, sia tramite il marketplace di AWS.
        <ul class="quicklist downlist">
          <li><a title="Immagini di macchina Amazon" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Immagini di macchina Amazon</a></li>
          <li><a title="Marketplace AWS" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">Marketplace AWS</a></li>
        </ul>
      </li>
      <li>Microsoft Azure, nel marketplace di Microsoft
        <ul class="quicklist downlist">
          <li><a title="Debian 12 sul marketplace Azure" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
          <li><a title="Debian 11 sul marketplace Azure" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
