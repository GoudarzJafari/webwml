<define-tag pagetitle>Il Progetto Debian piange la scomparsa di Peter De Schrijver</define-tag>
<define-tag release_date>2024-07-25</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="08d95547b797f8dd3e3b1575b759cba78e627b32" maintainer="Luca Monducci"

<p>
Il Progetto Debian piange l'improvvisa scomparsa del nostro collega
sviluppatore e amico Peter De Schrijver.
</p>

<p>
Molti di noi conoscevano Peter come una persona molto disponibile e
impegnata e abbiamo apprezzato i suoi contributi al nostro progetto e
alla comunità Linux.
</p>

<p>
Peter era un volto regolare e familiare in molte conferenze e incontri
in tutto il mondo.
</p>

<p>
Peter era molto apprezzato per la sua competenza tecnica nella risoluzione
dei problemi e per la sua disponibilità a condividere questa conoscenza.
Quando gli si chiedeva <q>a cosa stai lavorando?</q>, Peter spesso si
prendeva il tempo per spiegare in modo comprensibile qualcosa che era
estremamente complicato, o per mostrare di persona la sua elevata competenza
tecnica in attività come la traduzione di un binario disassemblato in
codice sorgente C.
</p>

<p>
Il lavoro, gli ideali e la memoria di Peter lasciano un'eredità notevole e
una perdita che si fa sentire in tutto il mondo, non solo nelle molte
comunità con cui ha interagito, ma anche in quelle che ha ispirato e toccato.
</p>

<p>
I nostri pensieri sono rivolti alla sua famiglia.
</p>

<h2>Su Debian</h2>

<p>Il Progetto Debian è un'associazione di sviluppatori di software
libero che volontariamente offrono il loro tempo libero e il loro lavoro per
produrre il sistema operativo libero Debian.</p>

<h2>Contatti</h2>

<p>Per maggiori informazioni si prega di visitare il sito web <a
href="$(HOME)/">https://www.debian.org/</a>, mandare un'e-mail a
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>

