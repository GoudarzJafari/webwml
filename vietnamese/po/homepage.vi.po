# Trần Ngọc Quân <vnwildman@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml homepage\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-07-17 16:13+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 3.38.0\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Hệ điều hành toàn cầu"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "DebConf đang diễn ra!"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "Lô gô DebConf"

#: ../../english/index.def:19
#, fuzzy
#| msgid "DC19 Group Photo"
msgid "DC22 Group Photo"
msgstr "Ảnh của nhóm DC19"

#: ../../english/index.def:22
#, fuzzy
#| msgid "DebConf19 Group Photo"
msgid "DebConf22 Group Photo"
msgstr "Ảnh của nhóm DebConf19"

#: ../../english/index.def:26
msgid "Debian Reunion Hamburg 2023"
msgstr ""

#: ../../english/index.def:29
#, fuzzy
#| msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgid "Group photo of the Debian Reunion Hamburg 2023"
msgstr "Ảnh của nhóm tại MiniDebConf ở Regensburg 2021"

#: ../../english/index.def:33
msgid "Mini DebConf Regensburg 2021"
msgstr ""

#: ../../english/index.def:36
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Ảnh của nhóm tại MiniDebConf ở Regensburg 2021"

#: ../../english/index.def:40
msgid "Screenshot Calamares Installer"
msgstr "Ảnh chụp màn hình bộ cài đặt Calamares"

#: ../../english/index.def:43
msgid "Screenshot from the Calamares installer"
msgstr "Ảnh chụp từ màn hình bộ cài đặt Calamares"

#: ../../english/index.def:47 ../../english/index.def:50
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian giống như một con dao quân đội Thụy Sỹ"

#: ../../english/index.def:54
msgid "People have fun with Debian"
msgstr "Mọi người đang vui vẻ cùng Debian"

#: ../../english/index.def:57
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Mọi người Debian tại Debconf18 ở Tân Trúc đang thật sự vui sướng"
